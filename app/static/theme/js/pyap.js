"use strict";

// подняться вверх сайта
jQuery(window).scroll(function () {
    if (jQuery(this).scrollTop() != 0) { jQuery('#toTop').fadeIn(); }
    else { jQuery('#toTop').fadeOut(); }
});
jQuery('#toTop').click(function () {
    jQuery('body,html').animate({scrollTop: 0}, 300);
});

/** --------------------
 * OLD functional
* -------------------- */

// // for IE7+ input type='number'
// $('input[type=number]').keypress(function(e) {if (!String.fromCharCode(e.keyCode).match(/[0-9\.]/)) {return false;}});
//
// // поиск
// var $searchBlock = $('#form-search');
// $(document).on('click', '#button-search', function () {
//     $searchBlock.slideToggle();
//     return false;
// });
// // всплывающие подсказки
// $(function () { $('[data-toggle="tooltip"]').tooltip() });
//
// // маска для телефонов
// $(".jsPhoneInput").mask("8(999)999-99-99", { placeholder: "8(999)999-99-99"});
//
// // маска для даты
// var $jsDatepicker = $('.jsDatepicker');
// $jsDatepicker.mask("00.00.0000", {
//     placeholder: "00.00.0000"
// });
// $jsDatepicker.datepicker({
//     date: new Date(1990, 1, 14),
//     format: 'dd.mm.YYYY',
//     language: 'ru-RU'
// });
//
// // адрес ГуглАПИ
// try {
//     var elemAdress = document.getElementById('id_address');
//     if (elemAdress) {
//         var id_address = new google.maps.places.Autocomplete(elemAdress, {
//         language: 'ru',
//         componentRestrictions: {country: 'ru'}});
//     }
// } catch (er){
//     console.warn('|!_!|: google.map JS\n', er);
// }

