--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.12
-- Dumped by pg_dump version 9.5.12

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: advertising_sliderhome; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.advertising_sliderhome (
    id integer NOT NULL,
    title character varying(126) NOT NULL,
    description text,
    image character varying(500) NOT NULL,
    url character varying(200),
    sort smallint,
    CONSTRAINT advertising_sliderhome_sort_check CHECK ((sort >= 0))
);


ALTER TABLE public.advertising_sliderhome OWNER TO dervek;

--
-- Name: advertising_sliderhome_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.advertising_sliderhome_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.advertising_sliderhome_id_seq OWNER TO dervek;

--
-- Name: advertising_sliderhome_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.advertising_sliderhome_id_seq OWNED BY public.advertising_sliderhome.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO dervek;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO dervek;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO dervek;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO dervek;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO dervek;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO dervek;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO dervek;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO dervek;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO dervek;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO dervek;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO dervek;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO dervek;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: blog_blog; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.blog_blog (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    author_id integer,
    parent_id integer,
    CONSTRAINT blog_blog_level_check CHECK ((level >= 0)),
    CONSTRAINT blog_blog_lft_check CHECK ((lft >= 0)),
    CONSTRAINT blog_blog_rght_check CHECK ((rght >= 0)),
    CONSTRAINT blog_blog_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.blog_blog OWNER TO dervek;

--
-- Name: blog_blog_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.blog_blog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_blog_id_seq OWNER TO dervek;

--
-- Name: blog_blog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.blog_blog_id_seq OWNED BY public.blog_blog.id;


--
-- Name: blog_blogimage; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.blog_blogimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    blog_id integer NOT NULL
);


ALTER TABLE public.blog_blogimage OWNER TO dervek;

--
-- Name: blog_blogimage_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.blog_blogimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_blogimage_id_seq OWNER TO dervek;

--
-- Name: blog_blogimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.blog_blogimage_id_seq OWNED BY public.blog_blogimage.id;


--
-- Name: blog_comment; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.blog_comment (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    text text NOT NULL,
    ip_address inet,
    username character varying(125),
    email character varying(254) NOT NULL,
    is_show boolean NOT NULL,
    post_id integer,
    user_id integer
);


ALTER TABLE public.blog_comment OWNER TO dervek;

--
-- Name: blog_comment_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.blog_comment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_comment_id_seq OWNER TO dervek;

--
-- Name: blog_comment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.blog_comment_id_seq OWNED BY public.blog_comment.id;


--
-- Name: blog_post; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.blog_post (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    is_allow_comments boolean NOT NULL,
    comment_count integer NOT NULL,
    author_id integer,
    blog_id integer
);


ALTER TABLE public.blog_post OWNER TO dervek;

--
-- Name: blog_post_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.blog_post_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_post_id_seq OWNER TO dervek;

--
-- Name: blog_post_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.blog_post_id_seq OWNED BY public.blog_post.id;


--
-- Name: blog_postimage; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.blog_postimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    post_id integer NOT NULL
);


ALTER TABLE public.blog_postimage OWNER TO dervek;

--
-- Name: blog_postimage_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.blog_postimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.blog_postimage_id_seq OWNER TO dervek;

--
-- Name: blog_postimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.blog_postimage_id_seq OWNED BY public.blog_postimage.id;


--
-- Name: catalog_catalog; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.catalog_catalog (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    author_id integer,
    parent_id integer,
    CONSTRAINT catalog_catalog_level_check CHECK ((level >= 0)),
    CONSTRAINT catalog_catalog_lft_check CHECK ((lft >= 0)),
    CONSTRAINT catalog_catalog_rght_check CHECK ((rght >= 0)),
    CONSTRAINT catalog_catalog_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.catalog_catalog OWNER TO dervek;

--
-- Name: catalog_catalog_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.catalog_catalog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalog_catalog_id_seq OWNER TO dervek;

--
-- Name: catalog_catalog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.catalog_catalog_id_seq OWNED BY public.catalog_catalog.id;


--
-- Name: catalog_catalogimage; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.catalog_catalogimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    catalog_id integer NOT NULL
);


ALTER TABLE public.catalog_catalogimage OWNER TO dervek;

--
-- Name: catalog_catalogimage_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.catalog_catalogimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.catalog_catalogimage_id_seq OWNER TO dervek;

--
-- Name: catalog_catalogimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.catalog_catalogimage_id_seq OWNED BY public.catalog_catalogimage.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO dervek;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO dervek;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO dervek;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO dervek;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO dervek;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO dervek;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO dervek;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO dervek;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO dervek;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.django_site_id_seq OWNED BY public.django_site.id;


--
-- Name: gallery_gallery; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.gallery_gallery (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    author_id integer,
    parent_id integer,
    CONSTRAINT gallery_gallery_level_check CHECK ((level >= 0)),
    CONSTRAINT gallery_gallery_lft_check CHECK ((lft >= 0)),
    CONSTRAINT gallery_gallery_rght_check CHECK ((rght >= 0)),
    CONSTRAINT gallery_gallery_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.gallery_gallery OWNER TO dervek;

--
-- Name: gallery_gallery_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.gallery_gallery_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gallery_gallery_id_seq OWNER TO dervek;

--
-- Name: gallery_gallery_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.gallery_gallery_id_seq OWNED BY public.gallery_gallery.id;


--
-- Name: gallery_galleryimage; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.gallery_galleryimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    gallery_id integer NOT NULL
);


ALTER TABLE public.gallery_galleryimage OWNER TO dervek;

--
-- Name: gallery_galleryimage_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.gallery_galleryimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gallery_galleryimage_id_seq OWNER TO dervek;

--
-- Name: gallery_galleryimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.gallery_galleryimage_id_seq OWNED BY public.gallery_galleryimage.id;


--
-- Name: home_home; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.home_home (
    id integer NOT NULL,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    title character varying(255) NOT NULL,
    html text,
    is_show boolean NOT NULL,
    blog_id integer
);


ALTER TABLE public.home_home OWNER TO dervek;

--
-- Name: home_home_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.home_home_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.home_home_id_seq OWNER TO dervek;

--
-- Name: home_home_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.home_home_id_seq OWNED BY public.home_home.id;


--
-- Name: home_homeimage; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.home_homeimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    home_id integer NOT NULL
);


ALTER TABLE public.home_homeimage OWNER TO dervek;

--
-- Name: home_homeimage_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.home_homeimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.home_homeimage_id_seq OWNER TO dervek;

--
-- Name: home_homeimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.home_homeimage_id_seq OWNED BY public.home_homeimage.id;


--
-- Name: include_area_includearea; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.include_area_includearea (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    is_show boolean NOT NULL,
    image character varying(500),
    description text,
    html_code character varying(255),
    code character varying(20) NOT NULL,
    sort smallint NOT NULL,
    CONSTRAINT include_area_includearea_sort_check CHECK ((sort >= 0))
);


ALTER TABLE public.include_area_includearea OWNER TO dervek;

--
-- Name: include_area_includearea_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.include_area_includearea_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.include_area_includearea_id_seq OWNER TO dervek;

--
-- Name: include_area_includearea_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.include_area_includearea_id_seq OWNED BY public.include_area_includearea.id;


--
-- Name: menu_mainmenu; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.menu_mainmenu (
    id integer NOT NULL,
    name character varying(80),
    is_show boolean NOT NULL,
    sort smallint NOT NULL,
    lft integer NOT NULL,
    rght integer NOT NULL,
    tree_id integer NOT NULL,
    level integer NOT NULL,
    blog_id integer,
    catalog_id integer,
    gallery_id integer,
    page_id integer,
    parent_id integer,
    CONSTRAINT menu_mainmenu_level_check CHECK ((level >= 0)),
    CONSTRAINT menu_mainmenu_lft_check CHECK ((lft >= 0)),
    CONSTRAINT menu_mainmenu_rght_check CHECK ((rght >= 0)),
    CONSTRAINT menu_mainmenu_tree_id_check CHECK ((tree_id >= 0))
);


ALTER TABLE public.menu_mainmenu OWNER TO dervek;

--
-- Name: menu_mainmenu_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.menu_mainmenu_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.menu_mainmenu_id_seq OWNER TO dervek;

--
-- Name: menu_mainmenu_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.menu_mainmenu_id_seq OWNED BY public.menu_mainmenu.id;


--
-- Name: order_order; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.order_order (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    first_name character varying(50) NOT NULL,
    last_name character varying(50) NOT NULL,
    email character varying(254) NOT NULL,
    address character varying(250) NOT NULL,
    postal_code character varying(20),
    city character varying(100),
    total_cost numeric(10,2) NOT NULL,
    ttn character varying(128),
    comment text,
    status_id integer,
    user_id integer NOT NULL
);


ALTER TABLE public.order_order OWNER TO dervek;

--
-- Name: order_order_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.order_order_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_order_id_seq OWNER TO dervek;

--
-- Name: order_order_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.order_order_id_seq OWNED BY public.order_order.id;


--
-- Name: order_orderitem; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.order_orderitem (
    id integer NOT NULL,
    price numeric(10,2) NOT NULL,
    quantity smallint NOT NULL,
    order_id integer NOT NULL,
    product_item_id integer NOT NULL,
    CONSTRAINT order_orderitem_quantity_check CHECK ((quantity >= 0))
);


ALTER TABLE public.order_orderitem OWNER TO dervek;

--
-- Name: order_orderitem_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.order_orderitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_orderitem_id_seq OWNER TO dervek;

--
-- Name: order_orderitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.order_orderitem_id_seq OWNED BY public.order_orderitem.id;


--
-- Name: order_status; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.order_status (
    id integer NOT NULL,
    name character varying(255) NOT NULL
);


ALTER TABLE public.order_status OWNER TO dervek;

--
-- Name: order_status_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.order_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_status_id_seq OWNER TO dervek;

--
-- Name: order_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.order_status_id_seq OWNED BY public.order_status.id;


--
-- Name: order_story; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.order_story (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    total_cost numeric(10,2) NOT NULL,
    comment text,
    order_id integer NOT NULL,
    status_id integer
);


ALTER TABLE public.order_story OWNER TO dervek;

--
-- Name: order_story_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.order_story_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.order_story_id_seq OWNER TO dervek;

--
-- Name: order_story_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.order_story_id_seq OWNED BY public.order_story.id;


--
-- Name: page_page; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.page_page (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    is_allow_comments boolean NOT NULL,
    author_id integer
);


ALTER TABLE public.page_page OWNER TO dervek;

--
-- Name: page_page_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.page_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_page_id_seq OWNER TO dervek;

--
-- Name: page_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.page_page_id_seq OWNED BY public.page_page.id;


--
-- Name: page_pagecomment; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.page_pagecomment (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    text text NOT NULL,
    ip_address inet,
    username character varying(125),
    email character varying(254) NOT NULL,
    is_show boolean NOT NULL,
    page_id integer,
    user_id integer
);


ALTER TABLE public.page_pagecomment OWNER TO dervek;

--
-- Name: page_pagecomment_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.page_pagecomment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_pagecomment_id_seq OWNER TO dervek;

--
-- Name: page_pagecomment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.page_pagecomment_id_seq OWNED BY public.page_pagecomment.id;


--
-- Name: page_pageimage; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.page_pageimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    page_id integer NOT NULL
);


ALTER TABLE public.page_pageimage OWNER TO dervek;

--
-- Name: page_pageimage_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.page_pageimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.page_pageimage_id_seq OWNER TO dervek;

--
-- Name: page_pageimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.page_pageimage_id_seq OWNED BY public.page_pageimage.id;


--
-- Name: product_product; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.product_product (
    id integer NOT NULL,
    title character varying(255),
    description text,
    slug character varying(50) NOT NULL,
    seo_title character varying(255),
    seo_description text,
    seo_keywords text,
    og_locale text,
    scripts text,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    html text,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    articul character varying(256),
    is_bestseller boolean NOT NULL,
    is_new boolean NOT NULL,
    author_id integer
);


ALTER TABLE public.product_product OWNER TO dervek;

--
-- Name: product_product_catalog; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.product_product_catalog (
    id integer NOT NULL,
    product_id integer NOT NULL,
    catalog_id integer NOT NULL
);


ALTER TABLE public.product_product_catalog OWNER TO dervek;

--
-- Name: product_product_catalog_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.product_product_catalog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_catalog_id_seq OWNER TO dervek;

--
-- Name: product_product_catalog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.product_product_catalog_id_seq OWNED BY public.product_product_catalog.id;


--
-- Name: product_product_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.product_product_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_id_seq OWNER TO dervek;

--
-- Name: product_product_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.product_product_id_seq OWNED BY public.product_product.id;


--
-- Name: product_product_recommend_products; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.product_product_recommend_products (
    id integer NOT NULL,
    from_product_id integer NOT NULL,
    to_product_id integer NOT NULL
);


ALTER TABLE public.product_product_recommend_products OWNER TO dervek;

--
-- Name: product_product_recommend_products_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.product_product_recommend_products_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_product_recommend_products_id_seq OWNER TO dervek;

--
-- Name: product_product_recommend_products_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.product_product_recommend_products_id_seq OWNED BY public.product_product_recommend_products.id;


--
-- Name: product_productcomment; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.product_productcomment (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    text text NOT NULL,
    ip_address inet,
    username character varying(125),
    email character varying(254) NOT NULL,
    is_show boolean NOT NULL,
    product_id integer,
    user_id integer
);


ALTER TABLE public.product_productcomment OWNER TO dervek;

--
-- Name: product_productcomment_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.product_productcomment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productcomment_id_seq OWNER TO dervek;

--
-- Name: product_productcomment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.product_productcomment_id_seq OWNED BY public.product_productcomment.id;


--
-- Name: product_productimage; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.product_productimage (
    id integer NOT NULL,
    image character varying(500),
    image_title character varying(255),
    image_is_main boolean NOT NULL,
    image_description text,
    product_id integer NOT NULL
);


ALTER TABLE public.product_productimage OWNER TO dervek;

--
-- Name: product_productimage_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.product_productimage_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productimage_id_seq OWNER TO dervek;

--
-- Name: product_productimage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.product_productimage_id_seq OWNED BY public.product_productimage.id;


--
-- Name: product_productitem; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.product_productitem (
    id integer NOT NULL,
    created timestamp with time zone NOT NULL,
    updated timestamp with time zone NOT NULL,
    name character varying(256) NOT NULL,
    articul character varying(256),
    price numeric(10,2) NOT NULL,
    price_discount numeric(10,2) NOT NULL,
    price_purchase numeric(10,2) NOT NULL,
    quantity integer NOT NULL,
    is_main boolean NOT NULL,
    product_id integer NOT NULL
);


ALTER TABLE public.product_productitem OWNER TO dervek;

--
-- Name: product_productitem_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.product_productitem_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.product_productitem_id_seq OWNER TO dervek;

--
-- Name: product_productitem_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.product_productitem_id_seq OWNED BY public.product_productitem.id;


--
-- Name: settings_template_footer; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.settings_template_footer (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    text_info_id integer,
    CONSTRAINT settings_template_footer_sort_check CHECK ((sort >= 0))
);


ALTER TABLE public.settings_template_footer OWNER TO dervek;

--
-- Name: settings_template_footer_blogs; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.settings_template_footer_blogs (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    blog_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_blogs OWNER TO dervek;

--
-- Name: settings_template_footer_blogs_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.settings_template_footer_blogs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_blogs_id_seq OWNER TO dervek;

--
-- Name: settings_template_footer_blogs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.settings_template_footer_blogs_id_seq OWNED BY public.settings_template_footer_blogs.id;


--
-- Name: settings_template_footer_catalog; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.settings_template_footer_catalog (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    catalog_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_catalog OWNER TO dervek;

--
-- Name: settings_template_footer_catalog_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.settings_template_footer_catalog_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_catalog_id_seq OWNER TO dervek;

--
-- Name: settings_template_footer_catalog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.settings_template_footer_catalog_id_seq OWNED BY public.settings_template_footer_catalog.id;


--
-- Name: settings_template_footer_galleries; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.settings_template_footer_galleries (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    gallery_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_galleries OWNER TO dervek;

--
-- Name: settings_template_footer_galleries_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.settings_template_footer_galleries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_galleries_id_seq OWNER TO dervek;

--
-- Name: settings_template_footer_galleries_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.settings_template_footer_galleries_id_seq OWNED BY public.settings_template_footer_galleries.id;


--
-- Name: settings_template_footer_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.settings_template_footer_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_id_seq OWNER TO dervek;

--
-- Name: settings_template_footer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.settings_template_footer_id_seq OWNED BY public.settings_template_footer.id;


--
-- Name: settings_template_footer_list_link; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.settings_template_footer_list_link (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    listlink_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_list_link OWNER TO dervek;

--
-- Name: settings_template_footer_list_link_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.settings_template_footer_list_link_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_list_link_id_seq OWNER TO dervek;

--
-- Name: settings_template_footer_list_link_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.settings_template_footer_list_link_id_seq OWNED BY public.settings_template_footer_list_link.id;


--
-- Name: settings_template_footer_page; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.settings_template_footer_page (
    id integer NOT NULL,
    footer_id integer NOT NULL,
    page_id integer NOT NULL
);


ALTER TABLE public.settings_template_footer_page OWNER TO dervek;

--
-- Name: settings_template_footer_page_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.settings_template_footer_page_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_footer_page_id_seq OWNER TO dervek;

--
-- Name: settings_template_footer_page_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.settings_template_footer_page_id_seq OWNED BY public.settings_template_footer_page.id;


--
-- Name: settings_template_settingstemplate; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.settings_template_settingstemplate (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    is_included boolean NOT NULL,
    phone character varying(30) NOT NULL,
    address character varying(255) NOT NULL,
    logo character varying(500),
    robots_txt text,
    terms_of_use text,
    scripts text,
    footer_id integer,
    home_id integer,
    site_id integer
);


ALTER TABLE public.settings_template_settingstemplate OWNER TO dervek;

--
-- Name: settings_template_settingstemplate_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.settings_template_settingstemplate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.settings_template_settingstemplate_id_seq OWNER TO dervek;

--
-- Name: settings_template_settingstemplate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.settings_template_settingstemplate_id_seq OWNED BY public.settings_template_settingstemplate.id;


--
-- Name: site_info_listlink; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.site_info_listlink (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    url character varying(255),
    sort smallint NOT NULL,
    is_show boolean NOT NULL,
    type_link character varying(1) NOT NULL,
    CONSTRAINT site_info_listlink_sort_check CHECK ((sort >= 0))
);


ALTER TABLE public.site_info_listlink OWNER TO dervek;

--
-- Name: site_info_listlink_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.site_info_listlink_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_info_listlink_id_seq OWNER TO dervek;

--
-- Name: site_info_listlink_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.site_info_listlink_id_seq OWNED BY public.site_info_listlink.id;


--
-- Name: site_info_socialnetwork; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.site_info_socialnetwork (
    id integer NOT NULL,
    title character varying(255),
    image character varying(512),
    html_link character varying(512),
    url character varying(200),
    image_description text,
    image_is_main boolean NOT NULL,
    image_title character varying(255)
);


ALTER TABLE public.site_info_socialnetwork OWNER TO dervek;

--
-- Name: site_info_socialnetwork_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.site_info_socialnetwork_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_info_socialnetwork_id_seq OWNER TO dervek;

--
-- Name: site_info_socialnetwork_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.site_info_socialnetwork_id_seq OWNED BY public.site_info_socialnetwork.id;


--
-- Name: site_info_textinfo; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.site_info_textinfo (
    id integer NOT NULL,
    title character varying(125) NOT NULL,
    html text NOT NULL
);


ALTER TABLE public.site_info_textinfo OWNER TO dervek;

--
-- Name: site_info_textinfo_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.site_info_textinfo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.site_info_textinfo_id_seq OWNER TO dervek;

--
-- Name: site_info_textinfo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.site_info_textinfo_id_seq OWNED BY public.site_info_textinfo.id;


--
-- Name: users_userlink; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.users_userlink (
    id integer NOT NULL,
    anchor character varying(100),
    url character varying(200),
    user_id integer NOT NULL
);


ALTER TABLE public.users_userlink OWNER TO dervek;

--
-- Name: users_userlink_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.users_userlink_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_userlink_id_seq OWNER TO dervek;

--
-- Name: users_userlink_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.users_userlink_id_seq OWNED BY public.users_userlink.id;


--
-- Name: users_userprofile; Type: TABLE; Schema: public; Owner: dervek
--

CREATE TABLE public.users_userprofile (
    id integer NOT NULL,
    avatar character varying(500),
    patronymic character varying(155),
    birthday date,
    phone character varying(32),
    address character varying(256),
    about text,
    id_signed_news boolean NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.users_userprofile OWNER TO dervek;

--
-- Name: users_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: dervek
--

CREATE SEQUENCE public.users_userprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_userprofile_id_seq OWNER TO dervek;

--
-- Name: users_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: dervek
--

ALTER SEQUENCE public.users_userprofile_id_seq OWNED BY public.users_userprofile.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.advertising_sliderhome ALTER COLUMN id SET DEFAULT nextval('public.advertising_sliderhome_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blog ALTER COLUMN id SET DEFAULT nextval('public.blog_blog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blogimage ALTER COLUMN id SET DEFAULT nextval('public.blog_blogimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_comment ALTER COLUMN id SET DEFAULT nextval('public.blog_comment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_post ALTER COLUMN id SET DEFAULT nextval('public.blog_post_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_postimage ALTER COLUMN id SET DEFAULT nextval('public.blog_postimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalog ALTER COLUMN id SET DEFAULT nextval('public.catalog_catalog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalogimage ALTER COLUMN id SET DEFAULT nextval('public.catalog_catalogimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_site ALTER COLUMN id SET DEFAULT nextval('public.django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_gallery ALTER COLUMN id SET DEFAULT nextval('public.gallery_gallery_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_galleryimage ALTER COLUMN id SET DEFAULT nextval('public.gallery_galleryimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.home_home ALTER COLUMN id SET DEFAULT nextval('public.home_home_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.home_homeimage ALTER COLUMN id SET DEFAULT nextval('public.home_homeimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.include_area_includearea ALTER COLUMN id SET DEFAULT nextval('public.include_area_includearea_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu ALTER COLUMN id SET DEFAULT nextval('public.menu_mainmenu_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_order ALTER COLUMN id SET DEFAULT nextval('public.order_order_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_orderitem ALTER COLUMN id SET DEFAULT nextval('public.order_orderitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_status ALTER COLUMN id SET DEFAULT nextval('public.order_status_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_story ALTER COLUMN id SET DEFAULT nextval('public.order_story_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_page ALTER COLUMN id SET DEFAULT nextval('public.page_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_pagecomment ALTER COLUMN id SET DEFAULT nextval('public.page_pagecomment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_pageimage ALTER COLUMN id SET DEFAULT nextval('public.page_pageimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product ALTER COLUMN id SET DEFAULT nextval('public.product_product_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_catalog ALTER COLUMN id SET DEFAULT nextval('public.product_product_catalog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_recommend_products ALTER COLUMN id SET DEFAULT nextval('public.product_product_recommend_products_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productcomment ALTER COLUMN id SET DEFAULT nextval('public.product_productcomment_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productimage ALTER COLUMN id SET DEFAULT nextval('public.product_productimage_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productitem ALTER COLUMN id SET DEFAULT nextval('public.product_productitem_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_blogs ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_blogs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_catalog ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_catalog_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_galleries ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_galleries_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_list_link ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_list_link_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_page ALTER COLUMN id SET DEFAULT nextval('public.settings_template_footer_page_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_settingstemplate ALTER COLUMN id SET DEFAULT nextval('public.settings_template_settingstemplate_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_listlink ALTER COLUMN id SET DEFAULT nextval('public.site_info_listlink_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_socialnetwork ALTER COLUMN id SET DEFAULT nextval('public.site_info_socialnetwork_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_textinfo ALTER COLUMN id SET DEFAULT nextval('public.site_info_textinfo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.users_userlink ALTER COLUMN id SET DEFAULT nextval('public.users_userlink_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.users_userprofile ALTER COLUMN id SET DEFAULT nextval('public.users_userprofile_id_seq'::regclass);


--
-- Data for Name: advertising_sliderhome; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.advertising_sliderhome (id, title, description, image, url, sort) FROM stdin;
1	Изысканный дизайн		слайдер/zona_pattern.jpg	\N	3000
2	Свежие идеи		слайдер/les5.jpg	\N	2000
3	Уютная атмосфера		слайдер/les12.jpg	\N	4000
4	Гарантия качества		слайдер/111.jpg	\N	1000
\.


--
-- Name: advertising_sliderhome_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.advertising_sliderhome_id_seq', 4, true);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add FileBrowser	1	add_filebrowser
2	Can change FileBrowser	1	change_filebrowser
3	Can delete FileBrowser	1	delete_filebrowser
4	Can add log entry	2	add_logentry
5	Can change log entry	2	change_logentry
6	Can delete log entry	2	delete_logentry
7	Can add site	3	add_site
8	Can change site	3	change_site
9	Can delete site	3	delete_site
10	Can add group	4	add_group
11	Can change group	4	change_group
12	Can delete group	4	delete_group
13	Can add user	5	add_user
14	Can change user	5	change_user
15	Can delete user	5	delete_user
16	Can add permission	6	add_permission
17	Can change permission	6	change_permission
18	Can delete permission	6	delete_permission
19	Can add content type	7	add_contenttype
20	Can change content type	7	change_contenttype
21	Can delete content type	7	delete_contenttype
22	Can add session	8	add_session
23	Can change session	8	change_session
24	Can delete session	8	delete_session
25	Can add слайды	9	add_sliderhome
26	Can change слайды	9	change_sliderhome
27	Can delete слайды	9	delete_sliderhome
28	Can add Пост	10	add_post
29	Can change Пост	10	change_post
30	Can delete Пост	10	delete_post
31	Can add Фотографию	11	add_postimage
32	Can change Фотографию	11	change_postimage
33	Can delete Фотографию	11	delete_postimage
34	Can add Коментарий	12	add_comment
35	Can change Коментарий	12	change_comment
36	Can delete Коментарий	12	delete_comment
37	Can add Блог	13	add_blog
38	Can change Блог	13	change_blog
39	Can delete Блог	13	delete_blog
40	Can add Фотографию	14	add_blogimage
41	Can change Фотографию	14	change_blogimage
42	Can delete Фотографию	14	delete_blogimage
43	Can add Фотографию	15	add_galleryimage
44	Can change Фотографию	15	change_galleryimage
45	Can delete Фотографию	15	delete_galleryimage
46	Can add Галерею	16	add_gallery
47	Can change Галерею	16	change_gallery
48	Can delete Галерею	16	delete_gallery
49	Can add Фотографию	17	add_catalogimage
50	Can change Фотографию	17	change_catalogimage
51	Can delete Фотографию	17	delete_catalogimage
52	Can add Категорию	18	add_catalog
53	Can change Категорию	18	change_catalog
54	Can delete Категорию	18	delete_catalog
55	Can add Коментарий	19	add_productcomment
56	Can change Коментарий	19	change_productcomment
57	Can delete Коментарий	19	delete_productcomment
58	Can add товар	20	add_product
59	Can change товар	20	change_product
60	Can delete товар	20	delete_product
61	Can add Фотографию	21	add_productimage
62	Can change Фотографию	21	change_productimage
63	Can delete Фотографию	21	delete_productimage
64	Can add Вариант продукта	22	add_productitem
65	Can change Вариант продукта	22	change_productitem
66	Can delete Вариант продукта	22	delete_productitem
67	Can add Историю	23	add_story
68	Can change Историю	23	change_story
69	Can delete Историю	23	delete_story
70	Can add Статус заказа	24	add_status
71	Can change Статус заказа	24	change_status
72	Can delete Статус заказа	24	delete_status
73	Can add пункт заказа	25	add_orderitem
74	Can change пункт заказа	25	change_orderitem
75	Can delete пункт заказа	25	delete_orderitem
76	Can add заказ	26	add_order
77	Can change заказ	26	change_order
78	Can delete заказ	26	delete_order
79	Can add user profile	27	add_userprofile
80	Can change user profile	27	change_userprofile
81	Can delete user profile	27	delete_userprofile
82	Can add user link	28	add_userlink
83	Can change user link	28	change_userlink
84	Can delete user link	28	delete_userlink
85	Can add Фото	29	add_homeimage
86	Can change Фото	29	change_homeimage
87	Can delete Фото	29	delete_homeimage
88	Can add главная страница	30	add_home
89	Can change главная страница	30	change_home
90	Can delete главная страница	30	delete_home
91	Can add пункт главного меню	31	add_mainmenu
92	Can change пункт главного меню	31	change_mainmenu
93	Can delete пункт главного меню	31	delete_mainmenu
94	Can add страница	32	add_page
95	Can change страница	32	change_page
96	Can delete страница	32	delete_page
97	Can add Коментарий	33	add_pagecomment
98	Can change Коментарий	33	change_pagecomment
99	Can delete Коментарий	33	delete_pagecomment
100	Can add Фотографию	34	add_pageimage
101	Can change Фотографию	34	change_pageimage
102	Can delete Фотографию	34	delete_pageimage
103	Can add быстрая ссылка	35	add_listlink
104	Can change быстрая ссылка	35	change_listlink
105	Can delete быстрая ссылка	35	delete_listlink
106	Can add текстовая информация	36	add_textinfo
107	Can change текстовая информация	36	change_textinfo
108	Can delete текстовая информация	36	delete_textinfo
109	Can add социальная сеть	37	add_socialnetwork
110	Can change социальная сеть	37	change_socialnetwork
111	Can delete социальная сеть	37	delete_socialnetwork
112	Can add футер	38	add_footer
113	Can change футер	38	change_footer
114	Can delete футер	38	delete_footer
115	Can add настройку шаблона	39	add_settingstemplate
116	Can change настройку шаблона	39	change_settingstemplate
117	Can delete настройку шаблона	39	delete_settingstemplate
118	Can add Включаемую область	40	add_includearea
119	Can change Включаемую область	40	change_includearea
120	Can delete Включаемую область	40	delete_includearea
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 120, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
2	pbkdf2_sha256$36000$k4XzvcxcBlng$wixbqn+m7esY7U3OHpbxlMgypweRPc3Yt1sMtgZVJOI=	2018-03-08 13:45:25.963462+00	f	marychev_mihail@mail.ru			marychev_mihail@mail.ru	f	t	2018-03-08 13:45:25.632721+00
1	pbkdf2_sha256$36000$EaOP80VivIa1$S/acSC8eIVnEe8/cOQF9coQMr+LDihtM7S4WEBowU88=	2018-03-11 07:17:40.968173+00	t	marychev	Михаил	Марычев	marychev@garpix.com	t	t	2018-03-03 21:32:11.686929+00
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 2, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Data for Name: blog_blog; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.blog_blog (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, lft, rght, tree_id, level, author_id, parent_id) FROM stdin;
1	Услуги	Индивидуальный и отличный продукт из массива - это детальный замер,\r\nвнимательная разработка, рождение изделия, тщательный монтаж!\r\nС Нами Ваш дом оживёт!	uslugi	Услуги	Услуги	Услуги	ru_RU	\N	2018-02-21 18:06:02.164+00	2018-02-21 20:48:28.107+00	<p><span style="font-family:Arial,Helvetica,sans-serif">Индивидуальный и отличный продукт из массива - это детальный замер, внимательная разработка, рождение изделия, тщательный монтаж! С Нами Ваш дом оживёт!</span></p>\r\n\r\n<p>Все&nbsp;<a href="http://dervek.ru/portfolio" title="лестницы, кухни, перегородки, порталы, двери, интерьеры, и многое другое из массива дуба, березы, бука, сосны, ясеня">свои столярные изделия</a>&nbsp;мы прорабатываем и изготовляем индивидуально. Вы можете позвонить (тел. 8-915-832-3836 ) или&nbsp;<strong><a href="mailto:revcloud@mail.ru" target="_blank" title="отправить письмо со своего почтового ящика">отправить письмо</a></strong>&nbsp;(<a href="http://dervek.ru/contact/#letter" title="отправить письмо с сайта Dervеk, без регистраций!">быстрое письмо&nbsp;</a>с сайта), так же отправить&nbsp;<a href="http://dervek.ru/#request" title="внизу страницы просто заполните форму">заявку</a>&nbsp;и получить консультацию, или ответ на интересующий вопрос. Мы не оставим Вас без внимания! Звоните и пишите нам!&quot;</p>	1000	t	1	4	1	0	\N	\N
3	Вакансии	Наше производство растет и процветает уже более 20 лет!\r\nМы можем предложить рабочие места для следующих специалистов:	vakansii	Вакансии	Вакансии	Вакансии	ru_RU	\N	2018-02-22 18:20:59.863+00	2018-02-22 18:22:37.355+00	<p>Наше производство растет и процветает уже более 20 лет!<br />\r\nМы можем предложить рабочие места для следующих специалистов:</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Наше производство растет и процветает уже более 20 лет!<br />\r\nМы можем предложить рабочие места для следующих специалистов:</p>\r\n\r\n<p>&nbsp;</p>	1000	t	1	2	2	0	\N	\N
2	Замер		zamer	Замер	Замер	Замер	ru_RU		2018-02-21 18:08:21.02+00	2018-03-03 23:26:19.631916+00	<p><em>&rdquo;Семь раз отмерь - один отрежь&rdquo;</em>&nbsp;- так и живет старая-добрая поговорка. Естественно, ведь замер, самый важный и главный этап. Отсюда начинает развиваться вся цепочка событий. Зарисовки, чертежи, заготовки - везде используются размеры. Большое количество ошибок, возникающих при монтаже&nbsp;<strong>столярной продукции</strong>&nbsp;- как правило, недобросовестный замер (особенно! если это лестница). При замере не следует отвлекать замерщика вопросами, советами или любыми другими задушевными беседами. Все пожелания&nbsp;<strong>до и после замера!</strong>&nbsp;Если замерщик измеряя, поднимает Вам настроение - вспомните, вышеописанное.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Не всегда задуманный визуальный вариант&nbsp;<strong>деревянной лестницы, мебели, или кухни&nbsp;</strong>оказывается таковым. Ссылаясь на практику, можно смело сказать, что при замере выявляются неприятные явления:&nbsp;<em>&quot;тут труба, придется подвинуть, тут стена неровная и т.д.&quot;</em>&nbsp;- это все ведет к изменению размеров. А как же учет&nbsp;<strong>комфортной эксплуатации продукта!?</strong>&nbsp;Не будешь же постоянно головой биться об кухонную дверцу, или семенить гусиными шагами по лестнице на 4й этаж.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Эти нюансы, уже на ранних этапах, тщательно нами прорабатываются, чтобы в последствии Вы пользовались&nbsp;<strong>удобной и прочной продукцией от&nbsp;<a href="http://dervek.ru/" target="_blank" title="столярное производство">столярной фирмы Dervek!</a></strong></p>	1000	f	2	3	1	1	\N	1
\.


--
-- Name: blog_blog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.blog_blog_id_seq', 3, true);


--
-- Data for Name: blog_blogimage; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.blog_blogimage (id, image, image_title, image_is_main, image_description, blog_id) FROM stdin;
2	услуги/e1bb60bfa1c0c44c41ac622cab37870e.jpg	\N	f		1
3	вакансии/Svoĭ-biznes-stolyarnyĭ-cekh-750x500.jpg	\N	f		3
1	услуги/zam.png	\N	f		2
\.


--
-- Name: blog_blogimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.blog_blogimage_id_seq', 3, true);


--
-- Data for Name: blog_comment; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.blog_comment (id, created, updated, text, ip_address, username, email, is_show, post_id, user_id) FROM stdin;
\.


--
-- Name: blog_comment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.blog_comment_id_seq', 1, false);


--
-- Data for Name: blog_post; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.blog_post (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, is_allow_comments, comment_count, author_id, blog_id) FROM stdin;
1	Замер	”Семь раз отмерь - один отрежь” - так и живет старая-добрая поговорка. Естественно, ведь замер, самый важный и главный этап. Отсюда начинает развиваться вся цепочка событий. Зарисовки, чертежи, заготовки - везде используются размеры. Большое количество ошибок, возникающих при монтаже столярной продукции - как правило, недобросовестный замер (особенно! если это лестница). При замере не следует отвлекать замерщика вопросами, советами или любыми другими задушевными беседами. Все пожелания до и после замера! Если замерщик измеряя, поднимает Вам настроение - вспомните, вышеописанное.	zamer	Замер	Замер	Замер	ru_RU	\N	2018-02-20 19:25:32.365+00	2018-02-25 06:44:35.956+00	<p><em>&rdquo;Семь раз отмерь - один отрежь&rdquo;</em>&nbsp;- так и живет старая-добрая поговорка. Естественно, ведь замер, самый важный и главный этап. Отсюда начинает развиваться вся цепочка событий. Зарисовки, чертежи, заготовки - везде используются размеры. Большое количество ошибок, возникающих при монтаже&nbsp;<strong>столярной продукции</strong>&nbsp;- как правило, недобросовестный замер (особенно! если это лестница). При замере не следует отвлекать замерщика вопросами, советами или любыми другими задушевными беседами. Все пожелания&nbsp;<strong>до и после замера!</strong>&nbsp;Если замерщик измеряя, поднимает Вам настроение - вспомните, вышеописанное.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Не всегда задуманный визуальный вариант&nbsp;<strong>деревянной лестницы, мебели, или кухни&nbsp;</strong>оказывается таковым. Ссылаясь на практику, можно смело сказать, что при замере выявляются неприятные явления:&nbsp;<em>&quot;тут труба, придется подвинуть, тут стена неровная и т.д.&quot;</em>&nbsp;- это все ведет к изменению размеров. А как же учет&nbsp;<strong>комфортной эксплуатации продукта!?</strong>&nbsp;Не будешь же постоянно головой биться об кухонную дверцу, или семенить гусиными шагами по лестнице на 4й этаж.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Эти нюансы, уже на ранних этапах, тщательно нами прорабатываются, чтобы в последствии Вы пользовались&nbsp;<strong>удобной и прочной продукцией от&nbsp;<a href="http://dervek.ru/" target="_blank" title="столярное производство">столярной фирмы Dervek!</a></strong></p>	1000	t	t	3	1	1
2	Разработка	Как описывают уже готовый дизайн помещения люди? Конечно же, ссылаются на увиденные вещи, которые находятся в нем, а так же цвет стен, потолка, пола и т.д. Лестница, ведущая на 2й этаж - это уже 30-40% дизайна всего дома (если он 2х-этажный, в данном случае).\r\n    Зная интерес и вкус клиента мы всегда сможем предложить несколько вариантов эксклюзивного дизайна. Мы учитываем все нюансы и говорим о реальных возможностях, оценив окружающую обстановку и технические характеристики помещения.\r\n    Работа над проектом проходит и художника, и дизайнера, и технолога. Это исключает право на ошибку. Вы сможете увидеть проект из массива в 3D-виде на мониторе, прокрутив его как захочется, разглядеть любую мелочь, которая Вам интересна.\r\n    Определить дизайн дома, квартиры или любого другого помещения с Нами покажется забавной и незаурядной игрой, не говоря уже о какой-то определенной конструкции. Декоративное старение, имитация определенного вида дерева, всевозможное комбинирование элементов, и т.п. Используется все, чтобы заказчик остался доволен после работы с нами!	razrabotka	Разработка	Разработка	Разработка	ru_RU	\N	2018-02-21 19:15:25.075+00	2018-03-03 12:05:38.335+00	<p style="text-align:justify">&nbsp;Как описывают уже&nbsp;<strong>готовый дизайн помещения</strong>&nbsp;люди? Конечно же, ссылаются на увиденные вещи, которые находятся в нем, а так же цвет стен, потолка, пола и т.д.&nbsp;<strong>Лестница</strong>, ведущая на 2й этаж - это уже 30-40% дизайна всего дома (если он 2х-этажный, в данном случае).<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Зная интерес и вкус клиента мы всегда сможем предложить несколько вариантов&nbsp;эксклюзивного дизайна.&nbsp;Мы учитываем все нюансы и говорим о реальных возможностях, оценив окружающую обстановку и технические характеристики помещения.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Работа над проектом проходит и&nbsp;<strong>художника, и дизайнера, и технолога</strong>. Это исключает право на ошибку. Вы сможете увидеть&nbsp;<strong>проект из массива в 3D-виде</strong>&nbsp;на мониторе, прокрутив его как захочется, разглядеть любую мелочь, которая Вам интересна.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Определить дизайн дома, квартиры или любого другого помещения с&nbsp;Нами&nbsp;покажется забавной и незаурядной игрой, не говоря уже о какой-то определенной конструкции.&nbsp;Декоративное старение, имитация определенного вида дерева, всевозможное комбинирование элементов, и т.п.Используется все, чтобы заказчик остался доволен после работы с нами!</p>	1000	t	t	3	\N	1
3	Изготовление	Наличие собственных паровых сушилок позволяет нам отслеживать состояние материала ежедневно. После сушки материал отлеживается, как минимум, 40 дней, для нормализации и снятия с него напряжения. Применяем собственный опыт и технические спецификации.\r\n    Подготовка рабочего места также не маловажное мероприятие. Продукция почти вс	izgotovlenie	Изготовление	Изготовление	Изготовление	ru_RU	\N	2018-02-21 19:22:19.222+00	2018-02-24 10:37:18.829+00	<p>Наличие собственных&nbsp;<strong>паровых сушилок</strong>&nbsp;позволяет нам отслеживать состояние материала ежедневно. После сушки материал отлеживается, как минимум, 40 дней, для нормализации и снятия с него напряжения. Применяем собственный опыт и технические спецификации.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;<strong>Подготовка рабочего</strong>&nbsp;места также не маловажное мероприятие. Продукция почти всегда разная, соответственно нужны определенные условия для каждой конструкции. Много факторов влияет на реализацию этого шага. Например, если изделие нуждается в сборке на месте, то это и происходит, иначе - при монтаже. От рабочего места зависит многое, даже психологический настрой работника. Мы следим за этим!<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Древесина проходит&nbsp;<strong>предварительный отбор, сортируется</strong>&nbsp;по многочисленным показателям и отправляется на&nbsp;<strong>заготовку</strong>&nbsp;в распиловочный цех. Тут строго по размерам она приобретает то, из чего потом уже рождается изделие.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;<strong>Склейка</strong>&nbsp;- тонкая и чувствительная операция. Используем&nbsp;<em>только профессиональные столярные клеи.</em>&nbsp;Учитываем вид массива, материала и среду, где будет находится продукция. Индивидуальное решение почти всегда, а это значит в состав изделия может быть включен металл, ковка, стекло, камень и многое другое.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Для&nbsp;<strong>покраски продукции из разнообразного массива древесины</strong>&nbsp;используется исключительно&nbsp;<strong>иностранные производители лакокрасочных материалов!</strong>&nbsp;Применяются&nbsp;<strong>шлифовальные, полировочные лаки и красители, а так же спец.эффекты,</strong>&nbsp;которые в последнее время очень популярны. Они поражают разнообразием и позволяют творить и воплощать по истине&nbsp;супер продукцию!</p>	1000	t	t	1	\N	1
4	Монтаж	Когда все готово к монтажу столярного изделия мы собираемся в путь, не зависимый от географического положения клиента. Каждый мастер отвечает за закрепленный за ним проект от его начало и до конца работы. Соответственно, он так же присутствует и контролирует установку изделия. Иногда на это уходит день, иногда целые недели, исходя из общего объема и города, где будет в дальнейшем жить клиент и наше детище. На месте может быть сделаны как и дополнительный инструмент, так же и любые конструкции, которые будут необходимы для монтажа.	montazh	Монтаж	Монтаж	Монтаж	ru_RU	\N	2018-02-21 20:24:50.627+00	2018-03-03 11:47:21.972+00	<p>Когда все готово к&nbsp;<strong>монтажу столярного изделия</strong>&nbsp;мы собираемся в путь, не зависимый от географического положения клиента. Каждый мастер отвечает за закрепленный за ним проект от его начало и до конца работы. Соответственно, он так же присутствует и контролирует установку изделия. Иногда на это уходит день, иногда целые недели, исходя из общего объема и города, где будет в дальнейшем жить клиент и наше детище. На месте может быть сделаны как и дополнительный инструмент, так же и любые конструкции, которые будут необходимы для&nbsp;<strong>монтажа</strong>.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Проще говоря, за спиной колоссальный опыт, который позволяет нам сделать на месте, то что может понадобится внезапно. То есть мы уже просматриваем все возможные казусы, которые могут возникнуть и берем с собой все необходимое.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;<strong>Монтаж</strong>&nbsp;- ответственная и тонкая грань, между эксплуатацией и довольным заказчиком. Чтобы при доставке продукция не поцарапалась и не пришла в негодный вид, мы очень тщательно ее фиксируем, плюс мягкая упаковка каждой детали. Это исключает любые повреждения, которые может приготовить нам&nbsp;<em>&quot;русская дорога&quot;</em>.</p>	1000	t	t	6	\N	1
5	Столяр	Обязанности: Изготовление мебели, лестниц, дверных блоков и различных элементов интерьера по индивидуальным проектам, из массива, шпона, МДФ.	stolyar	Столяр	Столяр	Столяр	ru_RU	\N	2018-02-22 18:24:17.22+00	2018-02-24 10:36:33.543+00	<p><strong>Обязанности:</strong>&nbsp;<em>Изготовление мебели, лестниц, дверных блоков и различных элементов интерьера по индивидуальным проектам, из массива, шпона, МДФ.</em><br />\r\n<strong>Требования:</strong>&nbsp;<em>Знание различных пород древесины и их свойств. Правила пользования ручным и электрическим столярным инструментом. Чтение и работа с исполнительными чертежами. Аккуратность, порядочность.</em><br />\r\n<strong>Условия:</strong>&nbsp;<em>Обучение в процессе работы. Работа в бригаде. График работы 08.00 - 17.00 (5/2) Оклад по результатам собеседования. Оформление по ТК РФ.</em></p>	1000	t	t	1	\N	3
6	Маляр	Обязанности: Покрытие лакокрасочными материалами столярных изделий, изготовленных из МДФ, массива, шпона. Использование двухкомпонентных лаков, эмалей. Применение 3D технологий.	malyar	Маляр	Маляр	Маляр	ru_RU	\N	2018-02-22 18:51:46.826+00	2018-03-03 12:05:59.136+00	<p><strong>Обязанности:</strong>&nbsp;<em>Покрытие лакокрасочными материалами столярных изделий, изготовленных из МДФ, массива, шпона. Использование двухкомпонентных лаков, эмалей. Применение 3D технологий.</em><br />\r\n<strong>Требования:</strong>&nbsp;<em>Обязателен опыт работы в столярном цехе. Аккуратность, порядочность.</em><br />\r\n<strong>Условия:</strong>&nbsp;<em>Возможно обучение в процессе работы. График работы 08.00 - 17.00 (5/2) Оклад по результатам собеседования. Оформление по ТК РФ.</em></p>	1000	t	t	2	\N	3
\.


--
-- Name: blog_post_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.blog_post_id_seq', 6, true);


--
-- Data for Name: blog_postimage; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.blog_postimage (id, image, image_title, image_is_main, image_description, post_id) FROM stdin;
1	услуги/lest2.gif	\N	f		1
2	услуги/381464508_w640_h2048_photo9_01.jpg	\N	f		2
3	услуги/mainHeader.jpg	\N	f		3
4	услуги/derevyannaya_bitovka_H000acdbf_1180651.jpg	\N	f		4
5	вакансии/94_carpenter_03.jpg	\N	f		5
6	вакансии/malar.jpg	\N	f		6
\.


--
-- Name: blog_postimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.blog_postimage_id_seq', 6, true);


--
-- Data for Name: catalog_catalog; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.catalog_catalog (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, lft, rght, tree_id, level, author_id, parent_id) FROM stdin;
6	11Портфолио	\N	11portfolio	Портфолио	Портфолио	Портфолио	ru_RU	\N	2018-02-17 18:24:14.601+00	2018-03-10 13:13:29.279251+00		2000	f	1	2	2	0	\N	\N
7	ПОРТФОЛИО	\N	portfolio	ПОРТ	ПОРТ	ПОРТ	ru_RU		2018-02-17 18:39:48.202+00	2018-03-10 14:37:56.155058+00		1000	t	1	12	1	0	\N	\N
3	Интерьеры	\N	interery	Интерьеры	Интерьеры	Интерьеры	ru_RU		2018-02-17 18:24:14.601+00	2018-03-10 14:38:47.604623+00		1400	t	8	9	1	1	\N	7
5	Декор	\N	dekor	Декор	Декор	Декор	ru_RU		2018-02-17 18:24:14.601+00	2018-03-10 14:42:22.34532+00		1500	t	10	11	1	1	\N	7
1	Лестницы	\N	lestnicy	Лестницы	Лестницы	Лестницы	ru_RU		2018-02-17 18:24:14.601+00	2018-03-10 13:27:57.911276+00		1100	t	2	3	1	1	\N	7
2	Кухни	\N	kuhni	Кухни	Кухни	Кухни	ru_RU		2018-02-17 18:24:14.601+00	2018-03-10 13:47:50.730017+00		1300	t	6	7	1	1	\N	7
4	Мебель	\N	mebel	Мебель	Мебель	Мебель	ru_RU		2018-02-17 18:24:14.601+00	2018-03-11 05:00:54.188917+00		1200	t	4	5	1	1	\N	7
\.


--
-- Name: catalog_catalog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.catalog_catalog_id_seq', 7, true);


--
-- Data for Name: catalog_catalogimage; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.catalog_catalogimage (id, image, image_title, image_is_main, image_description, catalog_id) FROM stdin;
1	портфолио/лестницы/ab5742e3dd38874fa3dbb1213ad3bdb9.JPG	Лестницы	t		1
4	портфолио/кухни/3bd9740f1b08e546d3b3d7f3d1fa3ead.JPG	Кухни	t		2
2	портфолио/декор/photo2014_14.JPG	ПОРТФОЛИО	t		7
5	портфолио/мебель/photo2014_10.JPG	Мебель	t		4
6	портфолио/интерьеры/photo2014_19.JPG	Интерьеры	t		3
3	портфолио/декор/035cb8bab693269ac485e05118a9a5ff.jpg	Декор	t		5
\.


--
-- Name: catalog_catalogimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.catalog_catalogimage_id_seq', 6, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-03-03 23:01:57.076841+00	1	marychev	2	[{"changed": {"fields": ["username", "first_name", "last_name", "email"]}}]	5	1
2	2018-03-03 23:06:33.363653+00	1	О нас	2	[{"changed": {"fields": ["html"]}}, {"changed": {"object": "PageImage object", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	32	1
3	2018-03-03 23:08:13.006086+00	1	О нас	2	[{"changed": {"fields": ["html"]}}, {"changed": {"object": "PageImage object", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	32	1
4	2018-03-03 23:11:36.36264+00	1	О нас	2	[{"changed": {"fields": ["html"]}}, {"changed": {"object": "PageImage object", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	32	1
5	2018-03-03 23:13:09.648603+00	1	О нас	2	[{"changed": {"fields": ["html"]}}, {"changed": {"object": "PageImage object", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	32	1
6	2018-03-03 23:13:46.355829+00	1	О нас	2	[{"changed": {"fields": ["description", "html"]}}, {"changed": {"object": "PageImage object", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	32	1
7	2018-03-03 23:14:33.34098+00	1	О нас	2	[{"changed": {"fields": ["html"]}}, {"changed": {"object": "PageImage object", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	32	1
8	2018-03-03 23:15:07.26081+00	1	О нас	2	[{"changed": {"fields": ["html"]}}, {"changed": {"object": "PageImage object", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	32	1
9	2018-03-03 23:18:18.993647+00	2	Contact	2	[{"changed": {"fields": ["html"]}}]	32	1
10	2018-03-03 23:19:34.584642+00	2	Contact	2	[{"changed": {"fields": ["html"]}}]	32	1
11	2018-03-03 23:20:19.31583+00	2	Контакт	2	[{"changed": {"fields": ["title"]}}]	32	1
12	2018-03-03 23:20:51.519315+00	2	Контакт	2	[{"changed": {"fields": ["slug", "seo_title", "seo_description"]}}]	32	1
13	2018-03-03 23:25:58.325013+00	2	Замер	2	[{"changed": {"fields": ["is_show"]}}, {"changed": {"object": "\\u0417\\u0430\\u043c\\u0435\\u0440", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	13	1
14	2018-03-03 23:26:19.686004+00	2	Замер	2	[{"changed": {"fields": ["is_show"]}}]	13	1
15	2018-03-06 19:41:25.882882+00	1	тестова ссылка	1	[{"added": {}}]	35	1
16	2018-03-07 20:11:09.162109+00	1	Мы в контакте	2	[{"changed": {"fields": ["image", "url"]}}]	37	1
17	2018-03-07 20:56:27.877322+00	1	Мы в контакте	2	[{"changed": {"fields": ["image"]}}]	37	1
18	2018-03-07 21:09:52.030392+00	1	Мы в контакте	2	[{"changed": {"fields": ["image"]}}]	37	1
19	2018-03-08 11:31:59.724663+00	2	Воплощаем сказку в реальность!	1	[{"added": {}}]	37	1
20	2018-03-08 11:42:11.778594+00	4	Заказ: 4	1	[{"added": {}}, {"added": {"name": "\\u043f\\u0443\\u043d\\u043a\\u0442 \\u0437\\u0430\\u043a\\u0430\\u0437\\u0430", "object": "3"}}, {"added": {"name": "\\u043f\\u0443\\u043d\\u043a\\u0442 \\u0437\\u0430\\u043a\\u0430\\u0437\\u0430", "object": "4"}}]	26	1
21	2018-03-08 11:42:24.550024+00	4	Заказ: 4	2	[{"changed": {"fields": ["price"], "name": "\\u043f\\u0443\\u043d\\u043a\\u0442 \\u0437\\u0430\\u043a\\u0430\\u0437\\u0430", "object": "4"}}]	26	1
22	2018-03-08 11:57:58.127043+00	1	Нижний футер	2	[{"changed": {"fields": ["catalog", "page", "text_info", "list_link"]}}]	38	1
23	2018-03-08 12:20:04.347112+00	1	О нас	2	[{"changed": {"object": "PageImage object", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	32	1
24	2018-03-08 12:22:37.61089+00	1	localhost:8000	2	[{"changed": {"fields": ["domain", "name"]}}]	3	1
25	2018-03-08 12:32:01.74608+00	1	Нижний футер	2	[{"changed": {"fields": ["page"]}}]	38	1
26	2018-03-08 12:59:15.873911+00	1	Нижний футер	2	[{"changed": {"fields": ["galleries", "blogs"]}}]	38	1
27	2018-03-08 13:09:11.800019+00	1	Нижний футер	2	[{"changed": {"fields": ["catalog"]}}]	38	1
28	2018-03-08 13:09:55.685863+00	1	Нижний футер	2	[{"changed": {"fields": ["page"]}}]	38	1
29	2018-03-08 13:10:37.215391+00	1	Нижний футер	2	[{"changed": {"fields": ["galleries", "blogs", "list_link"]}}]	38	1
30	2018-03-08 13:11:11.961157+00	1	Нижний футер	2	[{"changed": {"fields": ["catalog", "page", "galleries", "blogs"]}}]	38	1
31	2018-03-10 08:56:25.479997+00	9	Лестница, дверь, массив	2	[{"changed": {"fields": ["title", "description", "html", "catalog", "recommend_products", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"deleted": {"object": "# 111", "name": "\\u0412\\u0430\\u0440\\u0438\\u0430\\u043d\\u0442 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u0430"}}, {"deleted": {"object": "# 222", "name": "\\u0412\\u0430\\u0440\\u0438\\u0430\\u043d\\u0442 \\u043f\\u0440\\u043e\\u0434\\u0443\\u043a\\u0442\\u0430"}}]	20	1
32	2018-03-10 08:57:42.090448+00	9	Лестница, дверь, массив	2	[{"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
33	2018-03-10 08:57:59.699779+00	9	Лестница, дверь, массив	2	[{"changed": {"fields": ["articul"]}}]	20	1
92	2018-03-10 13:27:57.811025+00	5	Декор	2	[{"changed": {"fields": ["sort"]}}]	18	1
93	2018-03-10 13:27:57.855693+00	3	Интерьеры	2	[{"changed": {"fields": ["sort"]}}]	18	1
34	2018-03-10 08:59:50.083566+00	8	Лестничный марш	2	[{"changed": {"fields": ["title", "description", "html", "catalog", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0447\\u043d\\u044b\\u0439 \\u043c\\u0430\\u0440\\u0448", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0447\\u043d\\u044b\\u0439 \\u043c\\u0430\\u0440\\u0448", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
35	2018-03-10 09:00:53.269123+00	7	Лестница на больцах из дуба	2	[{"changed": {"fields": ["title", "description", "html", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u043d\\u0430 \\u0431\\u043e\\u043b\\u044c\\u0446\\u0430\\u0445 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
36	2018-03-10 09:01:21.871422+00	7	Лестница на больцах из дуба	2	[{"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u043d\\u0430 \\u0431\\u043e\\u043b\\u044c\\u0446\\u0430\\u0445 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
37	2018-03-10 09:02:36.188896+00	6	Лестница из массива сосны	2	[{"changed": {"fields": ["is_new", "title", "description", "html", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0438\\u0437 \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432\\u0430 \\u0441\\u043e\\u0441\\u043d\\u044b", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
38	2018-03-10 09:04:03.404636+00	5	Прихожая - Лестница ,дверь, массив сосна	2	[{"changed": {"fields": ["title", "description", "html", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041f\\u0440\\u0438\\u0445\\u043e\\u0436\\u0430\\u044f - \\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 ,\\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432 \\u0441\\u043e\\u0441\\u043d\\u0430", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
39	2018-03-10 09:04:05.797123+00	5	Прихожая - Лестница ,дверь, массив сосна	2	[{"changed": {"object": "\\u041f\\u0440\\u0438\\u0445\\u043e\\u0436\\u0430\\u044f - \\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 ,\\u0434\\u0432\\u0435\\u0440\\u044c, \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432 \\u0441\\u043e\\u0441\\u043d\\u0430", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
40	2018-03-10 09:05:29.69471+00	4	Лестница в квартиру	2	[{"changed": {"fields": ["is_bestseller", "is_new", "title", "description", "html", "catalog", "recommend_products", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440\\u0443", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
41	2018-03-10 09:06:01.316343+00	4	Лестница в квартиру	2	[{"changed": {"fields": ["is_new"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440\\u0443", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
42	2018-03-10 09:06:12.276627+00	4	Лестница в квартиру	2	[{"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440\\u0443", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
43	2018-03-10 09:08:14.581922+00	4	Лестница в квартиру	2	[{"changed": {"fields": ["is_bestseller"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u043a\\u0432\\u0430\\u0440\\u0442\\u0438\\u0440\\u0443", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
44	2018-03-10 09:09:31.961242+00	3	Мансардная лестница, хвоя	2	[{"changed": {"fields": ["is_new", "title", "description", "html", "catalog", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041c\\u0430\\u043d\\u0441\\u0430\\u0440\\u0434\\u043d\\u0430\\u044f \\u043b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0445\\u0432\\u043e\\u044f", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
45	2018-03-10 09:11:10.302094+00	3	Мансардная лестница, хвоя	2	[{"changed": {"object": "\\u041c\\u0430\\u043d\\u0441\\u0430\\u0440\\u0434\\u043d\\u0430\\u044f \\u043b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0445\\u0432\\u043e\\u044f", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
46	2018-03-10 09:12:30.016052+00	2	Фрагмент лестничного ограждения	2	[{"changed": {"fields": ["title", "description", "html", "catalog", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u0424\\u0440\\u0430\\u0433\\u043c\\u0435\\u043d\\u0442 \\u043b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0447\\u043d\\u043e\\u0433\\u043e \\u043e\\u0433\\u0440\\u0430\\u0436\\u0434\\u0435\\u043d\\u0438\\u044f", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
47	2018-03-10 09:13:43.336515+00	7	Лестница на больцах из дуба	2	[{"added": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u043d\\u0430 \\u0431\\u043e\\u043b\\u044c\\u0446\\u0430\\u0445 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u043d\\u0430 \\u0431\\u043e\\u043b\\u044c\\u0446\\u0430\\u0445 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430", "fields": ["image", "image_is_main"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
48	2018-03-10 09:15:41.955467+00	1	Лестница из дуба с ковкой	2	[{"changed": {"fields": ["is_new", "title", "description", "html", "catalog", "slug", "seo_title", "seo_description", "seo_keywords"]}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430 \\u0441 \\u043a\\u043e\\u0432\\u043a\\u043e\\u0439", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430 \\u0441 \\u043a\\u043e\\u0432\\u043a\\u043e\\u0439", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430 \\u0441 \\u043a\\u043e\\u0432\\u043a\\u043e\\u0439", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
49	2018-03-10 09:19:00.065569+00	10	Лестница резная, дуб	1	[{"added": {}}, {"added": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0440\\u0435\\u0437\\u043d\\u0430\\u044f, \\u0434\\u0443\\u0431", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
94	2018-03-10 13:27:57.888975+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
50	2018-03-10 09:20:46.830183+00	11	Лестница с позолотой	1	[{"added": {}}, {"added": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0441 \\u043f\\u043e\\u0437\\u043e\\u043b\\u043e\\u0442\\u043e\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
51	2018-03-10 09:22:14.832848+00	12	Ограждение лестничного марша	1	[{"added": {}}, {"added": {"object": "\\u041e\\u0433\\u0440\\u0430\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 \\u043b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0447\\u043d\\u043e\\u0433\\u043e \\u043c\\u0430\\u0440\\u0448\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
52	2018-03-10 09:22:43.697093+00	12	Ограждение лестничного марша	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"object": "\\u041e\\u0433\\u0440\\u0430\\u0436\\u0434\\u0435\\u043d\\u0438\\u0435 \\u043b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0447\\u043d\\u043e\\u0433\\u043e \\u043c\\u0430\\u0440\\u0448\\u0430", "fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
53	2018-03-10 09:24:08.465892+00	13	Лестница "отбеленный" дуб	1	[{"added": {}}, {"added": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\"\\u043e\\u0442\\u0431\\u0435\\u043b\\u0435\\u043d\\u043d\\u044b\\u0439\\" \\u0434\\u0443\\u0431", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
54	2018-03-10 09:25:28.979866+00	11	Лестница с позолотой	2	[{"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0441 \\u043f\\u043e\\u0437\\u043e\\u043b\\u043e\\u0442\\u043e\\u0439"}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image", "image_is_main"], "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0441 \\u043f\\u043e\\u0437\\u043e\\u043b\\u043e\\u0442\\u043e\\u0439"}}]	20	1
55	2018-03-10 09:25:38.158875+00	11	Лестница с позолотой	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0441 \\u043f\\u043e\\u0437\\u043e\\u043b\\u043e\\u0442\\u043e\\u0439"}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0441 \\u043f\\u043e\\u0437\\u043e\\u043b\\u043e\\u0442\\u043e\\u0439"}}]	20	1
56	2018-03-10 09:27:28.373088+00	14	Лестница из дуба	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430"}}]	20	1
57	2018-03-10 09:28:20.862184+00	15	Лестница, дуб	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0443\\u0431"}}]	20	1
58	2018-03-10 09:29:30.292974+00	16	Лестница из массива дуб	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0438\\u0437 \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432\\u0430 \\u0434\\u0443\\u0431"}}]	20	1
59	2018-03-10 09:30:36.148127+00	17	Лестница, дверь	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c"}}]	20	1
60	2018-03-10 09:31:02.737644+00	17	Лестница, дверь	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430, \\u0434\\u0432\\u0435\\u0440\\u044c"}}]	20	1
61	2018-03-10 09:32:40.543841+00	18	Лестница в бревенчатый дом	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u0431\\u0440\\u0435\\u0432\\u0435\\u043d\\u0447\\u0430\\u0442\\u044b\\u0439 \\u0434\\u043e\\u043c"}}]	20	1
62	2018-03-10 09:33:33.386349+00	1	Лестницы	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u044b"}}]	18	1
63	2018-03-10 13:12:26.188+00	7	ПОРТФОЛИО	2	[{"changed": {"fields": ["sort"]}}]	18	1
64	2018-03-10 13:12:26.253667+00	5	Декор	2	[{"changed": {"fields": ["sort"]}}]	18	1
65	2018-03-10 13:12:26.286923+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
66	2018-03-10 13:12:26.320203+00	3	Интерьеры	2	[{"changed": {"fields": ["sort"]}}]	18	1
67	2018-03-10 13:12:26.353477+00	2	Кухни	2	[{"changed": {"fields": ["sort"]}}]	18	1
68	2018-03-10 13:12:26.386844+00	1	Лестницы	2	[{"changed": {"fields": ["sort"]}}]	18	1
69	2018-03-10 13:13:29.302978+00	6	11Портфолио	2	[{"changed": {"fields": ["sort"]}}]	18	1
70	2018-03-10 13:16:18.089568+00	5	Декор	2	[{"changed": {"fields": ["sort"]}}]	18	1
71	2018-03-10 13:16:18.133255+00	1	Лестницы	2	[{"changed": {"fields": ["sort"]}}]	18	1
72	2018-03-10 13:16:18.166507+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
73	2018-03-10 13:16:18.200065+00	2	Кухни	2	[{"changed": {"fields": ["sort"]}}]	18	1
74	2018-03-10 13:16:48.608775+00	2	Кухни	2	[{"changed": {"fields": ["sort"]}}]	18	1
75	2018-03-10 13:16:48.642762+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
76	2018-03-10 13:16:48.686497+00	3	Интерьеры	2	[{"changed": {"fields": ["sort"]}}]	18	1
77	2018-03-10 13:16:48.719791+00	7	ПОРТФОЛИО	2	[{"changed": {"fields": ["sort"]}}]	18	1
78	2018-03-10 13:16:48.753077+00	1	Лестницы	2	[{"changed": {"fields": ["sort"]}}]	18	1
79	2018-03-10 13:16:48.786316+00	5	Декор	2	[{"changed": {"fields": ["sort"]}}]	18	1
80	2018-03-10 13:23:39.062422+00	2	Кухни	2	[{"changed": {"fields": ["sort"]}}]	18	1
81	2018-03-10 13:23:39.092884+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
82	2018-03-10 13:23:39.126189+00	3	Интерьеры	2	[{"changed": {"fields": ["sort"]}}]	18	1
83	2018-03-10 13:23:39.237365+00	1	Лестницы	2	[{"changed": {"fields": ["sort"]}}]	18	1
84	2018-03-10 13:24:45.347342+00	7	ПОРТФОЛИО	2	[{"changed": {"fields": ["sort"]}}]	18	1
85	2018-03-10 13:24:45.38065+00	2	Кухни	2	[{"changed": {"fields": ["sort"]}}]	18	1
86	2018-03-10 13:24:45.41402+00	5	Декор	2	[{"changed": {"fields": ["sort"]}}]	18	1
87	2018-03-10 13:24:45.4584+00	3	Интерьеры	2	[{"changed": {"fields": ["sort"]}}]	18	1
88	2018-03-10 13:24:45.491669+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
89	2018-03-10 13:24:45.524968+00	1	Лестницы	2	[{"changed": {"fields": ["sort"]}}]	18	1
90	2018-03-10 13:24:53.626154+00	5	Декор	2	[{"changed": {"object": "\\u0414\\u0435\\u043a\\u043e\\u0440", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	18	1
91	2018-03-10 13:27:57.777771+00	2	Кухни	2	[{"changed": {"fields": ["sort"]}}]	18	1
95	2018-03-10 13:27:57.922392+00	1	Лестницы	2	[{"changed": {"fields": ["sort"]}}]	18	1
96	2018-03-10 13:32:48.863461+00	19	Кухня с выходом на балкон	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
97	2018-03-10 13:33:18.744167+00	19	Кухня с выходом на балкон	2	[{"changed": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image", "image_is_main"]}}, {"changed": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}, {"changed": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}, {"changed": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0441 \\u0432\\u044b\\u0445\\u043e\\u0434\\u043e\\u043c \\u043d\\u0430 \\u0431\\u0430\\u043b\\u043a\\u043e\\u043d", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	20	1
98	2018-03-10 13:39:55.901515+00	20	Кухня в деревянном доме	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0432 \\u0434\\u0435\\u0440\\u0435\\u0432\\u044f\\u043d\\u043d\\u043e\\u043c \\u0434\\u043e\\u043c\\u0435", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
99	2018-03-10 13:40:36.2588+00	21	Кухня из дуба	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
100	2018-03-10 13:41:24.474812+00	22	Кухонный гарнитур	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0433\\u0430\\u0440\\u043d\\u0438\\u0442\\u0443\\u0440", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
101	2018-03-10 13:42:14.362935+00	23	Кухня классическая	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u043a\\u043b\\u0430\\u0441\\u0441\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
102	2018-03-10 13:43:28.187829+00	24	Кухонный гарнитур, орех	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0433\\u0430\\u0440\\u043d\\u0438\\u0442\\u0443\\u0440, \\u043e\\u0440\\u0435\\u0445", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
103	2018-03-10 13:44:23.711767+00	25	Кухня классика	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u043a\\u043b\\u0430\\u0441\\u0441\\u0438\\u043a\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
104	2018-03-10 13:45:35.927512+00	26	Кухня в деревенском доме	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0443\\u0445\\u043d\\u044f \\u0432 \\u0434\\u0435\\u0440\\u0435\\u0432\\u0435\\u043d\\u0441\\u043a\\u043e\\u043c \\u0434\\u043e\\u043c\\u0435", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
105	2018-03-10 13:47:50.734088+00	2	Кухни	2	[{"changed": {"object": "\\u041a\\u0443\\u0445\\u043d\\u0438", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	18	1
106	2018-03-10 13:52:40.772414+00	27	Холл и Зал заседаний	1	[{"added": {}}, {"added": {"object": "\\u0425\\u043e\\u043b\\u043b \\u0438 \\u0417\\u0430\\u043b \\u0437\\u0430\\u0441\\u0435\\u0434\\u0430\\u043d\\u0438\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"added": {"object": "\\u0425\\u043e\\u043b\\u043b \\u0438 \\u0417\\u0430\\u043b \\u0437\\u0430\\u0441\\u0435\\u0434\\u0430\\u043d\\u0438\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
107	2018-03-10 13:53:08.811094+00	27	Холл и Зал заседаний	2	[{"changed": {"object": "\\u0425\\u043e\\u043b\\u043b \\u0438 \\u0417\\u0430\\u043b \\u0437\\u0430\\u0441\\u0435\\u0434\\u0430\\u043d\\u0438\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image", "image_is_main"]}}, {"changed": {"object": "\\u0425\\u043e\\u043b\\u043b \\u0438 \\u0417\\u0430\\u043b \\u0437\\u0430\\u0441\\u0435\\u0434\\u0430\\u043d\\u0438\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	20	1
108	2018-03-10 13:54:27.669775+00	28	Между этажами	1	[{"added": {}}, {"added": {"object": "\\u041c\\u0435\\u0436\\u0434\\u0443 \\u044d\\u0442\\u0430\\u0436\\u0430\\u043c\\u0438", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
109	2018-03-10 13:55:01.929159+00	29	Прихожая	1	[{"added": {}}, {"added": {"object": "\\u041f\\u0440\\u0438\\u0445\\u043e\\u0436\\u0430\\u044f", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
110	2018-03-10 13:55:30.957795+00	30	Двери с порталами из массива	1	[{"added": {}}, {"added": {"object": "\\u0414\\u0432\\u0435\\u0440\\u0438 \\u0441 \\u043f\\u043e\\u0440\\u0442\\u0430\\u043b\\u0430\\u043c\\u0438 \\u0438\\u0437 \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
111	2018-03-10 13:56:18.770744+00	18	Лестница в бревенчатый дом	2	[{"added": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u0431\\u0440\\u0435\\u0432\\u0435\\u043d\\u0447\\u0430\\u0442\\u044b\\u0439 \\u0434\\u043e\\u043c", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0432 \\u0431\\u0440\\u0435\\u0432\\u0435\\u043d\\u0447\\u0430\\u0442\\u044b\\u0439 \\u0434\\u043e\\u043c", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	20	1
112	2018-03-10 13:57:53.11668+00	31	Бильярдная	1	[{"added": {}}, {"added": {"object": "\\u0411\\u0438\\u043b\\u044c\\u044f\\u0440\\u0434\\u043d\\u0430\\u044f", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"added": {"object": "\\u0411\\u0438\\u043b\\u044c\\u044f\\u0440\\u0434\\u043d\\u0430\\u044f", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
156	2018-03-10 14:33:14.899189+00	61	Тумба под аппаратуру	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0422\\u0443\\u043c\\u0431\\u0430 \\u043f\\u043e\\u0434 \\u0430\\u043f\\u043f\\u0430\\u0440\\u0430\\u0442\\u0443\\u0440\\u0443"}}]	20	1
113	2018-03-10 13:58:06.528462+00	31	Бильярдная	2	[{"changed": {"object": "\\u0411\\u0438\\u043b\\u044c\\u044f\\u0440\\u0434\\u043d\\u0430\\u044f", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image", "image_is_main"]}}, {"changed": {"object": "\\u0411\\u0438\\u043b\\u044c\\u044f\\u0440\\u0434\\u043d\\u0430\\u044f", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image", "image_is_main"]}}]	20	1
114	2018-03-10 13:59:57.501789+00	32	Дверь, шкаф, детская кровать	1	[{"added": {}}, {"added": {"object": "\\u0414\\u0432\\u0435\\u0440\\u044c, \\u0448\\u043a\\u0430\\u0444, \\u0434\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f \\u043a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
115	2018-03-10 14:00:44.592068+00	33	Портал, декорации на стенах и потолке	1	[{"added": {}}, {"added": {"object": "\\u041f\\u043e\\u0440\\u0442\\u0430\\u043b, \\u0434\\u0435\\u043a\\u043e\\u0440\\u0430\\u0446\\u0438\\u0438 \\u043d\\u0430 \\u0441\\u0442\\u0435\\u043d\\u0430\\u0445 \\u0438 \\u043f\\u043e\\u0442\\u043e\\u043b\\u043a\\u0435", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
116	2018-03-10 14:01:35.732387+00	34	Порталы межкомнатные	1	[{"added": {}}, {"added": {"object": "\\u041f\\u043e\\u0440\\u0442\\u0430\\u043b\\u044b \\u043c\\u0435\\u0436\\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u043d\\u044b\\u0435", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
117	2018-03-10 14:02:23.662841+00	35	Кровать, дверь, тумба, шкаф	1	[{"added": {}}, {"added": {"object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
118	2018-03-10 14:03:15.626357+00	36	Оформление окон в спальне	1	[{"added": {}}, {"added": {"object": "\\u041e\\u0444\\u043e\\u0440\\u043c\\u043b\\u0435\\u043d\\u0438\\u0435 \\u043e\\u043a\\u043e\\u043d \\u0432 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u0435", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
119	2018-03-10 14:04:33.365786+00	37	Комната в классическом стиле	1	[{"added": {}}, {"added": {"object": "\\u041a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430 \\u0432 \\u043a\\u043b\\u0430\\u0441\\u0441\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c \\u0441\\u0442\\u0438\\u043b\\u0435", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
120	2018-03-10 14:05:36.539196+00	38	Восточная комната	1	[{"added": {}}, {"added": {"object": "\\u0412\\u043e\\u0441\\u0442\\u043e\\u0447\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
121	2018-03-10 14:06:13.870539+00	39	Детская комната	1	[{"added": {}}, {"added": {"object": "\\u0414\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
122	2018-03-10 14:06:50.722071+00	40	Декоративная-люстра в зал	1	[{"added": {}}, {"added": {"object": "\\u0414\\u0435\\u043a\\u043e\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f-\\u043b\\u044e\\u0441\\u0442\\u0440\\u0430 \\u0432 \\u0437\\u0430\\u043b", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
123	2018-03-10 14:07:50.678723+00	27	Холл и Зал заседаний	2	[{"added": {"object": "\\u0425\\u043e\\u043b\\u043b \\u0438 \\u0417\\u0430\\u043b \\u0437\\u0430\\u0441\\u0435\\u0434\\u0430\\u043d\\u0438\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}, {"changed": {"object": "\\u0425\\u043e\\u043b\\u043b \\u0438 \\u0417\\u0430\\u043b \\u0437\\u0430\\u0441\\u0435\\u0434\\u0430\\u043d\\u0438\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}, {"changed": {"object": "\\u0425\\u043e\\u043b\\u043b \\u0438 \\u0417\\u0430\\u043b \\u0437\\u0430\\u0441\\u0435\\u0434\\u0430\\u043d\\u0438\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	20	1
124	2018-03-10 14:08:44.730604+00	41	Спальная комната	1	[{"added": {}}, {"added": {"object": "\\u0421\\u043f\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
125	2018-03-10 14:09:58.728808+00	42	Арка с колонной	1	[{"added": {}}, {"added": {"object": "\\u0410\\u0440\\u043a\\u0430 \\u0441 \\u043a\\u043e\\u043b\\u043e\\u043d\\u043d\\u043e\\u0439", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
126	2018-03-10 14:11:45.444285+00	43	Детская белая комната	1	[{"added": {}}, {"added": {"object": "\\u0414\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f \\u0431\\u0435\\u043b\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
127	2018-03-10 14:12:34.61665+00	44	Коридор в деревенском стиле	1	[{"added": {}}, {"added": {"object": "\\u041a\\u043e\\u0440\\u0438\\u0434\\u043e\\u0440 \\u0432 \\u0434\\u0435\\u0440\\u0435\\u0432\\u0435\\u043d\\u0441\\u043a\\u043e\\u043c \\u0441\\u0442\\u0438\\u043b\\u0435", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
128	2018-03-10 14:13:22.636799+00	45	Детский городок	1	[{"added": {}}, {"added": {"object": "\\u0414\\u0435\\u0442\\u0441\\u043a\\u0438\\u0439 \\u0433\\u043e\\u0440\\u043e\\u0434\\u043e\\u043a", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e"}}]	20	1
129	2018-03-10 14:17:21.443863+00	46	шкаф в кабинет	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0448\\u043a\\u0430\\u0444 \\u0432 \\u043a\\u0430\\u0431\\u0438\\u043d\\u0435\\u0442"}}]	20	1
130	2018-03-10 14:18:12.599559+00	47	Обеденный стол	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041e\\u0431\\u0435\\u0434\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0441\\u0442\\u043e\\u043b"}}]	20	1
131	2018-03-10 14:18:55.628841+00	48	стол, табуретки	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441\\u0442\\u043e\\u043b, \\u0442\\u0430\\u0431\\u0443\\u0440\\u0435\\u0442\\u043a\\u0438"}}]	20	1
132	2018-03-10 14:20:36.91072+00	48	стол, табуретки, подставка под цветы	2	[{"changed": {"fields": ["title", "description", "catalog"]}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0441\\u0442\\u043e\\u043b, \\u0442\\u0430\\u0431\\u0443\\u0440\\u0435\\u0442\\u043a\\u0438, \\u043f\\u043e\\u0434\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e\\u0434 \\u0446\\u0432\\u0435\\u0442\\u044b"}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0441\\u0442\\u043e\\u043b, \\u0442\\u0430\\u0431\\u0443\\u0440\\u0435\\u0442\\u043a\\u0438, \\u043f\\u043e\\u0434\\u0441\\u0442\\u0430\\u0432\\u043a\\u0430 \\u043f\\u043e\\u0434 \\u0446\\u0432\\u0435\\u0442\\u044b"}}]	20	1
133	2018-03-10 14:21:14.583613+00	49	Шкаф домашней библиотеки	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0428\\u043a\\u0430\\u0444 \\u0434\\u043e\\u043c\\u0430\\u0448\\u043d\\u0435\\u0439 \\u0431\\u0438\\u0431\\u043b\\u0438\\u043e\\u0442\\u0435\\u043a\\u0438"}}]	20	1
134	2018-03-10 14:22:17.990812+00	50	Тумба, плинтус, кровать	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0422\\u0443\\u043c\\u0431\\u0430, \\u043f\\u043b\\u0438\\u043d\\u0442\\u0443\\u0441, \\u043a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c"}}]	20	1
135	2018-03-10 14:23:15.895828+00	51	Стол с выдвижной панелью	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0421\\u0442\\u043e\\u043b \\u0441 \\u0432\\u044b\\u0434\\u0432\\u0438\\u0436\\u043d\\u043e\\u0439 \\u043f\\u0430\\u043d\\u0435\\u043b\\u044c\\u044e"}}]	20	1
136	2018-03-10 14:24:00.73603+00	52	Гардеробная комната, бюджетный вариант	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0413\\u0430\\u0440\\u0434\\u0435\\u0440\\u043e\\u0431\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430, \\u0431\\u044e\\u0434\\u0436\\u0435\\u0442\\u043d\\u044b\\u0439 \\u0432\\u0430\\u0440\\u0438\\u0430\\u043d\\u0442"}}]	20	1
137	2018-03-10 14:24:27.933891+00	53	шкаф для посуды	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0448\\u043a\\u0430\\u0444 \\u0434\\u043b\\u044f \\u043f\\u043e\\u0441\\u0443\\u0434\\u044b"}}]	20	1
138	2018-03-10 14:24:52.997578+00	53	шкаф для посуды	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0448\\u043a\\u0430\\u0444 \\u0434\\u043b\\u044f \\u043f\\u043e\\u0441\\u0443\\u0434\\u044b"}}]	20	1
139	2018-03-10 14:25:21.159433+00	49	Шкаф домашней библиотеки	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0428\\u043a\\u0430\\u0444 \\u0434\\u043e\\u043c\\u0430\\u0448\\u043d\\u0435\\u0439 \\u0431\\u0438\\u0431\\u043b\\u0438\\u043e\\u0442\\u0435\\u043a\\u0438"}}]	20	1
140	2018-03-10 14:25:37.795706+00	47	Обеденный стол	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041e\\u0431\\u0435\\u0434\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0441\\u0442\\u043e\\u043b"}}]	20	1
141	2018-03-10 14:26:02.32315+00	41	Спальная комната	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0421\\u043f\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430"}}]	20	1
142	2018-03-10 14:26:09.782876+00	39	Детская комната	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0414\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430"}}]	20	1
143	2018-03-10 14:26:16.385195+00	37	Комната в классическом стиле	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430 \\u0432 \\u043a\\u043b\\u0430\\u0441\\u0441\\u0438\\u0447\\u0435\\u0441\\u043a\\u043e\\u043c \\u0441\\u0442\\u0438\\u043b\\u0435"}}]	20	1
144	2018-03-10 14:26:31.360484+00	36	Оформление окон в спальне	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041e\\u0444\\u043e\\u0440\\u043c\\u043b\\u0435\\u043d\\u0438\\u0435 \\u043e\\u043a\\u043e\\u043d \\u0432 \\u0441\\u043f\\u0430\\u043b\\u044c\\u043d\\u0435"}}]	20	1
145	2018-03-10 14:26:38.602809+00	32	Дверь, шкаф, детская кровать	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0414\\u0432\\u0435\\u0440\\u044c, \\u0448\\u043a\\u0430\\u0444, \\u0434\\u0435\\u0442\\u0441\\u043a\\u0430\\u044f \\u043a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c"}}]	20	1
146	2018-03-10 14:26:45.526205+00	30	Двери с порталами из массива	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0414\\u0432\\u0435\\u0440\\u0438 \\u0441 \\u043f\\u043e\\u0440\\u0442\\u0430\\u043b\\u0430\\u043c\\u0438 \\u0438\\u0437 \\u043c\\u0430\\u0441\\u0441\\u0438\\u0432\\u0430"}}]	20	1
147	2018-03-10 14:26:52.183856+00	29	Прихожая	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041f\\u0440\\u0438\\u0445\\u043e\\u0436\\u0430\\u044f"}}]	20	1
148	2018-03-10 14:27:32.872482+00	40	Декоративная-люстра в зал	2	[{"changed": {"fields": ["catalog"]}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0414\\u0435\\u043a\\u043e\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u0430\\u044f-\\u043b\\u044e\\u0441\\u0442\\u0440\\u0430 \\u0432 \\u0437\\u0430\\u043b"}}]	20	1
149	2018-03-10 14:28:28.888154+00	54	Журнальный стол из бука	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0416\\u0443\\u0440\\u043d\\u0430\\u043b\\u044c\\u043d\\u044b\\u0439 \\u0441\\u0442\\u043e\\u043b \\u0438\\u0437 \\u0431\\u0443\\u043a\\u0430"}}]	20	1
150	2018-03-10 14:29:23.259926+00	55	Резьба - ручная работа	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0420\\u0435\\u0437\\u044c\\u0431\\u0430 - \\u0440\\u0443\\u0447\\u043d\\u0430\\u044f \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430"}}]	20	1
151	2018-03-10 14:29:55.848846+00	56	Камин из дуба	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041a\\u0430\\u043c\\u0438\\u043d \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430"}}]	20	1
152	2018-03-10 14:30:43.317797+00	57	Дверной карниз	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0414\\u0432\\u0435\\u0440\\u043d\\u043e\\u0439 \\u043a\\u0430\\u0440\\u043d\\u0438\\u0437"}}]	20	1
153	2018-03-10 14:31:23.500273+00	58	Декоративный фрагмент портала	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0414\\u0435\\u043a\\u043e\\u0440\\u0430\\u0442\\u0438\\u0432\\u043d\\u044b\\u0439 \\u0444\\u0440\\u0430\\u0433\\u043c\\u0435\\u043d\\u0442 \\u043f\\u043e\\u0440\\u0442\\u0430\\u043b\\u0430"}}]	20	1
154	2018-03-10 14:31:56.3182+00	59	Фрагмент арки	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0424\\u0440\\u0430\\u0433\\u043c\\u0435\\u043d\\u0442 \\u0430\\u0440\\u043a\\u0438"}}]	20	1
155	2018-03-10 14:32:28.468401+00	60	Поручень (ручная работа)	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041f\\u043e\\u0440\\u0443\\u0447\\u0435\\u043d\\u044c (\\u0440\\u0443\\u0447\\u043d\\u0430\\u044f \\u0440\\u0430\\u0431\\u043e\\u0442\\u0430)"}}]	20	1
171	2018-03-11 05:00:46.988789+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
157	2018-03-10 14:34:37.432791+00	62	Усиленная входная дверь	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0423\\u0441\\u0438\\u043b\\u0435\\u043d\\u043d\\u0430\\u044f \\u0432\\u0445\\u043e\\u0434\\u043d\\u0430\\u044f \\u0434\\u0432\\u0435\\u0440\\u044c"}}]	20	1
158	2018-03-10 14:35:32.827091+00	63	Балконный блок	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0411\\u0430\\u043b\\u043a\\u043e\\u043d\\u043d\\u044b\\u0439 \\u0431\\u043b\\u043e\\u043a"}}]	20	1
159	2018-03-10 14:36:15.900717+00	64	Элементы лестничного марша	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u042d\\u043b\\u0435\\u043c\\u0435\\u043d\\u0442\\u044b \\u043b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0447\\u043d\\u043e\\u0433\\u043e \\u043c\\u0430\\u0440\\u0448\\u0430"}}]	20	1
160	2018-03-10 14:36:52.391939+00	65	Арка для сауны	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0410\\u0440\\u043a\\u0430 \\u0434\\u043b\\u044f \\u0441\\u0430\\u0443\\u043d\\u044b"}}]	20	1
161	2018-03-10 14:37:28.074924+00	66	роспись на филенки для дверей	1	[{"added": {}}, {"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0440\\u043e\\u0441\\u043f\\u0438\\u0441\\u044c \\u043d\\u0430 \\u0444\\u0438\\u043b\\u0435\\u043d\\u043a\\u0438 \\u0434\\u043b\\u044f \\u0434\\u0432\\u0435\\u0440\\u0435\\u0439"}}]	20	1
162	2018-03-10 14:37:56.158237+00	7	ПОРТФОЛИО	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041f\\u041e\\u0420\\u0422\\u0424\\u041e\\u041b\\u0418\\u041e"}}]	18	1
163	2018-03-10 14:38:21.639069+00	4	Мебель	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041c\\u0435\\u0431\\u0435\\u043b\\u044c"}}]	18	1
164	2018-03-10 14:38:47.60839+00	3	Интерьеры	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0418\\u043d\\u0442\\u0435\\u0440\\u044c\\u0435\\u0440\\u044b"}}]	18	1
165	2018-03-10 14:42:22.349075+00	5	Декор	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u0414\\u0435\\u043a\\u043e\\u0440"}}]	18	1
166	2018-03-10 14:43:39.57824+00	2	Контакт	2	[{"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PageImage object"}}]	32	1
167	2018-03-10 14:44:20.224369+00	2	Контакт	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "PageImage object"}}]	32	1
168	2018-03-10 16:25:10.213271+00	41	Спальная комната	2	[{"changed": {"object": "\\u0421\\u043f\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	20	1
169	2018-03-10 16:28:07.823217+00	41	Спальная комната	2	[{"changed": {"object": "\\u0421\\u043f\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430", "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"]}}]	20	1
170	2018-03-10 16:31:28.383971+00	41	Спальная комната	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u0421\\u043f\\u0430\\u043b\\u044c\\u043d\\u0430\\u044f \\u043a\\u043e\\u043c\\u043d\\u0430\\u0442\\u0430", "fields": ["image"]}}]	20	1
172	2018-03-11 05:00:54.204631+00	4	Мебель	2	[{"changed": {"fields": ["sort"]}}]	18	1
173	2018-03-11 07:19:30.033334+00	35	Кровать, дверь, тумба, шкаф	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444"}}]	20	1
174	2018-03-11 07:23:09.595085+00	35	Кровать, дверь, тумба, шкаф	2	[{"added": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444"}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444"}}]	20	1
175	2018-03-11 07:23:27.280932+00	35	Кровать, дверь, тумба, шкаф	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image", "image_is_main"], "object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444"}}, {"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image", "image_is_main"], "object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444"}}]	20	1
176	2018-03-11 07:23:56.199287+00	35	Кровать, дверь, тумба, шкаф	2	[{"changed": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "fields": ["image"], "object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444"}}, {"deleted": {"name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041a\\u0440\\u043e\\u0432\\u0430\\u0442\\u044c, \\u0434\\u0432\\u0435\\u0440\\u044c, \\u0442\\u0443\\u043c\\u0431\\u0430, \\u0448\\u043a\\u0430\\u0444"}}]	20	1
177	2018-03-13 17:46:08.706477+00	1	О нас	2	[{"changed": {"fields": ["description"]}}, {"changed": {"fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PageImage object"}}]	32	1
178	2018-03-13 17:48:07.636353+00	1	О нас	2	[{"changed": {"fields": ["description"]}}, {"changed": {"fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PageImage object"}}]	32	1
179	2018-03-13 17:48:20.622205+00	1	О нас	2	[{"changed": {"fields": ["is_allow_comments"]}}, {"changed": {"fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PageImage object"}}]	32	1
180	2018-03-13 17:48:50.685519+00	1	О нас	2	[{"changed": {"fields": ["is_allow_comments"]}}, {"changed": {"fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "PageImage object"}}]	32	1
181	2018-03-13 17:54:23.596694+00	14	Лестница из дуба	2	[{"changed": {"fields": ["description"]}}, {"changed": {"fields": ["image"], "name": "\\u0424\\u043e\\u0442\\u043e\\u0433\\u0440\\u0430\\u0444\\u0438\\u044e", "object": "\\u041b\\u0435\\u0441\\u0442\\u043d\\u0438\\u0446\\u0430 \\u0438\\u0437 \\u0434\\u0443\\u0431\\u0430"}}]	20	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 181, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	filebrowser	filebrowser
2	admin	logentry
3	sites	site
4	auth	group
5	auth	user
6	auth	permission
7	contenttypes	contenttype
8	sessions	session
9	advertising	sliderhome
10	blog	post
11	blog	postimage
12	blog	comment
13	blog	blog
14	blog	blogimage
15	gallery	galleryimage
16	gallery	gallery
17	catalog	catalogimage
18	catalog	catalog
19	product	productcomment
20	product	product
21	product	productimage
22	product	productitem
23	order	story
24	order	status
25	order	orderitem
26	order	order
27	users	userprofile
28	users	userlink
29	home	homeimage
30	home	home
31	menu	mainmenu
32	page	page
33	page	pagecomment
34	page	pageimage
35	site_info	listlink
36	site_info	textinfo
37	site_info	socialnetwork
38	settings_template	footer
39	settings_template	settingstemplate
40	include_area	includearea
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 40, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-03-03 21:28:48.051651+00
2	auth	0001_initial	2018-03-03 21:28:49.384819+00
3	admin	0001_initial	2018-03-03 21:28:49.784338+00
4	admin	0002_logentry_remove_auto_add	2018-03-03 21:28:49.817351+00
5	advertising	0001_initial	2018-03-03 21:28:50.128732+00
6	contenttypes	0002_remove_content_type_name	2018-03-03 21:28:50.172839+00
7	auth	0002_alter_permission_name_max_length	2018-03-03 21:28:50.195072+00
8	auth	0003_alter_user_email_max_length	2018-03-03 21:28:50.217204+00
9	auth	0004_alter_user_username_opts	2018-03-03 21:28:50.236654+00
10	auth	0005_alter_user_last_login_null	2018-03-03 21:28:50.272809+00
11	auth	0006_require_contenttypes_0002	2018-03-03 21:28:50.283946+00
12	auth	0007_alter_validators_add_error_messages	2018-03-03 21:28:50.302703+00
13	auth	0008_alter_user_username_max_length	2018-03-03 21:28:50.45061+00
14	blog	0001_initial	2018-03-03 21:28:52.560565+00
15	catalog	0001_initial	2018-03-03 21:28:53.603875+00
16	gallery	0001_initial	2018-03-03 21:28:54.871235+00
17	home	0001_initial	2018-03-03 21:28:55.648474+00
18	include_area	0001_initial	2018-03-03 21:28:55.87043+00
19	page	0001_initial	2018-03-03 21:28:56.936446+00
20	menu	0001_initial	2018-03-03 21:28:57.78008+00
21	product	0001_initial	2018-03-03 21:28:59.967634+00
22	order	0001_initial	2018-03-03 21:29:01.056713+00
23	sessions	0001_initial	2018-03-03 21:29:01.344219+00
24	sites	0001_initial	2018-03-03 21:29:01.489064+00
25	sites	0002_alter_domain_unique	2018-03-03 21:29:01.621761+00
26	site_info	0001_initial	2018-03-03 21:29:02.545052+00
27	settings_template	0001_initial	2018-03-03 21:29:04.175695+00
28	users	0001_initial	2018-03-03 21:29:04.586253+00
29	site_info	0002_auto_20180307_2026	2018-03-07 20:26:56.956143+00
30	blog	0002_auto_20180307_2106	2018-03-07 21:06:19.168031+00
31	catalog	0002_auto_20180307_2106	2018-03-07 21:06:19.262681+00
32	gallery	0002_auto_20180307_2106	2018-03-07 21:06:19.310377+00
33	home	0002_auto_20180307_2106	2018-03-07 21:06:19.361871+00
34	page	0002_auto_20180307_2106	2018-03-07 21:06:19.428263+00
35	product	0002_auto_20180307_2106	2018-03-07 21:06:19.538357+00
36	site_info	0003_auto_20180307_2106	2018-03-07 21:06:19.568861+00
37	settings_template	0002_auto_20180308_1254	2018-03-08 12:54:07.991615+00
38	settings_template	0003_auto_20180308_1258	2018-03-08 12:58:47.9757+00
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 38, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
b7w3xnccs8k8o8hmmc4mmtzka4j1bfm9	YjA0YjM0OWM1ZjZkMDU4OTA3YzFmNzhhOTA5YTQ0MDc3MjUxNWY4ZTqABJWXAAAAAAAAAH2UKIwNX2F1dGhfdXNlcl9pZJSMATGUjA9fYXV0aF91c2VyX2hhc2iUjChhMmMxNzU0MzMzMjU1MzZkY2E4YjFmZWI5MDE5YjEwMTQ1ODM1YTdhlIwSX2F1dGhfdXNlcl9iYWNrZW5klIwpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmSUdS4=	2018-03-24 08:37:39.49827+00
kkbov4bl8mo3uloe5dl0amooslbf0sjr	ODY4OTNkOTllNjgxODA5NmE3NGJiZjc4Y2NiNWJkNDQzYzU2YTEwNDqABJWXAAAAAAAAAH2UKIwSX2F1dGhfdXNlcl9iYWNrZW5klIwpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmSUjA1fYXV0aF91c2VyX2lklIwBMZSMD19hdXRoX3VzZXJfaGFzaJSMKGEyYzE3NTQzMzMyNTUzNmRjYThiMWZlYjkwMTliMTAxNDU4MzVhN2GUdS4=	2018-03-24 16:24:09.171428+00
v7ntvwq5mgnuccoj6vh5v3m2r25cbscd	YjY4MzE1NTMwMjZhZTM3YmUzMDBjNzAwOWU3YzFkNDc2YTIzZDIyZTqABJWXAAAAAAAAAH2UKIwSX2F1dGhfdXNlcl9iYWNrZW5klIwpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmSUjA9fYXV0aF91c2VyX2hhc2iUjChhMmMxNzU0MzMzMjU1MzZkY2E4YjFmZWI5MDE5YjEwMTQ1ODM1YTdhlIwNX2F1dGhfdXNlcl9pZJSMATGUdS4=	2018-03-25 04:59:53.369393+00
hqrp6aqz1hlpb6wkyvbabkbg4j5cofsv	YjY4MzE1NTMwMjZhZTM3YmUzMDBjNzAwOWU3YzFkNDc2YTIzZDIyZTqABJWXAAAAAAAAAH2UKIwSX2F1dGhfdXNlcl9iYWNrZW5klIwpZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmSUjA9fYXV0aF91c2VyX2hhc2iUjChhMmMxNzU0MzMzMjU1MzZkY2E4YjFmZWI5MDE5YjEwMTQ1ODM1YTdhlIwNX2F1dGhfdXNlcl9pZJSMATGUdS4=	2018-03-25 07:17:40.978507+00
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.django_site (id, domain, name) FROM stdin;
1	localhost:8000	dervek.ru
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- Data for Name: gallery_gallery; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.gallery_gallery (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, lft, rght, tree_id, level, author_id, parent_id) FROM stdin;
1	Галерея лучших работ	\N	galereya-luchshih-rabot	Галерея лучших работ	Галерея лучших работ	Галерея лучших работ	ru_RU	\N	2018-02-18 18:35:17.585+00	2018-02-21 17:36:50.259+00	<p>Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;Галерея лучших работ&nbsp;</p>	1000	t	1	2	4	0	\N	\N
2	Спецэффекты - 3d эффекты	\N	speceffekty-3d-effekty	Спецэффекты - 3d эффекты	Спецэффекты - 3d эффекты	Спецэффекты - 3d эффекты			2018-02-21 17:31:31.954+00	2018-03-03 15:34:10.675+00	<p style="text-align:justify">Сегодня большой популярность пользуется&nbsp;<strong>покраска лестниц, мебели, дверей, кухонных гарнитуров из массива со спецэффектами 3D.&nbsp;</strong>То есть плоскость дерева можно превратить визуально в пластик, металл, изобразить искусственное отверстие, представить продукт старинным, нанести какие либо узоры или просто изменить его текстуру.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;<strong>3D декорирование</strong>&nbsp;сегодня широко применяется во многих производственных отраслях. Это придает объекту невероятный дизайн. Его использование требует внимательности к самой сути модели, потому что эта технология ломает самые закостенелые стереотипы!<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Применение&nbsp;<strong>покраски с 3D эффектом для деревянных конструкций&nbsp;</strong>задача не из легких. Как придать деревянному изделию необычный, эксклюзивный вид, со вкусом мы знаем, и готовы поделиться этим чудом с Вашими будущими&nbsp;<strong>столярными изделиями.</strong>&nbsp;У нас работают художники и дизайнеры, которые рассматривают Ваши пожелания и способны предоставить несколько вариантов. Также есть различные образцы, которые помогут Вам сориентироваться в этой технологии. В любом случае результат приятно удивит Вас!</p>	1000	t	2	3	3	1	\N	3
3	Колеровка	\N	kolerovka	Колеровка	Колеровка	Колеровка	ru_RU		2018-02-21 17:36:43.645+00	2018-03-03 15:33:21.684+00	<p>Работая только с иностранными поставщиками лакокрасочных изделий мы уверены в качестве своего покрытия.</p>\r\n\r\n<p>Дерево проходит специальные предварительные обработки, если они имеют место быть.</p>\r\n\r\n<p>Принимаются во внимание все технологические моменты, для сохранения&nbsp;<strong>экологических свойств</strong>, изначально заложенных природой в дерево.</p>\r\n\r\n<p>Работа настоящих &quot;мастеров малярного искусства&quot; дает качественный результат.</p>\r\n\r\n<p>Применяя новейшие технологии&nbsp;<strong>3D декорирования</strong>&nbsp;можно добиться практически любых визуальных предпочтений.</p>	1000	t	1	6	3	0	\N	\N
4	Цветная палитра	\N	cvetnaya-palitra	Цветная палитра	Цветная палитра	Цветная палитра	ru_RU	\N	2018-02-21 17:38:52.955+00	2018-02-21 17:58:05.896+00	<p style="text-align:justify"><strong>Столярное производство Dervek</strong>&nbsp;предоставляет огромный выбор для&nbsp;<strong>покраски мебели, лестниц, дверей и кухонных гарнитуров.</strong>Среди образчиков для покраски продукции имеется любой окрас под натуральный массив. По желанию, сделать темнее или светлее существующий вариант образца не составляет каких либо технологических проблем.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Используя&nbsp;<strong>лаки иностранных производителей</strong>&nbsp;изделие может быть и матовым, и полу-матовым, и глянцевым, в зависимости от желания клиента. В нашем случае, не исключен и предоставляемый совет, который видят дизайнеры и художники, относительно финишной покраски.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Известно, что при широком ассортименте разбегаются глаза и сложно выбрать, потому что все красиво и хорошо! С учетом освещения помещений и прочих нюансов мы подскажем правильное решение. Потому что&nbsp;<em>цветность столярного продукта влияет на визуальный размер помещения, на общую цветовую гамму,</em>&nbsp;да и вообще на дизайн в целом.<br />\r\n&nbsp;&nbsp;&nbsp;&nbsp;Так же не составляет никаких проблем подобрать цвет к образцам, которые клиенты предоставляют нам при размещении заказа.</p>	1000	t	4	5	3	1	\N	3
\.


--
-- Name: gallery_gallery_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.gallery_gallery_id_seq', 4, true);


--
-- Data for Name: gallery_galleryimage; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.gallery_galleryimage (id, image, image_title, image_is_main, image_description, gallery_id) FROM stdin;
1	галерея/photo_18.JPG	\N	t		1
2	галерея/c9a8065aa4104e8cbaaf424eb4dcc528.jpg	\N	f		1
3	галерея/photo2014_8.JPG	\N	f		1
4	галерея/photo_21.JPG	\N	f		1
5	галерея/photo2014_9.JPG	\N	f		1
6	галерея/photo_34.JPG	\N	f		1
7	галерея/photo_15.JPG	\N	f		1
8	галерея/photo_27.JPG	\N	f		1
9	галерея/41e6f254ef764e13a961b60bc8583f1d.JPG	\N	f		1
10	галерея/3bd9740f1b08e546d3b3d7f3d1fa3ead.JPG	\N	f		1
11	галерея/photo_14.JPG	\N	f		1
12	галерея/2134b61db316c9545a8081ae0453db6b.JPG	\N	f		1
13	галерея/7e2ca214b45128f0e1d9ac82ac434f0e.jpg	\N	f		1
14	галерея/2f999b87a08c9ee4005b8f61f9a45ac5.jpg	\N	f		1
15	колеровка/321b2180d899968d079b2b9f12431c46.jpg	Колеровка	t		3
16	колеровка/Спецэффекты_3d_эффекты/c97fa3245298622788b2e39cc4595632.jpg	Спецэффекты - 3d эффекты	f		2
17	колеровка/Спецэффекты_3d_эффекты/7d8d94268f2a33733059d11f13455693.jpg	Спецэффекты - 3d эффекты	f		2
18	колеровка/Спецэффекты_3d_эффекты/db397842135f5a7637edc8aba71bd1d7.jpg	Спецэффекты - 3d эффекты	f		2
19	колеровка/Спецэффекты_3d_эффекты/cc5558cf725b47246c625fcce21ac373.png	Спецэффекты - 3d эффекты	f		2
20	колеровка/Спецэффекты_3d_эффекты/f0135377f896f9dc7a40ebeafbb8e442.jpg	Спецэффекты - 3d эффекты	f		2
21	колеровка/Спецэффекты_3d_эффекты/321b2180d899968d079b2b9f12431c46 (1).jpg	Спецэффекты - 3d эффекты	f		2
22	колеровка/Спецэффекты_3d_эффекты/de2fc0554897e1e2fe5d6d73d55760c6.jpg	Спецэффекты - 3d эффекты	f		2
23	колеровка/Спецэффекты_3d_эффекты/1658659bc405bb68f05dbe731cebddb4.jpg	Спецэффекты - 3d эффекты	f		2
24	колеровка/Спецэффекты_3d_эффекты/4061c1497726749473df1f45d046b276.jpg	Спецэффекты - 3d эффекты	t		2
25	колеровка/Спецэффекты_3d_эффекты/67be5fa2864e3bdff4cf8c4c0d65f5a6.jpg	Спецэффекты - 3d эффекты	f		2
26	колеровка/Спецэффекты_3d_эффекты/20226bdbe2e75babc718be9a0fb77e8e.jpg	Спецэффекты - 3d эффекты	f		2
27	колеровка/Спецэффекты_3d_эффекты/2dcf29b585073d8c106a5d27fb03d0b6.jpg	Спецэффекты - 3d эффекты	f		2
28	колеровка/Спецэффекты_3d_эффекты/8b0ea09cbdcab953f84fc884973b5421.jpg	Спецэффекты - 3d эффекты	f		2
29	колеровка/Спецэффекты_3d_эффекты/62b2d1cbb27ef40b225737750c1e080c.jpg	Спецэффекты - 3d эффекты	f		2
30	колеровка/Спецэффекты_3d_эффекты/63d6fc68e528068b20e9760ddb3cd850.jpg	Спецэффекты - 3d эффекты	f		2
31	колеровка/Спецэффекты_3d_эффекты/d41b2f5ca697e895c2d57406db8f2ee1.jpg	Спецэффекты - 3d эффекты	f		2
32	колеровка/Спецэффекты_3d_эффекты/d5507435358a6496dacbc710d686db50.jpg	Спецэффекты - 3d эффекты	f		2
33	колеровка/Спецэффекты_3d_эффекты/606a8571662d4df3db144fe84c524ebb.jpg	Спецэффекты - 3d эффекты	f		2
34	колеровка/Спецэффекты_3d_эффекты/321b2180d899968d079b2b9f12431c46.jpg	Спецэффекты - 3d эффекты	t		2
35	колеровка/цветовая_палитра/352d3b81883a26cde37cf6d3ce2d358e.jpg	\N	f		4
36	колеровка/цветовая_палитра/0707040f0393ea73f16160e17fc938df.jpg	\N	f		4
37	колеровка/цветовая_палитра/6dcf413cb5bb7ce7506d591a41ab6b98.jpg	\N	f		4
38	колеровка/цветовая_палитра/e6ca06644798b8350b78f5f12adace97.jpg	\N	f		4
39	колеровка/цветовая_палитра/334c55e912f0646a04aa11f1551ecb40.jpg	\N	f		4
40	колеровка/цветовая_палитра/f8a8d9c9e13888f742a086ed9910b9ae.jpg	\N	f		4
41	колеровка/цветовая_палитра/f0bddddb51ff051ac940807b48313de2.jpg	\N	f		4
42	колеровка/цветовая_палитра/ebc682e7195fc35f76b913b1058f3ee0.jpg	\N	f		4
43	колеровка/цветовая_палитра/2718b51b7ef4a451e0bf4e77c5f5415b.jpg	\N	f		4
44	колеровка/цветовая_палитра/f13a5302e555dca68f88b6604a531f2a.jpg	\N	f		4
45	колеровка/цветовая_палитра/2db977ab484f51090c553507029a88aa.jpg	\N	f		4
46	колеровка/цветовая_палитра/28d2190994064a6154d7727a3f213b88.jpg	\N	f		4
47	колеровка/цветовая_палитра/ce3b1579ede81532a39f083cbdb163b3.jpg	\N	f		4
48	колеровка/цветовая_палитра/8718b355dac94474d9f39480035a973c.jpg	\N	f		4
49	колеровка/цветовая_палитра/98579cc1956e4c60ef768ab11f4b9f74.jpg	\N	f		4
50	колеровка/цветовая_палитра/0ed728ee19fcb94232b42bc4b1754c0e.jpg	\N	f		4
51	колеровка/цветовая_палитра/8a95b02bc1d90d88ad1e996b6995693b.jpg	\N	f		4
52	колеровка/цветовая_палитра/8d349442a95dbc5cde1d91f42f2ec306.jpg	\N	f		4
53	колеровка/цветовая_палитра/5e15a035953ebf2568220c618edef91b.jpg	\N	f		4
54	колеровка/цветовая_палитра/1d2a20d542313aa9a2ccbac4f4feb301.jpg	\N	f		4
\.


--
-- Name: gallery_galleryimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.gallery_galleryimage_id_seq', 54, true);


--
-- Data for Name: home_home; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.home_home (id, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, title, html, is_show, blog_id) FROM stdin;
1	home	HOME	HOME	HOME	ru_RU		Воплощаем сказку в реальность!	<p>Мы занимаемся изготовлением лестниц, кухонных гарнитуров, мебели, дверей, интерьеров, и, конечно же, воплощаем в реальность любые фантазии человеческого разума. Проекты любой сложности под ключ. Натуральный массив. Все возможно, когда за дело берутся настоящие мастера, которые гордятся и восхищаются своей профессией! Мы всегда находим решения! Мы готовы взять на себя ответственность за комфорт и уют в Вашем доме!</p>	t	3
\.


--
-- Name: home_home_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.home_home_id_seq', 1, true);


--
-- Data for Name: home_homeimage; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.home_homeimage (id, image, image_title, image_is_main, image_description, home_id) FROM stdin;
1	галерея/photo_14.JPG	\N	f		1
2	слайдер/les12.jpg	\N	f		1
\.


--
-- Name: home_homeimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.home_homeimage_id_seq', 2, true);


--
-- Data for Name: include_area_includearea; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.include_area_includearea (id, title, is_show, image, description, html_code, code, sort) FROM stdin;
1	Комфорт	t	главная_страница/comf.jpg	Деревянный интерьер дома - это всегда чистый воздух, прошедший естественную фильтрацию путем медленного выделения целебных смол; поддержка оптимальной влажности и концентрации тепла в помещении. К примеру, дуб, береза и клен за много веков запатентовали себя как деревья-"доноры", которые дарят необходимую жизненную энергию, восстанавливают силы и снимают усталость, благоприятно воздействует на психологическое состояние человека. Дерево - это естественная и неотъемлемая часть вашего дома, так человек и природа - единая Экосистема.	\N	advantages	1000
2	Надежность	t	главная_страница/nad.jpg	Учитывая человеческие факторы уже при замере или проектировании можно добиться высочайшей комфортабельности изделия, будь то стул или лестница. Деревянную конструкцию без труда можно подобрать к уже готовому интерьеру, особенно если она выполняется на заказ. Да и по своей природе деревянная мебель на генетическом уровне заставляет понять что это красиво, уютно и тепло. Домашний интерьер, в коем преобладает натуральный массив древесины, несомненно подчеркивает престиж, духовность, предусмотрительность и социальный статус хозяина.	\N	advantages	0
3	Экологичность	t	главная_страница/ecol.jpg	До сих пор дерево остается самым популярным строительным материалом, который применяется даже в возведении домов. Обязательная сушка древесины предотвращает множество проблем. Покрытие и обработка на водной основе увеличивают срок службы продукции, не сказываясь на экологии помещения. С новейшими технологиями обработки, крепления, сращивания надежность изделий приобрела новый уровень. При поломке или старении деревянной вещи несложно произвести ремонт, реконструкцию или реставрацию.	\N	advantages	0
4	test	f	maxresdefault.jpg	testt esttesttesttestt esttesttesttesttes ttesttes ttest	\N	advantages	1000
5	123	f	вакансии/malar.jpg	123	\N	advantages	1000
\.


--
-- Name: include_area_includearea_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.include_area_includearea_id_seq', 5, true);


--
-- Data for Name: menu_mainmenu; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.menu_mainmenu (id, name, is_show, sort, lft, rght, tree_id, level, blog_id, catalog_id, gallery_id, page_id, parent_id) FROM stdin;
1	ГЛАВНАЯ	t	1000	1	2	1	0	\N	\N	\N	\N	\N
2	ПОРТФОЛИО	t	3000	1	2	3	0	\N	7	\N	\N	\N
3	ГАЛЕРЕЯ	t	4000	1	2	4	0	\N	\N	1	\N	\N
4	КОЛЕРОВКА	t	5000	1	2	5	0	\N	\N	3	\N	\N
5	УСЛУГИ	t	6000	1	2	6	0	1	\N	\N	\N	\N
6	О НАС	t	2000	1	2	2	0	\N	\N	\N	1	\N
7	ВАКАНСИИ	t	8000	1	2	7	0	3	\N	\N	\N	\N
8	КОНТАКТЫ	t	8000	1	2	8	0	\N	\N	\N	2	\N
\.


--
-- Name: menu_mainmenu_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.menu_mainmenu_id_seq', 8, true);


--
-- Data for Name: order_order; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.order_order (id, created, updated, first_name, last_name, email, address, postal_code, city, total_cost, ttn, comment, status_id, user_id) FROM stdin;
1	2018-02-25 10:58:27.655+00	2018-02-25 10:58:28.348+00	Михаил	Марычев	marychev_mihail@mail.ru	8	\N	\N	1500000.00			\N	1
2	2018-02-25 13:02:41.48+00	2018-02-25 13:02:41.549+00	Admin	Admin	marychev@garpix.com	Колесанова	\N	\N	500000.00		my first order	\N	1
4	2018-03-08 11:42:11.695839+00	2018-03-08 11:42:24.548673+00	Михаил	Марычев		8	8	8	1464.00	0000-0000-0000-0000-8	sdcfsdcsd	1	1
\.


--
-- Name: order_order_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.order_order_id_seq', 4, true);


--
-- Data for Name: order_orderitem; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.order_orderitem (id, price, quantity, order_id, product_item_id) FROM stdin;
\.


--
-- Name: order_orderitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.order_orderitem_id_seq', 4, true);


--
-- Data for Name: order_status; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.order_status (id, name) FROM stdin;
1	Ожидание
2	Оплачен
\.


--
-- Name: order_status_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.order_status_id_seq', 2, true);


--
-- Data for Name: order_story; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.order_story (id, created, updated, total_cost, comment, order_id, status_id) FROM stdin;
1	2018-02-25 10:58:28.275+00	2018-02-25 10:58:28.275+00	1500000.00	[+]Добавлен товар `# 111` 500000.00 x 3 = 1500000.00 p	1	\N
2	2018-02-25 13:02:41.53+00	2018-02-25 13:02:41.53+00	500000.00	[+]Добавлен товар `# 111` 500000.00 x 1 = 500000.00 p	2	\N
3	2018-03-08 11:42:11.739578+00	2018-03-08 11:42:11.739605+00	132.00	[+]Добавлен товар `# 222` 12 x 11 = 132 p	4	1
4	2018-03-08 11:42:11.770845+00	2018-03-08 11:42:11.770901+00	3577884.00	[+]Добавлен товар `# 111` 32232 x 111 = 3577752 p	4	1
5	2018-03-08 11:42:24.544449+00	2018-03-08 11:42:24.544478+00	1464.00	[*]Цена изменена. Товар:`# 111` 12 x 111 = 1332 p	4	1
\.


--
-- Name: order_story_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.order_story_id_seq', 5, true);


--
-- Data for Name: page_page; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.page_page (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, is_allow_comments, author_id) FROM stdin;
2	Контакт		kontakt	Контакт	Контакт	Contact	ru_RU	<a id="firmsonmap_biglink" href="http://maps.2gis.ru/#/?history=project/ivanovo/center/41.047377890079,56.957263892855/zoom/16/state/widget/id/9148466024633817/firms/9148466024633817">Перейти к большой карте</a>\r\n<script charset="utf-8" type="text/javascript" src="http://firmsonmap.api.2gis.ru/js/DGWidgetLoader.js"></script>\r\n<script charset="utf-8" type="text/javascript">new DGWidgetLoader({"borderColor":"#a3a3a3","width":"900","height":"600","wid":"8ef1edcf3a24f9bc527475964d453cea","pos":{"lon":"41.047377890079","lat":"56.957263892855","zoom":"16"},"opt":{"ref":"hidden","card":["name","contacts","schedule","payings"],"city":"ivanovo"},"org":[{"id":"9148466024633817","hash":"c598qa34848IJ1HJ97f5uvgc69030J341dd6178649366937596A69GC1J0I920673"}]});</script>\r\n<noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>	2018-03-03 13:22:13.705+00	2018-03-10 14:44:20.220124+00	<h4>&nbsp;</h4>\r\n\r\n<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:80%">\r\n\t<tbody>\r\n\t\t<tr>\r\n\t\t\t<td>\r\n\t\t\t<h4>Наш Адрес:</h4>\r\n\r\n\t\t\t<address>153051 г.Иваново, ул. Домостроителей, 2 лит А<br />\r\n\t\t\tТелефон:&nbsp;+7(915)-832-38-36<br />\r\n\t\t\tПочта:&nbsp;<a href="mailto:revcloud@mail.ru">revcloud@mail.ru</a></address>\r\n\t\t\t</td>\r\n\t\t\t<td>\r\n\t\t\t<h4>Часы Работы:</h4>\r\n\r\n\t\t\t<ul>\r\n\t\t\t\t<li>Понедельник - Пятница:08:00 - 18:00</li>\r\n\t\t\t\t<li>Суббота:08:00 - 14:00</li>\r\n\t\t\t\t<li>Воскресенье:Выходной</li>\r\n\t\t\t</ul>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</tbody>\r\n</table>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<h4>&nbsp;</h4>\r\n\r\n<ul>\r\n</ul>	1000	t	t	\N
1	О нас		o-nas	О нас	О нас	О нас	ru_RU		2018-02-21 20:51:32.269+00	2018-03-13 17:48:50.666187+00	<p>...история начинается с конца 80х годов.&nbsp;</p>\r\n\r\n<p>Наше производство имеет&nbsp;<strong>собственные паровые сушильные камеры</strong>, примерно 40 кв/м. Мы с трепетом относимся к материалу который в последствии станет живым детищем! Большой&nbsp;<strong>станочный парк</strong>, производство около 1200кв/м, грамотно распределенное на участки с&nbsp;покраской продукции, отборочный (здесь мы сортируем материал), заготовительный и отделочный цеха. Наш штат сотрудников многочисленный и разнообразный: дизайнеры, художники, мастера, маляры, начальники по участкам, столяры, операторы паровых котлов - это если не вдаваться в подробности!</p>\r\n\r\n<p><strong>Лестницы, мебель,&nbsp;интерьеры, кухни, двери, ограждения, столы и стулья из разных пород древесины!</strong>&nbsp;Да. Это основная тематика нешей работы.&nbsp; Штат клиентов ростет и&nbsp;продукция радует людей из разных уголков&nbsp;России.</p>\r\n\r\n<p>Мы научились видеть наперед готовые решения, что помогает предопределить множество ошибок.</p>\r\n\r\n<hr />\r\n<p style="text-align:right"><em>Наша продукция будет служить Вам долго!<br />\r\nВсегда будет радовать Ваши глаза и согревать душу!<br />\r\nНаша продукция - на ВЕК`a.</em></p>	1000	t	f	\N
\.


--
-- Name: page_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.page_page_id_seq', 2, true);


--
-- Data for Name: page_pagecomment; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.page_pagecomment (id, created, updated, text, ip_address, username, email, is_show, page_id, user_id) FROM stdin;
1	2018-03-03 14:06:17.274+00	2018-03-03 14:06:17.274+00	drgfvdrgvdrgvdrgdrg	0.0.0.0	anonymous		f	2	\N
\.


--
-- Name: page_pagecomment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.page_pagecomment_id_seq', 1, true);


--
-- Data for Name: page_pageimage; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.page_pageimage (id, image, image_title, image_is_main, image_description, page_id) FROM stdin;
2	портфолио/декор/photo2014_31.JPG	Контакт	t		2
1	колеровка/Спецэффекты_3d_эффекты/7d8d94268f2a33733059d11f13455693.jpg	О нас	t		1
\.


--
-- Name: page_pageimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.page_pageimage_id_seq', 2, true);


--
-- Data for Name: product_product; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.product_product (id, title, description, slug, seo_title, seo_description, seo_keywords, og_locale, scripts, created, updated, html, sort, is_show, articul, is_bestseller, is_new, author_id) FROM stdin;
16	Лестница из массива дуб	Лестница из массива дуб\r\nМосква , Немчиновка 2010	lestnica-iz-massiva-dub	Лестница из массива дуб	Лестница из массива дуб	Лестница из массива дуб	ru_RU		2018-03-10 09:29:30.284767+00	2018-03-10 09:29:30.284795+00	<p>Лестница из массива дуб<br />\r\nМосква , Немчиновка 2010</p>	1000	t	\N	f	f	\N
11	Лестница с позолотой	Лестница с позолотой\r\nИваново 2008	lestnica-s-pozolotoj	Лестница с позолотой	Лестница с позолотой	Лестница с позолотой	ru_RU		2018-03-10 09:20:46.822395+00	2018-03-10 09:25:38.148149+00	<p>Лестница с позолотой<br />\r\nИваново 2008</p>	1000	t	\N	f	t	\N
7	Лестница на больцах из дуба	Лестница на больцах из дуба\r\nцвет: отбеленный дуб	lestnica-na-bolcah-iz-duba	Лестница на больцах из дуба	Лестница на больцах из дуба	Лестница на больцах из дуба	ru_RU		2018-02-17 20:00:07.223+00	2018-03-10 09:13:43.29578+00	<p>Лестница на больцах из дуба<br />\r\nцвет: отбеленный дуб</p>	1000	t	\N	f	t	\N
1	Лестница из дуба с ковкой	Лестница из дуба с ковкой\r\nИваново 2011	lestnica-iz-duba-s-kovkoj	Лестница из дуба с ковкой	Лестница из дуба с ковкой	Лестница из дуба с ковкой	ru_RU		2018-02-17 19:17:48.732+00	2018-03-10 09:15:41.945197+00	<p>Лестница из дуба с ковкой<br />\r\nИваново 2011</p>	1000	t	Лестница ,дверь, массив сосна	f	f	\N
10	Лестница резная, дуб	Лестница "резная",  дуб\r\nСамара 2011	lestnica-reznaya-dub	Лестница резная, дуб	Лестница резная, дуб	Лестница резная, дуб	ru_RU		2018-03-10 09:19:00.03395+00	2018-03-10 09:19:00.033973+00	<p>Лестница &quot;резная&quot;,&nbsp;&nbsp;дуб<br />\r\nСамара 2011</p>	1000	t	\N	f	t	\N
5	Прихожая - Лестница ,дверь, массив сосна	Прихожая - Лестница ,дверь, массив сосна\r\nКинешма 2008	prihozhaya-lestnica-dver-massiv-sosna	Прихожая - Лестница ,дверь, массив сосна	Прихожая - Лестница ,дверь, массив сосна	Прихожая - Лестница ,дверь, массив сосна	ru_RU		2018-02-17 19:57:20.33+00	2018-03-10 09:04:05.789405+00	<h4>Прихожая - Лестница ,дверь, массив сосна</h4>\r\n\r\n<p>Кинешма 2008</p>	1000	t	\N	f	t	\N
4	Лестница в квартиру	Лестница в квартиру\r\nмассив дуб	lestnica-v-kvartiru	Лестница в квартиру	Лестница в квартиру	Лестница в квартиру	ru_RU		2018-02-17 19:42:44.947+00	2018-03-10 09:08:14.576215+00	<h4>Лестница в квартиру</h4>\r\n\r\n<p>массив дуб</p>	1000	t	\N	f	f	\N
9	Лестница, дверь, массив	Лестница, дверь, массив\r\nМосква 2014	lestnica-dver-massiv	Лестница, дверь, массив	Лестница, дверь, массив	Лестница, дверь, массив	ru_RU		2018-02-17 20:13:14.655+00	2018-03-10 08:57:59.649504+00	<h4>Лестница, дверь, массив</h4>\r\n\r\n<p style="text-align:justify">Москва 2014</p>	1000	t	\N	f	t	\N
8	Лестничный марш	Лестничный марш\r\nЯрославль	lestnichnyj-marsh	Лестничный марш	Лестничный марш	Лестничный марш	ru_RU		2018-02-17 20:11:01.508+00	2018-03-10 08:59:50.071734+00	<p>Лестничный марш<br />\r\nЯрославль</p>	1000	t	\N	f	t	\N
6	Лестница из массива сосны	Лестница из массива сосны\r\nКоридор	lestnica-iz-massiva-sosny	Лестница из массива сосны	Лестница из массива сосны	Лестница из массива сосны	ru_RU		2018-02-17 19:58:58.767+00	2018-03-10 09:02:36.183352+00	<h4>Лестница из массива сосны</h4>\r\n\r\n<p>Коридор</p>	1000	t	\N	f	f	\N
3	Мансардная лестница, хвоя	Мансардная лестница, хвоя\r\nКохма 2008	mansardnaya-lestnica-hvoya	Мансардная лестница, хвоя	Мансардная лестница, хвоя	Мансардная лестница, хвоя	ru_RU		2018-02-17 19:38:59.191+00	2018-03-10 09:11:10.296602+00	<h4>Мансардная лестница, хвоя</h4>\r\n\r\n<p>Кохма 2008</p>	1000	t	\N	f	f	\N
2	Фрагмент лестничного ограждения	Фрагмент лестничного ограждения\r\nМосква 2014	fragment-lestnichnogo-ograzhdeniya	Фрагмент лестничного ограждения	Фрагмент лестничного ограждения	Фрагмент лестничного ограждения	ru_RU		2018-02-17 19:36:53.593+00	2018-03-10 09:12:30.0008+00	<h4>Фрагмент лестничного ограждения</h4>\r\n\r\n<p>Москва 2014</p>	1000	t	\N	f	f	\N
12	Ограждение лестничного марша	Ограждение лестничного марша\r\nЯрославль 2011	ograzhdenie-lestnichnogo-marsha	Ограждение лестничного марша	Ограждение лестничного марша	Ограждение лестничного марша	ru_RU		2018-03-10 09:22:14.824717+00	2018-03-10 09:22:43.688318+00	<p>Ограждение лестничного марша<br />\r\nЯрославль 2011</p>	1000	t	\N	f	f	\N
13	Лестница "отбеленный" дуб	Лестница "отбеленный" дуб\r\nВичуга	lestnica-otbelennyj-dub	Лестница "отбеленный" дуб	Лестница "отбеленный" дуб	Лестница "отбеленный" дуб	ru_RU		2018-03-10 09:24:08.457121+00	2018-03-10 09:24:08.457161+00	<p>Лестница &quot;отбеленный&quot; дуб<br />\r\nВичуга</p>	1000	t	\N	f	t	\N
15	Лестница, дуб	Лестница, дуб\r\nЯрославль	lestnica-dub	Лестница, дуб	Лестница, дуб	Лестница, дуб	ru_RU		2018-03-10 09:28:20.852557+00	2018-03-10 09:28:20.852578+00	<p>Лестница, дуб<br />\r\nЯрославль</p>	1000	t	\N	f	t	\N
17	Лестница, дверь	Лестница, дверь\r\nхвоя, дуб	lestnica-dver	Лестница, дверь	Лестница, дверь	Лестница, дверь	ru_RU		2018-03-10 09:30:36.13791+00	2018-03-10 09:31:02.731986+00	<h4>Лестница, дверь</h4>\r\n\r\n<p>хвоя, дуб</p>	1000	t	\N	f	t	\N
19	Кухня с выходом на балкон	Кухня с выходом на балкон, стол, стулья\r\nЯрославль	kuhnya-s-vyhodom-na-balkon	Кухня с выходом на балкон	Кухня с выходом на балкон	Кухня с выходом на балкон	ru_RU		2018-03-10 13:32:48.601906+00	2018-03-10 13:33:18.732977+00	<p>Кухня с выходом на балкон, стол, стулья<br />\r\nЯрославль</p>	1000	t	\N	f	t	\N
20	Кухня в деревянном доме	Кухня в деревянном доме\r\nПлес 2014	kuhnya-v-derevyannom-dome	Кухня в деревянном доме	Кухня в деревянном доме	Кухня в деревянном доме	ru_RU		2018-03-10 13:39:55.892296+00	2018-03-10 13:39:55.892353+00	<p>Кухня в деревянном доме<br />\r\nПлес 2014</p>	1000	t	\N	f	t	\N
21	Кухня из дуба	Кухня из дуба\r\nШуя	kuhnya-iz-duba	Кухня из дуба	Кухня из дуба	Кухня из дуба	ru_RU		2018-03-10 13:40:36.248501+00	2018-03-10 13:40:36.248525+00	<p>Кухня из дуба<br />\r\nШуя&nbsp;</p>	1000	t	\N	f	t	\N
22	Кухонный гарнитур	Кухонный гарнитур\r\nИвановская область	kuhonnyj-garnitur	Кухонный гарнитур	Кухонный гарнитур	Кухонный гарнитур	ru_RU		2018-03-10 13:41:24.466247+00	2018-03-10 13:41:24.46628+00	<p>Кухонный гарнитур<br />\r\nИвановская область</p>	1000	t	\N	f	t	\N
23	Кухня классическая	Кухня классическая\r\nОрехово Зуево	kuhnya-klassicheskaya	Кухня классическая	Кухня классическая	Кухня классическая	ru_RU		2018-03-10 13:42:14.355585+00	2018-03-10 13:42:14.355608+00	<p>Кухня классическая<br />\r\nОрехово Зуево</p>	1000	t	\N	f	t	\N
24	Кухонный гарнитур, орех	Кухонный гарнитур, орех\r\nШуя 2010	kuhonnyj-garnitur-oreh	Кухонный гарнитур, орех	Кухонный гарнитур, орех	Кухонный гарнитур, орех	ru_RU		2018-03-10 13:43:28.177132+00	2018-03-10 13:43:28.177155+00	<p>Кухонный гарнитур, орех<br />\r\nШуя 2010</p>	1000	t	\N	f	t	\N
25	Кухня классика	Кухня классика\r\nИваново 2012	kuhnya-klassika	Кухня классика	Кухня классика	Кухня классика	ru_RU		2018-03-10 13:44:23.700513+00	2018-03-10 13:44:23.700541+00	<p>Кухня классика<br />\r\nИваново 2012</p>	1000	t	\N	f	t	\N
26	Кухня в деревенском доме	Кухня в деревенском доме\r\nИвановская область	kuhnya-v-derevenskom-dome	Кухня в деревенском доме	Кухня в деревенском доме	Кухня в деревенском доме	ru_RU		2018-03-10 13:45:35.917753+00	2018-03-10 13:45:35.917775+00	<p>Кухня в деревенском доме<br />\r\nИвановская область</p>	1000	t	\N	f	t	\N
36	Оформление окон в спальне	Оформление окон в спальне\r\nСамара 2009	oformlenie-okon-v-spalne	Оформление окон в спальне	Оформление окон в спальне	Оформление окон в спальне	ru_RU		2018-03-10 14:03:15.616757+00	2018-03-10 14:26:31.353433+00	<h4>Оформление окон в спальне</h4>\r\n\r\n<p>Самара 2009</p>	1000	t	\N	f	t	\N
28	Между этажами	Между этажами	mezhdu-etazhami	Между этажами	Между этажами	Между этажами	ru_RU		2018-03-10 13:54:27.65945+00	2018-03-10 13:54:27.659472+00	<p>Между этажами</p>	1000	t	\N	f	t	\N
18	Лестница в бревенчатый дом	Лестница в бревенчатый дом\r\nКлассический стиль	lestnica-v-brevenchatyj-dom	Лестница в бревенчатый дом	Лестница в бревенчатый дом	Лестница в бревенчатый дом	ru_RU		2018-03-10 09:32:40.532996+00	2018-03-10 13:56:18.763347+00	<h4>Лестница в бревенчатый дом</h4>\r\n\r\n<p>Классический стиль</p>	1000	t	\N	f	t	\N
31	Бильярдная	Бильярдная\r\nИваново 2010	bilyardnaya	Бильярдная	Бильярдная	Бильярдная	ru_RU		2018-03-10 13:57:53.105023+00	2018-03-10 13:58:06.521528+00	<p>Бильярдная<br />\r\nИваново 2010</p>	1000	t	\N	f	t	\N
33	Портал, декорации на стенах и потолке	Портал, декорации на стенах и потолке\r\nЯрославль 2012	portal-dekoracii-na-stenah-i-potolke	Портал, декорации на стенах и потолке	Портал, декорации на стенах и потолке	Портал, декорации на стенах и потолке	ru_RU		2018-03-10 14:00:44.584453+00	2018-03-10 14:00:44.584475+00	<p>Портал, декорации на стенах и потолке<br />\r\nЯрославль 2012</p>	1000	t	\N	f	t	\N
34	Порталы межкомнатные	Порталы межкомнатные\r\nПлес	portaly-mezhkomnatnye	Порталы межкомнатные	Порталы межкомнатные	Порталы межкомнатные	ru_RU		2018-03-10 14:01:35.723392+00	2018-03-10 14:01:35.723439+00	<p>Порталы межкомнатные<br />\r\nПлес</p>	1000	t	\N	f	t	\N
32	Дверь, шкаф, детская кровать	Дверь, шкаф, детская кровать и отделка\r\nМосква 2014	dver-shkaf-detskaya-krovat	Дверь, шкаф, детская кровать	Дверь, шкаф, детская кровать	Дверь, шкаф, детская кровать	ru_RU		2018-03-10 13:59:57.493094+00	2018-03-10 14:26:38.59448+00	<p>Дверь, шкаф, детская кровать и отделка<br />\r\nМосква 2014</p>	1000	t	\N	f	t	\N
30	Двери с порталами из массива	Двери с порталами из массива\r\nЯрославль 2011	dveri-s-portalami-iz-massiva	Двери с порталами из массива	Двери с порталами из массива	Двери с порталами из массива	ru_RU		2018-03-10 13:55:30.949299+00	2018-03-10 14:26:45.517185+00	<h4>Двери с порталами из массива</h4>\r\n\r\n<p>Ярославль 2011</p>	1000	t	\N	f	t	\N
29	Прихожая	Прихожая\r\nМосква, 2014	prihozhaya	Прихожая	Прихожая	Прихожая	ru_RU		2018-03-10 13:55:01.92113+00	2018-03-10 14:26:52.176189+00	<p>Прихожая<br />\r\nМосква, 2014</p>	1000	t	\N	f	t	\N
38	Восточная комната	Восточная комната\r\nСамара, массив дуб	vostochnaya-komnata	Восточная комната	Восточная комната	Восточная комната	ru_RU		2018-03-10 14:05:36.531755+00	2018-03-10 14:05:36.531777+00	<h4>Восточная комната</h4>\r\n\r\n<p>Самара, массив дуб</p>	1000	t	\N	f	t	\N
27	Холл и Зал заседаний	Зал заседаний, Холл обл.суда\r\nИваново 2013	holl-i-zal-zasedanij	Холл и Зал заседаний	Холл и Зал заседаний	Холл и Зал заседаний	ru_RU		2018-03-10 13:52:40.762485+00	2018-03-10 14:07:50.669797+00	<p>Зал заседаний, Холл обл.суда<br />\r\nИваново 2013</p>	1000	t	\N	f	t	\N
42	Арка с колонной	Арка с колонной\r\nбарельеф, дуб, поталь	arka-s-kolonnoj	Арка с колонной	Арка с колонной	Арка с колонной	ru_RU		2018-03-10 14:09:58.721441+00	2018-03-10 14:09:58.721462+00	<h4>арка с колонной</h4>\r\n\r\n<p>барельеф, дуб, поталь</p>	1000	t	\N	f	t	\N
43	Детская белая комната	Детская комната\r\nМосква 2014	detskaya-komnata	Детская белая комната	Детская белая комната	Детская белая комната	ru_RU		2018-03-10 14:11:45.435219+00	2018-03-10 14:11:45.435248+00	<h4>Детская комната</h4>\r\n\r\n<p>Москва 2014</p>	1000	t	\N	f	t	\N
44	Коридор в деревенском стиле	Коридор в деревенском стиле\r\nмассив	koridor-v-derevenskom-stile	Коридор в деревенском стиле	Коридор в деревенском стиле	Коридор в деревенском стиле	ru_RU		2018-03-10 14:12:34.60929+00	2018-03-10 14:12:34.609313+00	<h4>Коридор в деревенском стиле</h4>\r\n\r\n<p>массив</p>	1000	t	\N	f	t	\N
45	Детский городок	Детский городок\r\nИваново 2013	detskij-gorodok	Детский городок	Детский городок	Детский городок	ru_RU		2018-03-10 14:13:22.627813+00	2018-03-10 14:13:22.627841+00	<h4>Детский городок</h4>\r\n\r\n<p>Иваново 2013</p>	1000	t	\N	f	t	\N
46	шкаф в кабинет	шкаф в кабинет\r\nМосква 2014	shkaf-v-kabinet	шкаф в кабинет	шкаф в кабинет	шкаф в кабинет	ru_RU		2018-03-10 14:17:21.433734+00	2018-03-10 14:17:21.433776+00	<h4>шкаф в кабинет</h4>\r\n\r\n<p>Москва 2014</p>	1000	t	\N	f	t	\N
48	стол, табуретки, подставка под цветы	стол, табуретки, подставка под цветы\r\nстолешница, инкрустация, хвоя	stol-taburetki	стол, табуретки	стол, табуретки	стол, табуретки	ru_RU		2018-03-10 14:18:55.620751+00	2018-03-10 14:20:36.902432+00	<h4>стол, табуретки</h4>\r\n\r\n<p>столешница, инкрустация, хвоя</p>	1000	t	\N	f	t	\N
50	Тумба, плинтус, кровать	Тумба, плинтус*, кровать\r\nМосква 2014	tumba-plintus-krovat	Тумба, плинтус, кровать	Тумба, плинтус, кровать	Тумба, плинтус, кровать	ru_RU		2018-03-10 14:22:17.982247+00	2018-03-10 14:22:17.982308+00	<h4>Тумба, плинтус*, кровать</h4>\r\n\r\n<p>Москва 2014</p>	1000	t	\N	f	t	\N
51	Стол с выдвижной панелью	Стол с выдвижной панелью из дуба\r\nМосква 2012	stol-s-vydvizhnoj-panelyu	Стол с выдвижной панелью	Стол с выдвижной панелью	Стол с выдвижной панелью	ru_RU		2018-03-10 14:23:15.885397+00	2018-03-10 14:23:15.885418+00	<h4>Стол с выдвижной панелью из дуба</h4>\r\n\r\n<p>Москва 2012</p>	1000	t	\N	f	f	\N
52	Гардеробная комната, бюджетный вариант	Гардеробная комната, бюджетный вариант. Ламинат + массив\r\nМосковская область, 2011	garderobnaya-komnata-byudzhetnyj-variant	Гардеробная комната, бюджетный вариант	Гардеробная комната, бюджетный вариант	Гардеробная комната, бюджетный вариант	ru_RU		2018-03-10 14:24:00.723955+00	2018-03-10 14:24:00.723983+00	<h4>Гардеробная комната, бюджетный вариант. Ламинат + массив</h4>\r\n\r\n<p>Московская область, 2011</p>	1000	t	\N	f	f	\N
53	шкаф для посуды	шкаф для посуды\r\nМосква 2014	shkaf-dlya-posudy	шкаф для посуды	шкаф для посуды	шкаф для посуды	ru_RU		2018-03-10 14:24:27.923715+00	2018-03-10 14:24:52.987665+00	<h4>шкаф для посуды</h4>\r\n\r\n<p>Москва 2014</p>	1000	t	\N	f	f	\N
49	Шкаф домашней библиотеки	Шкаф домашней библиотеки\r\nмассив сосна	shkaf-domashnej-biblioteki	Шкаф домашней библиотеки	Шкаф домашней библиотеки	Шкаф домашней библиотеки	ru_RU		2018-03-10 14:21:14.575028+00	2018-03-10 14:25:21.150117+00	<h4>Шкаф домашней библиотеки</h4>\r\n\r\n<p>массив сосна</p>	1000	t	\N	f	f	\N
47	Обеденный стол	Обеденный стол для комнаты	obedennyj-stol	Обеденный стол	Обеденный стол	Обеденный стол	ru_RU		2018-03-10 14:18:12.591551+00	2018-03-10 14:25:37.784557+00	<h4>Обеденный стол для комнаты</h4>	1000	t	\N	f	t	\N
39	Детская комната	Детская комната\r\nМосква 2014	detskaya-komnata	Детская комната	Детская комната	Детская комната	ru_RU		2018-03-10 14:06:13.863941+00	2018-03-10 14:26:09.775844+00	<p>Детская комната<br />\r\nМосква 2014</p>	1000	t	\N	f	t	\N
37	Комната в классическом стиле	Комната в классическом стиле\r\nИваново 2013	komnata-v-klassicheskom-stile	Комната в классическом стиле	Комната в классическом стиле	Комната в классическом стиле	ru_RU		2018-03-10 14:04:33.356435+00	2018-03-10 14:26:16.375544+00	<h4>Комната в классическом стиле</h4>\r\n\r\n<p>Иваново 2013</p>	1000	t	\N	f	t	\N
40	Декоративная-люстра в зал	Декоративная-люстра в зал\r\nПлес,Ивановская область	dekorativnaya-lyustra-v-zal	Декоративная-люстра в зал	Декоративная-люстра в зал	Декоративная-люстра в зал	ru_RU		2018-03-10 14:06:50.714981+00	2018-03-10 14:27:32.865344+00	<h4>Декоративная-люстра в зал</h4>\r\n\r\n<p>Плес,Ивановская область</p>	1000	t	\N	f	t	\N
54	Журнальный стол из бука	Журнальный стол из бука, позолота поталью\r\nМосква, Немчиновка	zhurnalnyj-stol-iz-buka	Журнальный стол из бука	Журнальный стол из бука	Журнальный стол из бука	ru_RU		2018-03-10 14:28:28.878355+00	2018-03-10 14:28:28.878442+00	<h4>Журнальный стол из бука, позолота поталью</h4>\r\n\r\n<p>Москва, Немчиновка</p>	1000	t	\N	f	t	\N
55	Резьба - ручная работа	Резьба - ручная работа\r\nПодарок (липа)	rezba-ruchnaya-rabota	Резьба - ручная работа	Резьба - ручная работа	Резьба - ручная работа	ru_RU		2018-03-10 14:29:23.249405+00	2018-03-10 14:29:23.249427+00	<h4>Резьба - ручная работа</h4>\r\n\r\n<p>Подарок (липа)</p>	1000	t	\N	f	f	\N
56	Камин из дуба	Камин из дуба\r\nМосква 2007	kamin-iz-duba	Камин из дуба	Камин из дуба	Камин из дуба	ru_RU		2018-03-10 14:29:55.838077+00	2018-03-10 14:29:55.838099+00	<h4>Камин из дуба</h4>\r\n\r\n<p>Москва 2007</p>	1000	t	\N	f	f	\N
57	Дверной карниз	Дверной карниз\r\nМосква, 2014	dvernoj-karniz	Дверной карниз	Дверной карниз	Дверной карниз	ru_RU		2018-03-10 14:30:43.306173+00	2018-03-10 14:30:43.306195+00	<h4>Дверной карниз</h4>\r\n\r\n<p>Москва, 2014</p>	1000	t	\N	f	f	\N
14	Лестница из дуба	Лестница, лестничный марш\r\nмассив дуб, \r\n\r\nКозино, Московская обл. 2005	lestnica-iz-duba	Лестница из дуба	Лестница из дуба	Лестница из дуба	ru_RU		2018-03-10 09:27:28.362956+00	2018-03-13 17:54:23.537482+00	<h4>Лестница из дуба</h4>\r\n\r\n<p>Козино, Московская обл. 2005</p>	1000	t	\N	f	t	\N
58	Декоративный фрагмент портала	Декоративный фрагмент портала\r\nмассив дуб	dekorativnyj-fragment-portala	Декоративный фрагмент портала	Декоративный фрагмент портала	Декоративный фрагмент портала	ru_RU		2018-03-10 14:31:23.48801+00	2018-03-10 14:31:23.488043+00	<h4>Декоративный фрагмент портала</h4>\r\n\r\n<p>массив дуб</p>	1000	t	\N	f	f	\N
59	Фрагмент арки	Фрагмент арки\r\nмассив	fragment-arki	Фрагмент арки	Фрагмент арки	Фрагмент арки	ru_RU		2018-03-10 14:31:56.306682+00	2018-03-10 14:31:56.306707+00	<h4>Фрагмент арки</h4>\r\n\r\n<p>массив</p>	1000	t	\N	f	f	\N
60	Поручень (ручная работа)	Поручень (ручная работа)\r\nИваново 2005	poruchen-ruchnaya-rabota	Поручень (ручная работа)	Поручень (ручная работа)	Поручень (ручная работа)	ru_RU		2018-03-10 14:32:28.461053+00	2018-03-10 14:32:28.461074+00	<h4>Поручень (ручная работа)</h4>\r\n\r\n<p>Иваново 2005</p>	1000	t	\N	f	f	\N
61	Тумба под аппаратуру	Тумба под аппаратуру, стиль "ретро"\r\nШуя 2009	tumba-pod-apparaturu	Тумба под аппаратуру	Тумба под аппаратуру	Тумба под аппаратуру	ru_RU		2018-03-10 14:33:14.886919+00	2018-03-10 14:33:14.886952+00	<h4>Тумба под аппаратуру, стиль &quot;ретро&quot;</h4>\r\n\r\n<p>Шуя 2009</p>	1000	t	\N	f	t	\N
62	Усиленная входная дверь	Усиленная входная дверь\r\nШуя, массив дуб	usilennaya-vhodnaya-dver	Усиленная входная дверь	Усиленная входная дверь	Усиленная входная дверь	ru_RU		2018-03-10 14:34:37.421848+00	2018-03-10 14:34:37.421869+00	<h4>Усиленная входная дверь</h4>\r\n\r\n<p>Шуя, массив дуб</p>	1000	t	\N	f	f	\N
63	Балконный блок	Балконный блок\r\nЯрославль	balkonnyj-blok	Балконный блок	Балконный блок	Балконный блок	ru_RU		2018-03-10 14:35:32.818302+00	2018-03-10 14:35:32.818337+00	<h4>Балконный блок</h4>\r\n\r\n<p>Ярославль</p>	1000	t	\N	f	f	\N
64	Элементы лестничного марша	Элементы лестничного марша	elementy-lestnichnogo-marsha	Элементы лестничного марша	Элементы лестничного марша	Элементы лестничного марша	ru_RU		2018-03-10 14:36:15.889362+00	2018-03-10 14:36:15.889394+00	<h4>Элементы лестничного марша</h4>	1000	t	\N	t	f	\N
65	Арка для сауны	Арка для сауны\r\nНерль	arka-dlya-sauny	Арка для сауны	Арка для сауны	Арка для сауны	ru_RU		2018-03-10 14:36:52.380998+00	2018-03-10 14:36:52.381021+00	<h4>Арка для сауны</h4>\r\n\r\n<p>Нерль</p>	1000	t	\N	f	f	\N
66	роспись на филенки для дверей	роспись на филенки для дверей\r\nхвоя, роспись	rospis-na-filenki-dlya-dverej	роспись на филенки для дверей	роспись на филенки для дверей	роспись на филенки для дверей	ru_RU		2018-03-10 14:37:28.066066+00	2018-03-10 14:37:28.066089+00	<h4>роспись на филенки для дверей</h4>\r\n\r\n<p>хвоя, роспись</p>	1000	t	\N	f	f	\N
41	Спальная комната	Спальная комната\r\nЯрославль 2010	spalnaya-komnata	Спальная комната	Спальная комната	Спальная комната	ru_RU		2018-03-10 14:08:44.723899+00	2018-03-10 16:31:28.341248+00	<h4>Спальная комната</h4>\r\n\r\n<p>Ярославль 2010</p>	1000	t	\N	f	t	\N
35	Кровать, дверь, тумба, шкаф	Кровать, дверь, тумба, шкаф\r\nДеревянный стиль в классическом исполнении	krovat-dver-tumba-shkaf	Кровать, дверь, тумба, шкаф	Кровать, дверь, тумба, шкаф	Кровать, дверь, тумба, шкаф	ru_RU		2018-03-10 14:02:23.652394+00	2018-03-11 07:23:56.163794+00	<p>Кровать, дверь, тумба, шкаф<br />\r\nДеревянный стиль в классическом исполнении</p>	1000	t	\N	f	t	\N
\.


--
-- Data for Name: product_product_catalog; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.product_product_catalog (id, product_id, catalog_id) FROM stdin;
1	1	1
3	1	7
4	2	1
6	2	7
7	4	1
11	4	7
12	5	1
13	5	7
14	6	1
15	6	7
16	7	1
17	7	7
19	8	7
22	9	7
23	9	1
24	8	1
25	3	1
26	3	7
27	2	5
28	10	1
29	10	7
30	11	1
31	11	7
32	12	1
33	12	7
34	12	5
35	13	1
36	13	7
37	14	1
38	14	7
39	15	1
40	15	7
41	16	1
42	16	7
43	17	1
44	17	7
45	18	1
46	18	3
47	18	7
48	19	2
49	19	3
50	19	7
51	20	2
52	20	7
53	21	2
54	21	7
55	22	2
56	22	7
57	23	2
58	23	7
59	24	2
60	24	7
61	25	2
62	25	7
63	26	2
64	26	7
65	27	3
66	27	4
67	27	7
68	28	3
69	28	5
70	28	7
71	31	3
72	31	5
73	31	7
74	33	3
75	33	7
76	34	3
77	34	7
78	35	3
79	35	7
80	38	3
81	38	5
82	38	7
83	42	3
84	42	7
85	43	3
86	43	4
87	43	5
88	43	7
89	44	3
90	44	7
91	45	3
92	45	4
93	45	5
94	45	7
95	46	4
96	46	7
97	48	4
98	48	5
99	48	7
100	50	3
101	50	4
102	50	5
103	50	7
104	51	4
105	51	7
106	52	4
107	52	7
108	53	4
109	53	7
110	49	4
111	49	7
112	47	4
113	47	7
114	41	3
115	41	4
116	41	7
117	39	3
118	39	7
119	37	3
120	37	7
121	36	3
122	36	5
123	36	7
124	32	3
125	32	7
126	30	3
127	30	7
128	29	3
129	29	7
130	40	3
131	40	5
132	40	7
133	54	4
134	54	5
135	54	7
136	55	5
137	55	7
138	56	5
139	56	7
140	57	5
141	57	7
142	58	5
143	58	7
144	59	5
145	59	7
146	60	5
147	60	7
148	61	4
149	61	5
150	61	7
151	62	3
152	62	5
153	62	7
154	63	5
155	63	7
156	64	1
157	64	5
158	64	7
159	65	5
160	65	7
161	66	5
162	66	7
\.


--
-- Name: product_product_catalog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.product_product_catalog_id_seq', 162, true);


--
-- Name: product_product_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.product_product_id_seq', 66, true);


--
-- Data for Name: product_product_recommend_products; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.product_product_recommend_products (id, from_product_id, to_product_id) FROM stdin;
\.


--
-- Name: product_product_recommend_products_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.product_product_recommend_products_id_seq', 9, true);


--
-- Data for Name: product_productcomment; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.product_productcomment (id, created, updated, text, ip_address, username, email, is_show, product_id, user_id) FROM stdin;
6	2018-03-03 09:41:24.09+00	2018-03-03 09:55:48.397+00	111111111	0.0.0.0	anonymous	marychev@garpix.com	t	8	\N
7	2018-03-03 09:41:41.306+00	2018-03-03 09:55:48.427+00	erere	0.0.0.0	anonymous	marychev@garpix.com	t	7	\N
8	2018-03-03 09:51:50.287+00	2018-03-03 09:51:50.287+00	dsfcsdsdf	0.0.0.0	anonymous	marychev@garpix.com	f	7	\N
9	2018-03-03 09:52:55.816+00	2018-03-03 09:55:48.45+00	zsczsczsczsc	0.0.0.0	anonymous	marychev@garpix.com	t	7	\N
10	2018-03-03 09:56:00.572+00	2018-03-03 09:56:00.572+00	xdcxdcxd	0.0.0.0	anonymous		f	7	\N
11	2018-03-03 10:06:39.466+00	2018-03-03 10:06:39.466+00	drfgd	0.0.0.0	anonymous		f	8	\N
12	2018-03-03 10:07:01.285+00	2018-03-03 10:07:01.285+00	wdadwda	0.0.0.0	anonymous	marychev.mihail@ya.ru	f	8	\N
13	2018-03-03 10:13:37.626+00	2018-03-03 10:13:37.626+00	dvx	0.0.0.0	anonymous	marychev_mihail@mail.ru	f	7	\N
14	2018-03-03 10:37:31.28+00	2018-03-03 10:37:31.28+00	rgrgr	0.0.0.0	anonymous		f	2	\N
15	2018-03-03 11:57:32.671+00	2018-03-03 11:57:32.671+00	swsw	0.0.0.0	anonymous		f	8	\N
16	2018-03-03 11:57:37.587+00	2018-03-03 11:57:37.587+00	wdwad	0.0.0.0	anonymous		f	8	\N
17	2018-03-03 11:58:36.288+00	2018-03-03 11:58:36.288+00	edfdf	0.0.0.0	anonymous		f	8	\N
18	2018-03-03 11:59:29.231+00	2018-03-03 11:59:29.231+00	sefes	0.0.0.0	anonymous		f	8	\N
19	2018-03-03 12:00:00.385+00	2018-03-03 12:00:00.385+00	serserserserserser	0.0.0.0	anonymous		f	7	\N
20	2018-03-03 12:05:45.146+00	2018-03-03 12:05:45.146+00	fgdfgdfg	0.0.0.0	anonymous		f	8	\N
21	2018-03-03 12:06:07.292+00	2018-03-03 12:06:07.292+00	sdfsdfsdf	0.0.0.0	anonymous	marychevmihail3737@gmail.com	f	5	\N
\.


--
-- Name: product_productcomment_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.product_productcomment_id_seq', 21, true);


--
-- Data for Name: product_productimage; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.product_productimage (id, image, image_title, image_is_main, image_description, product_id) FROM stdin;
19	портфолио/лестницы/photo_23.JPG	Ограждение лестничного марша	t		12
20	портфолио/лестницы/eb1f3327d84528126dbe15a43e4b2c62.jpg	Лестница "отбеленный" дуб	t		13
43	портфолио/лестницы/photo2014_13.JPG	Лестница в бревенчатый дом	f		18
18	портфолио/лестницы/photo_20.JPG	Лестница с позолотой	f		11
21	портфолио/лестницы/photo2014_17.JPG	Лестница с позолотой	t		11
23	портфолио/лестницы/2f999b87a08c9ee4005b8f61f9a45ac5.jpg	Лестница, дуб	t		15
24	портфолио/лестницы/photo2014_4.jpg	Лестница из массива дуб	t		16
25	портфолио/лестницы/ae3eea86a020d16136ea075971b0c59b.jpg	Лестница, дверь	t		17
27	портфолио/кухни/41e6f254ef764e13a961b60bc8583f1d.JPG	Кухня с выходом на балкон	f		19
28	портфолио/кухни/3bd9740f1b08e546d3b3d7f3d1fa3ead.JPG	Кухня с выходом на балкон	t		19
29	портфолио/кухни/photo2014_а.jpg	Кухня с выходом на балкон	f		19
1	портфолио/лестницы/7e2ca214b45128f0e1d9ac82ac434f0e.jpg	Кухонный стол из дуба	t		9
6	продукция/лестницы/043f4444b3f19902b4c3fbe294af2284.jpg	Кухонный стол из дуба	f		9
7	галерея/photo2014_9.JPG	Кухонный стол из дуба	f		9
8	продукция/лестницы/photo2014_17.jpg	Кухонный стол из дуба	f		9
2	портфолио/лестницы/photo2014_26.JPG	Кухня в деревянном доме	t		8
13	maxresdefault.jpg	Кухня в деревянном доме	f		8
30	портфолио/кухни/photo_15.JPG	Кухня с выходом на балкон	f		19
31	портфолио/кухни/photo2014_6.JPG	Кухня в деревянном доме	t		20
4	портфолио/лестницы/photo2014_28.JPG	Лестница из массива сосны	t		6
32	портфолио/кухни/photo2014_15.JPG	Кухня из дуба	t		21
5	портфолио/лестницы/b2c6af8b9e25282d1dfdf64e71d34c75.JPG	Прихожая - Лестница ,дверь, массив сосна	t		5
33	портфолио/кухни/photo2014_25.JPG	Кухонный гарнитур	t		22
34	портфолио/кухни/photo2014_22.JPG	Кухня классическая	t		23
35	портфолио/кухни/27700181b48eba86892f3027f039aad0.JPG	Кухонный гарнитур, орех	t		24
9	портфолио/лестницы/photo2014_23.JPG	Лестница в квартиру	t		4
36	портфолио/кухни/photo_35.JPG	Кухня классика	t		25
10	портфолио/лестницы/043f4444b3f19902b4c3fbe294af2284.jpg	Мансардная лестница, хвоя	t		3
11	портфолио/лестницы/2134b61db316c9545a8081ae0453db6b.JPG	Фрагмент лестничного ограждения	t		2
3	портфолио/лестницы/photo2014_18.JPG	Лестница на больцах из дуба	f		7
16	портфолио/лестницы/photo_22.JPG	Лестница на больцах из дуба	t		7
12	портфолио/лестницы/photo_17.JPG	Прихожая	t		1
14	maxresdefault.jpg	Прихожая	f		1
15	вакансии/94_carpenter_03.jpg	Прихожая	f		1
17	портфолио/лестницы/photo_14.JPG	Лестница резная, дуб	t		10
37	портфолио/кухни/photo2014_.jpg	Кухня в деревенском доме	t		26
57	портфолио/интерьеры/c9a8065aa4104e8cbaaf424eb4dcc528.jpg	Арка с колонной	t		42
40	портфолио/интерьеры/photo_34.JPG	Между этажами	t		28
26	портфолио/лестницы/photo2014_12.JPG	Лестница в бревенчатый дом	t		18
44	портфолио/интерьеры/photo2014_19.JPG	Бильярдная	f		31
45	портфолио/интерьеры/photo2014_24.JPG	Бильярдная	f		31
47	портфолио/интерьеры/photo2014_29.JPG	Портал, декорации на стенах и потолке	t		33
48	портфолио/интерьеры/photo2014_21.JPG	Порталы межкомнатные	t		34
52	портфолио/интерьеры/photo_26.JPG	Восточная комната	t		38
38	портфолио/интерьеры/photo2014_34.JPG	Холл	f		27
39	портфолио/интерьеры/photo_21.JPG	Зал заседаний	t		27
55	портфолио/интерьеры/photo2014_35.JPG	Холл и Зал заседаний	f		27
58	портфолио/интерьеры/photo2014_8.JPG	Детская белая комната	t		43
22	портфолио/лестницы/ab5742e3dd38874fa3dbb1213ad3bdb9.JPG	Лестница из дуба	t		14
53	портфолио/интерьеры/photo2014_9.JPG	Детская комната	t		39
50	портфолио/интерьеры/photo2014_33.JPG	Оформление окон в спальне	t		36
46	портфолио/интерьеры/ab584693be2e3ff09617199596b66e5a.jpg	Дверь, шкаф, детская кровать	t		32
42	портфолио/интерьеры/photo2014_30.JPG	Двери с порталами из массива	t		30
41	портфолио/интерьеры/721850565a6a7472882523c72b55c9d9.jpg	Прихожая	t		29
54	портфолио/интерьеры/photo_18.JPG	Декоративная-люстра в зал	t		40
59	портфолио/интерьеры/photo2014_27.JPG	Коридор в деревенском стиле	t		44
60	портфолио/интерьеры/photo_27.JPG	Детский городок	t		45
61	портфолио/мебель/photo2014_10.JPG	шкаф в кабинет	t		46
63	портфолио/мебель/351ef7e4bef43f6ca614780877fa32e4.jpg	стол, табуретки	t		48
64	портфолио/мебель/6759a87042aae76626f20c8b03874179.jpg	подставка под цветы	f		48
66	портфолио/мебель/photo2014_7.JPG	Тумба, плинтус, кровать	t		50
67	портфолио/мебель/283811220a15c2e66ea310cf981710c7.jpg	Стол с выдвижной панелью	t		51
68	портфолио/мебель/5c41c85036b76dbbef1b46ff90a0a958.jpg	Гардеробная комната, бюджетный вариант	t		52
69	портфолио/мебель/daf44fa0e8bd9f3b149f3cb28f294fbb.JPG	шкаф для посуды	t		53
65	портфолио/мебель/photo2014_20.JPG	Шкаф домашней библиотеки	t		49
62	портфолио/мебель/c512f60f283360161f0ed31b2f5df30f.jpg	Обеденный стол	t		47
51	портфолио/интерьеры/photo_36.JPG	Комната в классическом стиле	t		37
70	портфолио/декор/e1263652ba00186c9e2370292deb0237.jpg	Журнальный стол из бука	t		54
71	портфолио/декор/photo2014_14.JPG	Резьба - ручная работа	t		55
72	портфолио/декор/photo2014_5.JPG	Камин из дуба	t		56
73	портфолио/декор/bbab9d205d7d2a2164b7d25691fc0b8e.jpg	Дверной карниз	t		57
74	портфолио/декор/photo2014_2.jpg	Декоративный фрагмент портала	t		58
75	портфолио/декор/e74d853034f4feb8a364492a4700abf4.jpg	Фрагмент арки	t		59
76	портфолио/декор/photo2014_11.JPG	Поручень (ручная работа)	t		60
77	портфолио/декор/09417d8aaf115b0ddfe7f74ec17442da.JPG	Тумба под аппаратуру	t		61
78	портфолио/декор/photo_25.JPG	Усиленная входная дверь	t		62
79	портфолио/декор/photo2014_31.JPG	Балконный блок	t		63
80	портфолио/декор/a094efb6d7373d789d1aeebad17a401b.jpeg	Элементы лестничного марша	t		64
81	портфолио/декор/da00a8895468d426ec76854f051c4555.jpg	Арка для сауны	t		65
82	портфолио/декор/035cb8bab693269ac485e05118a9a5ff.jpg	роспись на филенки для дверей	t		66
56	портфолио/интерьеры/photo_16.JPG	Спальная комната	t		41
49	портфолио/интерьеры/photo2014_16.JPG	Кровать, дверь, тумба, шкаф	f		35
\.


--
-- Name: product_productimage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.product_productimage_id_seq', 83, true);


--
-- Data for Name: product_productitem; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.product_productitem (id, created, updated, name, articul, price, price_discount, price_purchase, quantity, is_main, product_id) FROM stdin;
\.


--
-- Name: product_productitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.product_productitem_id_seq', 2, true);


--
-- Data for Name: settings_template_footer; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.settings_template_footer (id, title, sort, is_show, text_info_id) FROM stdin;
1	Нижний футер	0	t	2
\.


--
-- Data for Name: settings_template_footer_blogs; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.settings_template_footer_blogs (id, footer_id, blog_id) FROM stdin;
3	1	3
\.


--
-- Name: settings_template_footer_blogs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.settings_template_footer_blogs_id_seq', 3, true);


--
-- Data for Name: settings_template_footer_catalog; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.settings_template_footer_catalog (id, footer_id, catalog_id) FROM stdin;
4	1	2
\.


--
-- Name: settings_template_footer_catalog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.settings_template_footer_catalog_id_seq', 4, true);


--
-- Data for Name: settings_template_footer_galleries; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.settings_template_footer_galleries (id, footer_id, gallery_id) FROM stdin;
3	1	4
\.


--
-- Name: settings_template_footer_galleries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.settings_template_footer_galleries_id_seq', 3, true);


--
-- Name: settings_template_footer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.settings_template_footer_id_seq', 1, true);


--
-- Data for Name: settings_template_footer_list_link; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.settings_template_footer_list_link (id, footer_id, listlink_id) FROM stdin;
\.


--
-- Name: settings_template_footer_list_link_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.settings_template_footer_list_link_id_seq', 1, true);


--
-- Data for Name: settings_template_footer_page; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.settings_template_footer_page (id, footer_id, page_id) FROM stdin;
3	1	1
\.


--
-- Name: settings_template_footer_page_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.settings_template_footer_page_id_seq', 3, true);


--
-- Data for Name: settings_template_settingstemplate; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.settings_template_settingstemplate (id, title, is_included, phone, address, logo, robots_txt, terms_of_use, scripts, footer_id, home_id, site_id) FROM stdin;
1	Основной шаблон	t	+7(915)-832-38-36	153051 г.Иваново, ул. Домостроителей, 2 лит А	logo/logo1.png	User-agent: Yandex \r\nDisallow: /admin			1	1	1
\.


--
-- Name: settings_template_settingstemplate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.settings_template_settingstemplate_id_seq', 1, true);


--
-- Data for Name: site_info_listlink; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.site_info_listlink (id, title, url, sort, is_show, type_link) FROM stdin;
1	тестова ссылка	http://google.com	0	t	T
\.


--
-- Name: site_info_listlink_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.site_info_listlink_id_seq', 1, true);


--
-- Data for Name: site_info_socialnetwork; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.site_info_socialnetwork (id, title, image, html_link, url, image_description, image_is_main, image_title) FROM stdin;
1	Мы в контакте	\N	<a class="fa fa-vk" href="https://vk.com/public85789063" target="blank"></a>	http://google.com	\N	f	Мы в контакте
2	Воплощаем сказку в реальность!	f.jpg	\N	https://vk.com/public85789063	\N	f	Воплощаем сказку в реальность!
\.


--
-- Name: site_info_socialnetwork_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.site_info_socialnetwork_id_seq', 2, true);


--
-- Data for Name: site_info_textinfo; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.site_info_textinfo (id, title, html) FROM stdin;
1	Наш Адрес:	<h4>153051 г.Иваново, ул. Домостроителей, 2 лит А<br />\r\nТелефон:&nbsp;+7(915)-832-38-36<br />\r\nПочта:&nbsp;<a href="mailto:revcloud@mail.ru">revcloud@mail.ru</a></h4>
2	Часы Работы:	<h4>Понедельник - Пятница:08:00 - 18:00</h4>\r\n\r\n<ul>\r\n\t<li>Суббота:08:00 - 14:00</li>\r\n\t<li>Воскресенье:Выходной</li>\r\n</ul>
3	Почему мы?	<p><img src="http://dervek.ru/img/ok_my.png" /><a href="http://dervek.ru/portfolio">Наша продукция из&nbsp;<strong>натурального массива!</strong></a></p>\r\n\r\n<p><img src="http://dervek.ru/img/dengi_my.png" /><a href="http://dervek.ru/about">Наше производство - Ваша&nbsp;<strong>экономия!</strong></a></p>\r\n\r\n<p><img src="http://dervek.ru/img/kolerovka_my.png" /><a href="http://dervek.ru/painting">New -&nbsp;<strong>Спецэффекты 3D!</strong>&nbsp;- Любые решения для колеровки</a></p>\r\n\r\n<p><img src="http://dervek.ru/img/art_my.png" /><a href="http://dervek.ru/portfolio">К&nbsp;<strong>столярному делу</strong>&nbsp;мы относимся как к искусству - с душой!</a></p>\r\n\r\n<p><img src="http://dervek.ru/img/dokument_my.png" /><a href="http://dervek.ru/contact">Увидеть&nbsp;<strong>продукцию</strong>&nbsp;можно перед&nbsp;<strong>оформлением заказа!</strong></a></p>
\.


--
-- Name: site_info_textinfo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.site_info_textinfo_id_seq', 3, true);


--
-- Data for Name: users_userlink; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.users_userlink (id, anchor, url, user_id) FROM stdin;
1	google	http://google.com	1
\.


--
-- Name: users_userlink_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.users_userlink_id_seq', 1, true);


--
-- Data for Name: users_userprofile; Type: TABLE DATA; Schema: public; Owner: dervek
--

COPY public.users_userprofile (id, avatar, patronymic, birthday, phone, address, about, id_signed_news, user_id) FROM stdin;
1	\N	Admin	1986-10-08	89203697923	Колесанова	\N	f	1
2	\N	\N	\N	\N	\N	\N	f	2
\.


--
-- Name: users_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: dervek
--

SELECT pg_catalog.setval('public.users_userprofile_id_seq', 2, true);


--
-- Name: advertising_sliderhome_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.advertising_sliderhome
    ADD CONSTRAINT advertising_sliderhome_pkey PRIMARY KEY (id);


--
-- Name: advertising_sliderhome_title_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.advertising_sliderhome
    ADD CONSTRAINT advertising_sliderhome_title_key UNIQUE (title);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: blog_blog_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_pkey PRIMARY KEY (id);


--
-- Name: blog_blog_title_parent_id_slug_581a794f_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_title_parent_id_slug_581a794f_uniq UNIQUE (title, parent_id, slug);


--
-- Name: blog_blogimage_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blogimage
    ADD CONSTRAINT blog_blogimage_pkey PRIMARY KEY (id);


--
-- Name: blog_comment_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_comment
    ADD CONSTRAINT blog_comment_pkey PRIMARY KEY (id);


--
-- Name: blog_post_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_post
    ADD CONSTRAINT blog_post_pkey PRIMARY KEY (id);


--
-- Name: blog_postimage_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_postimage
    ADD CONSTRAINT blog_postimage_pkey PRIMARY KEY (id);


--
-- Name: catalog_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_pkey PRIMARY KEY (id);


--
-- Name: catalog_catalog_title_parent_id_slug_03b445d7_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_title_parent_id_slug_03b445d7_uniq UNIQUE (title, parent_id, slug);


--
-- Name: catalog_catalogimage_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalogimage
    ADD CONSTRAINT catalog_catalogimage_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_domain_a2e37b91_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_domain_a2e37b91_uniq UNIQUE (domain);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: gallery_gallery_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_pkey PRIMARY KEY (id);


--
-- Name: gallery_gallery_title_parent_id_slug_4948e644_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_title_parent_id_slug_4948e644_uniq UNIQUE (title, parent_id, slug);


--
-- Name: gallery_galleryimage_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_galleryimage
    ADD CONSTRAINT gallery_galleryimage_pkey PRIMARY KEY (id);


--
-- Name: home_home_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.home_home
    ADD CONSTRAINT home_home_pkey PRIMARY KEY (id);


--
-- Name: home_home_title_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.home_home
    ADD CONSTRAINT home_home_title_key UNIQUE (title);


--
-- Name: home_homeimage_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.home_homeimage
    ADD CONSTRAINT home_homeimage_pkey PRIMARY KEY (id);


--
-- Name: include_area_includearea_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.include_area_includearea
    ADD CONSTRAINT include_area_includearea_pkey PRIMARY KEY (id);


--
-- Name: include_area_includearea_title_code_03a59a42_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.include_area_includearea
    ADD CONSTRAINT include_area_includearea_title_code_03a59a42_uniq UNIQUE (title, code);


--
-- Name: menu_mainmenu_name_parent_id_is_show_d30070c3_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_name_parent_id_is_show_d30070c3_uniq UNIQUE (name, parent_id, is_show);


--
-- Name: menu_mainmenu_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_pkey PRIMARY KEY (id);


--
-- Name: order_order_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_pkey PRIMARY KEY (id);


--
-- Name: order_orderitem_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_orderitem
    ADD CONSTRAINT order_orderitem_pkey PRIMARY KEY (id);


--
-- Name: order_status_name_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_name_key UNIQUE (name);


--
-- Name: order_status_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_status
    ADD CONSTRAINT order_status_pkey PRIMARY KEY (id);


--
-- Name: order_story_order_id_status_id_total_cost_6c166f53_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_order_id_status_id_total_cost_6c166f53_uniq UNIQUE (order_id, status_id, total_cost);


--
-- Name: order_story_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_pkey PRIMARY KEY (id);


--
-- Name: page_page_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_page
    ADD CONSTRAINT page_page_pkey PRIMARY KEY (id);


--
-- Name: page_page_slug_created_a4ecd142_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_page
    ADD CONSTRAINT page_page_slug_created_a4ecd142_uniq UNIQUE (slug, created);


--
-- Name: page_pagecomment_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_pagecomment
    ADD CONSTRAINT page_pagecomment_pkey PRIMARY KEY (id);


--
-- Name: page_pageimage_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_pageimage
    ADD CONSTRAINT page_pageimage_pkey PRIMARY KEY (id);


--
-- Name: product_product_articul_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_articul_key UNIQUE (articul);


--
-- Name: product_product_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_catalog_pkey PRIMARY KEY (id);


--
-- Name: product_product_catalog_product_id_catalog_id_e156d1b4_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_catalog_product_id_catalog_id_e156d1b4_uniq UNIQUE (product_id, catalog_id);


--
-- Name: product_product_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_pkey PRIMARY KEY (id);


--
-- Name: product_product_recommen_from_product_id_to_produ_881dcca6_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_recommen_from_product_id_to_produ_881dcca6_uniq UNIQUE (from_product_id, to_product_id);


--
-- Name: product_product_recommend_products_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_recommend_products_pkey PRIMARY KEY (id);


--
-- Name: product_product_title_slug_1dc8429a_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_title_slug_1dc8429a_uniq UNIQUE (title, slug);


--
-- Name: product_productcomment_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productcomment
    ADD CONSTRAINT product_productcomment_pkey PRIMARY KEY (id);


--
-- Name: product_productimage_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productimage
    ADD CONSTRAINT product_productimage_pkey PRIMARY KEY (id);


--
-- Name: product_productitem_articul_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productitem
    ADD CONSTRAINT product_productitem_articul_key UNIQUE (articul);


--
-- Name: product_productitem_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productitem
    ADD CONSTRAINT product_productitem_pkey PRIMARY KEY (id);


--
-- Name: product_productitem_product_id_name_ec7ae63d_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productitem
    ADD CONSTRAINT product_productitem_product_id_name_ec7ae63d_uniq UNIQUE (product_id, name);


--
-- Name: settings_template_footer_blogs_footer_id_blog_id_180aa4d1_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_footer_blogs_footer_id_blog_id_180aa4d1_uniq UNIQUE (footer_id, blog_id);


--
-- Name: settings_template_footer_blogs_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_footer_blogs_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_catalog_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_footer_catalog_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_footer_id_catalog_id_59a33efe_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_footer_footer_id_catalog_id_59a33efe_uniq UNIQUE (footer_id, catalog_id);


--
-- Name: settings_template_footer_footer_id_gallery_id_5431ae84_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_footer_footer_id_gallery_id_5431ae84_uniq UNIQUE (footer_id, gallery_id);


--
-- Name: settings_template_footer_footer_id_listlink_id_e083fab1_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_footer_footer_id_listlink_id_e083fab1_uniq UNIQUE (footer_id, listlink_id);


--
-- Name: settings_template_footer_galleries_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_footer_galleries_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_list_link_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_footer_list_link_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_page_footer_id_page_id_4c96a8bf_uniq; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_footer_page_footer_id_page_id_4c96a8bf_uniq UNIQUE (footer_id, page_id);


--
-- Name: settings_template_footer_page_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_footer_page_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer
    ADD CONSTRAINT settings_template_footer_pkey PRIMARY KEY (id);


--
-- Name: settings_template_footer_title_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer
    ADD CONSTRAINT settings_template_footer_title_key UNIQUE (title);


--
-- Name: settings_template_settingstemplate_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_settingstemplate_pkey PRIMARY KEY (id);


--
-- Name: settings_template_settingstemplate_site_id_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_settingstemplate_site_id_key UNIQUE (site_id);


--
-- Name: settings_template_settingstemplate_title_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_settingstemplate_title_key UNIQUE (title);


--
-- Name: site_info_listlink_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_listlink
    ADD CONSTRAINT site_info_listlink_pkey PRIMARY KEY (id);


--
-- Name: site_info_socialnetwork_html_link_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_socialnetwork
    ADD CONSTRAINT site_info_socialnetwork_html_link_key UNIQUE (html_link);


--
-- Name: site_info_socialnetwork_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_socialnetwork
    ADD CONSTRAINT site_info_socialnetwork_pkey PRIMARY KEY (id);


--
-- Name: site_info_textinfo_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_textinfo
    ADD CONSTRAINT site_info_textinfo_pkey PRIMARY KEY (id);


--
-- Name: site_info_textinfo_title_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.site_info_textinfo
    ADD CONSTRAINT site_info_textinfo_title_key UNIQUE (title);


--
-- Name: users_userlink_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.users_userlink
    ADD CONSTRAINT users_userlink_pkey PRIMARY KEY (id);


--
-- Name: users_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.users_userprofile
    ADD CONSTRAINT users_userprofile_pkey PRIMARY KEY (id);


--
-- Name: users_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.users_userprofile
    ADD CONSTRAINT users_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: advertising_sliderhome_title_2871ce14_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX advertising_sliderhome_title_2871ce14_like ON public.advertising_sliderhome USING btree (title varchar_pattern_ops);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: blog_blog_author_id_8791af69; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_author_id_8791af69 ON public.blog_blog USING btree (author_id);


--
-- Name: blog_blog_level_33dfff40; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_level_33dfff40 ON public.blog_blog USING btree (level);


--
-- Name: blog_blog_lft_5b58ade6; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_lft_5b58ade6 ON public.blog_blog USING btree (lft);


--
-- Name: blog_blog_parent_id_2195dcd3; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_parent_id_2195dcd3 ON public.blog_blog USING btree (parent_id);


--
-- Name: blog_blog_rght_0b0e6ca6; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_rght_0b0e6ca6 ON public.blog_blog USING btree (rght);


--
-- Name: blog_blog_slug_4812aa2c; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_slug_4812aa2c ON public.blog_blog USING btree (slug);


--
-- Name: blog_blog_slug_4812aa2c_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_slug_4812aa2c_like ON public.blog_blog USING btree (slug varchar_pattern_ops);


--
-- Name: blog_blog_title_942d8a1e; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_title_942d8a1e ON public.blog_blog USING btree (title);


--
-- Name: blog_blog_title_942d8a1e_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_title_942d8a1e_like ON public.blog_blog USING btree (title varchar_pattern_ops);


--
-- Name: blog_blog_tree_id_0a4b3027; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blog_tree_id_0a4b3027 ON public.blog_blog USING btree (tree_id);


--
-- Name: blog_blogimage_blog_id_17a42bec; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_blogimage_blog_id_17a42bec ON public.blog_blogimage USING btree (blog_id);


--
-- Name: blog_comment_post_id_580e96ef; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_comment_post_id_580e96ef ON public.blog_comment USING btree (post_id);


--
-- Name: blog_comment_user_id_59a54155; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_comment_user_id_59a54155 ON public.blog_comment USING btree (user_id);


--
-- Name: blog_post_author_id_dd7a8485; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_post_author_id_dd7a8485 ON public.blog_post USING btree (author_id);


--
-- Name: blog_post_blog_id_1d63f737; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_post_blog_id_1d63f737 ON public.blog_post USING btree (blog_id);


--
-- Name: blog_post_slug_b95473f2; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_post_slug_b95473f2 ON public.blog_post USING btree (slug);


--
-- Name: blog_post_slug_b95473f2_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_post_slug_b95473f2_like ON public.blog_post USING btree (slug varchar_pattern_ops);


--
-- Name: blog_post_title_adf2f203; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_post_title_adf2f203 ON public.blog_post USING btree (title);


--
-- Name: blog_post_title_adf2f203_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_post_title_adf2f203_like ON public.blog_post USING btree (title varchar_pattern_ops);


--
-- Name: blog_postimage_post_id_09cc1915; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX blog_postimage_post_id_09cc1915 ON public.blog_postimage USING btree (post_id);


--
-- Name: catalog_catalog_author_id_73764c6e; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_author_id_73764c6e ON public.catalog_catalog USING btree (author_id);


--
-- Name: catalog_catalog_level_88531b31; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_level_88531b31 ON public.catalog_catalog USING btree (level);


--
-- Name: catalog_catalog_lft_b0780e88; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_lft_b0780e88 ON public.catalog_catalog USING btree (lft);


--
-- Name: catalog_catalog_parent_id_fbb74747; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_parent_id_fbb74747 ON public.catalog_catalog USING btree (parent_id);


--
-- Name: catalog_catalog_rght_0cb95583; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_rght_0cb95583 ON public.catalog_catalog USING btree (rght);


--
-- Name: catalog_catalog_slug_b54675e0; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_slug_b54675e0 ON public.catalog_catalog USING btree (slug);


--
-- Name: catalog_catalog_slug_b54675e0_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_slug_b54675e0_like ON public.catalog_catalog USING btree (slug varchar_pattern_ops);


--
-- Name: catalog_catalog_title_af2f356b; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_title_af2f356b ON public.catalog_catalog USING btree (title);


--
-- Name: catalog_catalog_title_af2f356b_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_title_af2f356b_like ON public.catalog_catalog USING btree (title varchar_pattern_ops);


--
-- Name: catalog_catalog_tree_id_6c61173c; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalog_tree_id_6c61173c ON public.catalog_catalog USING btree (tree_id);


--
-- Name: catalog_catalogimage_catalog_id_8c434043; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX catalog_catalogimage_catalog_id_8c434043 ON public.catalog_catalogimage USING btree (catalog_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: django_site_domain_a2e37b91_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX django_site_domain_a2e37b91_like ON public.django_site USING btree (domain varchar_pattern_ops);


--
-- Name: gallery_gallery_author_id_0e1b2c34; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_author_id_0e1b2c34 ON public.gallery_gallery USING btree (author_id);


--
-- Name: gallery_gallery_level_aaaf9a3d; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_level_aaaf9a3d ON public.gallery_gallery USING btree (level);


--
-- Name: gallery_gallery_lft_d9a9ff1a; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_lft_d9a9ff1a ON public.gallery_gallery USING btree (lft);


--
-- Name: gallery_gallery_parent_id_641f5ece; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_parent_id_641f5ece ON public.gallery_gallery USING btree (parent_id);


--
-- Name: gallery_gallery_rght_e74ab111; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_rght_e74ab111 ON public.gallery_gallery USING btree (rght);


--
-- Name: gallery_gallery_slug_7f00eb51; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_slug_7f00eb51 ON public.gallery_gallery USING btree (slug);


--
-- Name: gallery_gallery_slug_7f00eb51_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_slug_7f00eb51_like ON public.gallery_gallery USING btree (slug varchar_pattern_ops);


--
-- Name: gallery_gallery_title_7efb69e4; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_title_7efb69e4 ON public.gallery_gallery USING btree (title);


--
-- Name: gallery_gallery_title_7efb69e4_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_title_7efb69e4_like ON public.gallery_gallery USING btree (title varchar_pattern_ops);


--
-- Name: gallery_gallery_tree_id_5cfb0bb2; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_gallery_tree_id_5cfb0bb2 ON public.gallery_gallery USING btree (tree_id);


--
-- Name: gallery_galleryimage_gallery_id_ad9f071e; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX gallery_galleryimage_gallery_id_ad9f071e ON public.gallery_galleryimage USING btree (gallery_id);


--
-- Name: home_home_blog_id_09dbd01d; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX home_home_blog_id_09dbd01d ON public.home_home USING btree (blog_id);


--
-- Name: home_home_slug_3c37aa8c; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX home_home_slug_3c37aa8c ON public.home_home USING btree (slug);


--
-- Name: home_home_slug_3c37aa8c_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX home_home_slug_3c37aa8c_like ON public.home_home USING btree (slug varchar_pattern_ops);


--
-- Name: home_home_title_9a3901d3_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX home_home_title_9a3901d3_like ON public.home_home USING btree (title varchar_pattern_ops);


--
-- Name: home_homeimage_home_id_930c6893; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX home_homeimage_home_id_930c6893 ON public.home_homeimage USING btree (home_id);


--
-- Name: menu_mainmenu_blog_id_392bfbf5; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_blog_id_392bfbf5 ON public.menu_mainmenu USING btree (blog_id);


--
-- Name: menu_mainmenu_catalog_id_68f3bf4e; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_catalog_id_68f3bf4e ON public.menu_mainmenu USING btree (catalog_id);


--
-- Name: menu_mainmenu_gallery_id_3c0f0648; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_gallery_id_3c0f0648 ON public.menu_mainmenu USING btree (gallery_id);


--
-- Name: menu_mainmenu_level_9d46a0e7; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_level_9d46a0e7 ON public.menu_mainmenu USING btree (level);


--
-- Name: menu_mainmenu_lft_55f0837f; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_lft_55f0837f ON public.menu_mainmenu USING btree (lft);


--
-- Name: menu_mainmenu_name_89d3c302; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_name_89d3c302 ON public.menu_mainmenu USING btree (name);


--
-- Name: menu_mainmenu_name_89d3c302_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_name_89d3c302_like ON public.menu_mainmenu USING btree (name varchar_pattern_ops);


--
-- Name: menu_mainmenu_page_id_802be0b8; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_page_id_802be0b8 ON public.menu_mainmenu USING btree (page_id);


--
-- Name: menu_mainmenu_parent_id_195402d7; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_parent_id_195402d7 ON public.menu_mainmenu USING btree (parent_id);


--
-- Name: menu_mainmenu_rght_fde3f865; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_rght_fde3f865 ON public.menu_mainmenu USING btree (rght);


--
-- Name: menu_mainmenu_tree_id_2a8d96f2; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX menu_mainmenu_tree_id_2a8d96f2 ON public.menu_mainmenu USING btree (tree_id);


--
-- Name: order_order_status_id_ec745f82; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX order_order_status_id_ec745f82 ON public.order_order USING btree (status_id);


--
-- Name: order_order_user_id_7cf9bc2b; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX order_order_user_id_7cf9bc2b ON public.order_order USING btree (user_id);


--
-- Name: order_orderitem_order_id_aba34f44; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX order_orderitem_order_id_aba34f44 ON public.order_orderitem USING btree (order_id);


--
-- Name: order_orderitem_product_item_id_0c1cac4f; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX order_orderitem_product_item_id_0c1cac4f ON public.order_orderitem USING btree (product_item_id);


--
-- Name: order_status_name_ff6ff8be_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX order_status_name_ff6ff8be_like ON public.order_status USING btree (name varchar_pattern_ops);


--
-- Name: order_story_order_id_f0d84c4a; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX order_story_order_id_f0d84c4a ON public.order_story USING btree (order_id);


--
-- Name: order_story_status_id_72564f64; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX order_story_status_id_72564f64 ON public.order_story USING btree (status_id);


--
-- Name: page_page_author_id_d62cc760; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_page_author_id_d62cc760 ON public.page_page USING btree (author_id);


--
-- Name: page_page_slug_d6b7c8ed; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_page_slug_d6b7c8ed ON public.page_page USING btree (slug);


--
-- Name: page_page_slug_d6b7c8ed_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_page_slug_d6b7c8ed_like ON public.page_page USING btree (slug varchar_pattern_ops);


--
-- Name: page_page_title_f3e79603; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_page_title_f3e79603 ON public.page_page USING btree (title);


--
-- Name: page_page_title_f3e79603_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_page_title_f3e79603_like ON public.page_page USING btree (title varchar_pattern_ops);


--
-- Name: page_pagecomment_page_id_070504d5; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_pagecomment_page_id_070504d5 ON public.page_pagecomment USING btree (page_id);


--
-- Name: page_pagecomment_user_id_9a4af13a; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_pagecomment_user_id_9a4af13a ON public.page_pagecomment USING btree (user_id);


--
-- Name: page_pageimage_page_id_26df7250; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX page_pageimage_page_id_26df7250 ON public.page_pageimage USING btree (page_id);


--
-- Name: product_product_articul_d058d835_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_articul_d058d835_like ON public.product_product USING btree (articul varchar_pattern_ops);


--
-- Name: product_product_author_id_9becb979; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_author_id_9becb979 ON public.product_product USING btree (author_id);


--
-- Name: product_product_catalog_catalog_id_28182fb8; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_catalog_catalog_id_28182fb8 ON public.product_product_catalog USING btree (catalog_id);


--
-- Name: product_product_catalog_product_id_2bb2e50d; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_catalog_product_id_2bb2e50d ON public.product_product_catalog USING btree (product_id);


--
-- Name: product_product_recommend_products_from_product_id_f4fd92d7; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_recommend_products_from_product_id_f4fd92d7 ON public.product_product_recommend_products USING btree (from_product_id);


--
-- Name: product_product_recommend_products_to_product_id_38eb3ce3; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_recommend_products_to_product_id_38eb3ce3 ON public.product_product_recommend_products USING btree (to_product_id);


--
-- Name: product_product_slug_76cde0ae; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_slug_76cde0ae ON public.product_product USING btree (slug);


--
-- Name: product_product_slug_76cde0ae_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_slug_76cde0ae_like ON public.product_product USING btree (slug varchar_pattern_ops);


--
-- Name: product_product_title_0434d32d; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_title_0434d32d ON public.product_product USING btree (title);


--
-- Name: product_product_title_0434d32d_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_product_title_0434d32d_like ON public.product_product USING btree (title varchar_pattern_ops);


--
-- Name: product_productcomment_product_id_bf5e6965; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_productcomment_product_id_bf5e6965 ON public.product_productcomment USING btree (product_id);


--
-- Name: product_productcomment_user_id_e1a22ad0; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_productcomment_user_id_e1a22ad0 ON public.product_productcomment USING btree (user_id);


--
-- Name: product_productimage_product_id_544084bb; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_productimage_product_id_544084bb ON public.product_productimage USING btree (product_id);


--
-- Name: product_productitem_articul_b01fcac5_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_productitem_articul_b01fcac5_like ON public.product_productitem USING btree (articul varchar_pattern_ops);


--
-- Name: product_productitem_product_id_16cb97c8; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX product_productitem_product_id_16cb97c8 ON public.product_productitem USING btree (product_id);


--
-- Name: settings_template_footer_blogs_blog_id_1531bf6f; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_blogs_blog_id_1531bf6f ON public.settings_template_footer_blogs USING btree (blog_id);


--
-- Name: settings_template_footer_blogs_footer_id_defb5a64; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_blogs_footer_id_defb5a64 ON public.settings_template_footer_blogs USING btree (footer_id);


--
-- Name: settings_template_footer_catalog_catalog_id_a6917c88; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_catalog_catalog_id_a6917c88 ON public.settings_template_footer_catalog USING btree (catalog_id);


--
-- Name: settings_template_footer_catalog_footer_id_129c20b7; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_catalog_footer_id_129c20b7 ON public.settings_template_footer_catalog USING btree (footer_id);


--
-- Name: settings_template_footer_galleries_footer_id_f9131e21; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_galleries_footer_id_f9131e21 ON public.settings_template_footer_galleries USING btree (footer_id);


--
-- Name: settings_template_footer_galleries_gallery_id_79e8c708; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_galleries_gallery_id_79e8c708 ON public.settings_template_footer_galleries USING btree (gallery_id);


--
-- Name: settings_template_footer_list_link_footer_id_79de261f; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_list_link_footer_id_79de261f ON public.settings_template_footer_list_link USING btree (footer_id);


--
-- Name: settings_template_footer_list_link_listlink_id_9240aede; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_list_link_listlink_id_9240aede ON public.settings_template_footer_list_link USING btree (listlink_id);


--
-- Name: settings_template_footer_page_footer_id_2e9b2f7f; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_page_footer_id_2e9b2f7f ON public.settings_template_footer_page USING btree (footer_id);


--
-- Name: settings_template_footer_page_page_id_75c91e8d; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_page_page_id_75c91e8d ON public.settings_template_footer_page USING btree (page_id);


--
-- Name: settings_template_footer_sort_e354a2b4; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_sort_e354a2b4 ON public.settings_template_footer USING btree (sort);


--
-- Name: settings_template_footer_text_info_id_d14ebd5e; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_text_info_id_d14ebd5e ON public.settings_template_footer USING btree (text_info_id);


--
-- Name: settings_template_footer_title_7e9adf32_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_footer_title_7e9adf32_like ON public.settings_template_footer USING btree (title varchar_pattern_ops);


--
-- Name: settings_template_settingstemplate_footer_id_5ddf2bbc; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_settingstemplate_footer_id_5ddf2bbc ON public.settings_template_settingstemplate USING btree (footer_id);


--
-- Name: settings_template_settingstemplate_home_id_661e3df9; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_settingstemplate_home_id_661e3df9 ON public.settings_template_settingstemplate USING btree (home_id);


--
-- Name: settings_template_settingstemplate_title_26e31781_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX settings_template_settingstemplate_title_26e31781_like ON public.settings_template_settingstemplate USING btree (title varchar_pattern_ops);


--
-- Name: site_info_listlink_sort_a165f28a; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX site_info_listlink_sort_a165f28a ON public.site_info_listlink USING btree (sort);


--
-- Name: site_info_listlink_title_0c1ab567; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX site_info_listlink_title_0c1ab567 ON public.site_info_listlink USING btree (title);


--
-- Name: site_info_listlink_title_0c1ab567_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX site_info_listlink_title_0c1ab567_like ON public.site_info_listlink USING btree (title varchar_pattern_ops);


--
-- Name: site_info_listlink_url_63ed37f5; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX site_info_listlink_url_63ed37f5 ON public.site_info_listlink USING btree (url);


--
-- Name: site_info_listlink_url_63ed37f5_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX site_info_listlink_url_63ed37f5_like ON public.site_info_listlink USING btree (url varchar_pattern_ops);


--
-- Name: site_info_socialnetwork_html_link_a63e625b_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX site_info_socialnetwork_html_link_a63e625b_like ON public.site_info_socialnetwork USING btree (html_link varchar_pattern_ops);


--
-- Name: site_info_textinfo_title_8a7ec2d6_like; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX site_info_textinfo_title_8a7ec2d6_like ON public.site_info_textinfo USING btree (title varchar_pattern_ops);


--
-- Name: users_userlink_user_id_5730a6f4; Type: INDEX; Schema: public; Owner: dervek
--

CREATE INDEX users_userlink_user_id_5730a6f4 ON public.users_userlink USING btree (user_id);


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blog_author_id_8791af69_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_author_id_8791af69_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blog_parent_id_2195dcd3_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blog
    ADD CONSTRAINT blog_blog_parent_id_2195dcd3_fk_blog_blog_id FOREIGN KEY (parent_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_blogimage_blog_id_17a42bec_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_blogimage
    ADD CONSTRAINT blog_blogimage_blog_id_17a42bec_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_comment_post_id_580e96ef_fk_blog_post_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_comment
    ADD CONSTRAINT blog_comment_post_id_580e96ef_fk_blog_post_id FOREIGN KEY (post_id) REFERENCES public.blog_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_comment_user_id_59a54155_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_comment
    ADD CONSTRAINT blog_comment_user_id_59a54155_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_post_author_id_dd7a8485_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_post
    ADD CONSTRAINT blog_post_author_id_dd7a8485_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_post_blog_id_1d63f737_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_post
    ADD CONSTRAINT blog_post_blog_id_1d63f737_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: blog_postimage_post_id_09cc1915_fk_blog_post_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.blog_postimage
    ADD CONSTRAINT blog_postimage_post_id_09cc1915_fk_blog_post_id FOREIGN KEY (post_id) REFERENCES public.blog_post(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalog_author_id_73764c6e_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_author_id_73764c6e_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalog_parent_id_fbb74747_fk_catalog_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalog
    ADD CONSTRAINT catalog_catalog_parent_id_fbb74747_fk_catalog_catalog_id FOREIGN KEY (parent_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: catalog_catalogimage_catalog_id_8c434043_fk_catalog_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.catalog_catalogimage
    ADD CONSTRAINT catalog_catalogimage_catalog_id_8c434043_fk_catalog_catalog_id FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_gallery_author_id_0e1b2c34_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_author_id_0e1b2c34_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_gallery_parent_id_641f5ece_fk_gallery_gallery_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_gallery
    ADD CONSTRAINT gallery_gallery_parent_id_641f5ece_fk_gallery_gallery_id FOREIGN KEY (parent_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: gallery_galleryimage_gallery_id_ad9f071e_fk_gallery_gallery_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.gallery_galleryimage
    ADD CONSTRAINT gallery_galleryimage_gallery_id_ad9f071e_fk_gallery_gallery_id FOREIGN KEY (gallery_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_home_blog_id_09dbd01d_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.home_home
    ADD CONSTRAINT home_home_blog_id_09dbd01d_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: home_homeimage_home_id_930c6893_fk_home_home_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.home_homeimage
    ADD CONSTRAINT home_homeimage_home_id_930c6893_fk_home_home_id FOREIGN KEY (home_id) REFERENCES public.home_home(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu_blog_id_392bfbf5_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_blog_id_392bfbf5_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu_catalog_id_68f3bf4e_fk_catalog_catalog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_catalog_id_68f3bf4e_fk_catalog_catalog_id FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu_gallery_id_3c0f0648_fk_gallery_gallery_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_gallery_id_3c0f0648_fk_gallery_gallery_id FOREIGN KEY (gallery_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu_page_id_802be0b8_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_page_id_802be0b8_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: menu_mainmenu_parent_id_195402d7_fk_menu_mainmenu_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.menu_mainmenu
    ADD CONSTRAINT menu_mainmenu_parent_id_195402d7_fk_menu_mainmenu_id FOREIGN KEY (parent_id) REFERENCES public.menu_mainmenu(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_order_status_id_ec745f82_fk_order_status_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_status_id_ec745f82_fk_order_status_id FOREIGN KEY (status_id) REFERENCES public.order_status(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_order_user_id_7cf9bc2b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_order
    ADD CONSTRAINT order_order_user_id_7cf9bc2b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_orderitem_order_id_aba34f44_fk_order_order_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_orderitem
    ADD CONSTRAINT order_orderitem_order_id_aba34f44_fk_order_order_id FOREIGN KEY (order_id) REFERENCES public.order_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_orderitem_product_item_id_0c1cac4f_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_orderitem
    ADD CONSTRAINT order_orderitem_product_item_id_0c1cac4f_fk_product_p FOREIGN KEY (product_item_id) REFERENCES public.product_productitem(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_story_order_id_f0d84c4a_fk_order_order_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_order_id_f0d84c4a_fk_order_order_id FOREIGN KEY (order_id) REFERENCES public.order_order(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: order_story_status_id_72564f64_fk_order_status_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.order_story
    ADD CONSTRAINT order_story_status_id_72564f64_fk_order_status_id FOREIGN KEY (status_id) REFERENCES public.order_status(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_page_author_id_d62cc760_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_page
    ADD CONSTRAINT page_page_author_id_d62cc760_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_pagecomment_page_id_070504d5_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_pagecomment
    ADD CONSTRAINT page_pagecomment_page_id_070504d5_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_pagecomment_user_id_9a4af13a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_pagecomment
    ADD CONSTRAINT page_pagecomment_user_id_9a4af13a_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: page_pageimage_page_id_26df7250_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.page_pageimage
    ADD CONSTRAINT page_pageimage_page_id_26df7250_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_author_id_9becb979_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product
    ADD CONSTRAINT product_product_author_id_9becb979_fk_auth_user_id FOREIGN KEY (author_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_cata_catalog_id_28182fb8_fk_catalog_c; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_cata_catalog_id_28182fb8_fk_catalog_c FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_cata_product_id_2bb2e50d_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_catalog
    ADD CONSTRAINT product_product_cata_product_id_2bb2e50d_fk_product_p FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_reco_from_product_id_f4fd92d7_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_reco_from_product_id_f4fd92d7_fk_product_p FOREIGN KEY (from_product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_product_reco_to_product_id_38eb3ce3_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_product_recommend_products
    ADD CONSTRAINT product_product_reco_to_product_id_38eb3ce3_fk_product_p FOREIGN KEY (to_product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productcomme_product_id_bf5e6965_fk_product_p; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productcomment
    ADD CONSTRAINT product_productcomme_product_id_bf5e6965_fk_product_p FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productcomment_user_id_e1a22ad0_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productcomment
    ADD CONSTRAINT product_productcomment_user_id_e1a22ad0_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productimage_product_id_544084bb_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productimage
    ADD CONSTRAINT product_productimage_product_id_544084bb_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: product_productitem_product_id_16cb97c8_fk_product_product_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.product_productitem
    ADD CONSTRAINT product_productitem_product_id_16cb97c8_fk_product_product_id FOREIGN KEY (product_id) REFERENCES public.product_product(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_catalog_id_a6917c88_fk_catalog_c; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_fo_catalog_id_a6917c88_fk_catalog_c FOREIGN KEY (catalog_id) REFERENCES public.catalog_catalog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_footer_id_129c20b7_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_catalog
    ADD CONSTRAINT settings_template_fo_footer_id_129c20b7_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_footer_id_2e9b2f7f_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_fo_footer_id_2e9b2f7f_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_footer_id_79de261f_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_fo_footer_id_79de261f_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_footer_id_defb5a64_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_fo_footer_id_defb5a64_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_footer_id_f9131e21_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_fo_footer_id_f9131e21_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_gallery_id_79e8c708_fk_gallery_g; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_galleries
    ADD CONSTRAINT settings_template_fo_gallery_id_79e8c708_fk_gallery_g FOREIGN KEY (gallery_id) REFERENCES public.gallery_gallery(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_listlink_id_9240aede_fk_site_info; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_list_link
    ADD CONSTRAINT settings_template_fo_listlink_id_9240aede_fk_site_info FOREIGN KEY (listlink_id) REFERENCES public.site_info_listlink(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_fo_text_info_id_d14ebd5e_fk_site_info; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer
    ADD CONSTRAINT settings_template_fo_text_info_id_d14ebd5e_fk_site_info FOREIGN KEY (text_info_id) REFERENCES public.site_info_textinfo(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_blogs_blog_id_1531bf6f_fk_blog_blog_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_blogs
    ADD CONSTRAINT settings_template_footer_blogs_blog_id_1531bf6f_fk_blog_blog_id FOREIGN KEY (blog_id) REFERENCES public.blog_blog(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_footer_page_page_id_75c91e8d_fk_page_page_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_footer_page
    ADD CONSTRAINT settings_template_footer_page_page_id_75c91e8d_fk_page_page_id FOREIGN KEY (page_id) REFERENCES public.page_page(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_se_footer_id_5ddf2bbc_fk_settings_; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_se_footer_id_5ddf2bbc_fk_settings_ FOREIGN KEY (footer_id) REFERENCES public.settings_template_footer(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_se_home_id_661e3df9_fk_home_home; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_se_home_id_661e3df9_fk_home_home FOREIGN KEY (home_id) REFERENCES public.home_home(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: settings_template_se_site_id_a21c4a42_fk_django_si; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.settings_template_settingstemplate
    ADD CONSTRAINT settings_template_se_site_id_a21c4a42_fk_django_si FOREIGN KEY (site_id) REFERENCES public.django_site(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_userlink_user_id_5730a6f4_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.users_userlink
    ADD CONSTRAINT users_userlink_user_id_5730a6f4_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: users_userprofile_user_id_87251ef1_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: dervek
--

ALTER TABLE ONLY public.users_userprofile
    ADD CONSTRAINT users_userprofile_user_id_87251ef1_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

