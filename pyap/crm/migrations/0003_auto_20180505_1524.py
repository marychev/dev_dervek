# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-05-05 15:24
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('crm', '0002_auto_20180505_1456'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProjectStep',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=255, null=True, verbose_name='Название*')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Описание (информация)')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='projectitem',
            options={'ordering': ('-created',), 'verbose_name': 'Пункт проекта', 'verbose_name_plural': 'Пункты проектов'},
        ),
        migrations.AlterField(
            model_name='projectitem',
            name='author',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Автор'),
        ),
        migrations.AddField(
            model_name='project',
            name='step',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.ProjectStep', verbose_name='Этапы'),
        ),
    ]
