# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-05-05 14:56
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import filebrowser.fields


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('crm', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=255, null=True, verbose_name='Название*')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Описание (информация)')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обнавлен')),
                ('start_at', models.DateField(blank=True, null=True, verbose_name='Дата приемки')),
                ('end_at', models.DateField(blank=True, null=True, verbose_name='Дата сдачи')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Замерщик')),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='client', to=settings.AUTH_USER_MODEL, verbose_name='Клиент')),
            ],
            options={
                'verbose_name': 'Проект',
                'verbose_name_plural': 'Проекты',
                'ordering': ('-created', 'end_at'),
            },
        ),
        migrations.CreateModel(
            name='ProjectItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(db_index=True, max_length=255, null=True, verbose_name='Название*')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Описание (информация)')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Создан')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Обнавлен')),
                ('file', filebrowser.fields.FileBrowseField(blank=True, max_length=255, null=True, verbose_name='Файл')),
                ('author', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Замерщик')),
                ('project', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.Project', verbose_name='Проект')),
            ],
            options={
                'verbose_name': 'Пункт проекта',
                'verbose_name_plural': 'Пункты проетов',
                'ordering': ('-created',),
            },
        ),
        migrations.AlterModelOptions(
            name='sample',
            options={'ordering': ('-created',), 'verbose_name': 'Замер', 'verbose_name_plural': 'Замеры'},
        ),
        migrations.RemoveField(
            model_name='sample',
            name='title',
        ),
        migrations.AddField(
            model_name='sample',
            name='project',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='crm.Project', verbose_name='Проект'),
        ),
    ]
