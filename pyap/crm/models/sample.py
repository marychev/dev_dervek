from django.db import models
from django.conf import settings
from filebrowser.fields import FileBrowseField
from ckeditor_uploader.fields import RichTextUploadingField
from utils.abstract_model import CreatedUpdatedModel
from .project import Project


class Sample(CreatedUpdatedModel):
    """
    Модель для хранения замеров.
    """
    project = models.ForeignKey(Project, verbose_name='Проект', blank=True, null=True)
    html = RichTextUploadingField(verbose_name='Данные замера', blank=True, null=True)
    file = FileBrowseField('Файл', max_length=255, directory="crm/", blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Замерщик', null=True, blank=True)

    def __str__(self):
        return str(self.project)
    
    class Meta:
        verbose_name = 'Замер'
        verbose_name_plural = 'Замеры'
        ordering = ('-created',)
