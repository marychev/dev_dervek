from utils.abstract_model import ABSTitleModel


class ProjectStep(ABSTitleModel):
    """
    Этапы которые проходит проект.
    """
    
    class Meta:
        verbose_name = 'Этап'
        verbose_name_plural = 'Этапы'
        ordering = ('title',)
