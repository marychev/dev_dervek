from django.db import models
from django.conf import settings
from filebrowser.fields import FileBrowseField
from utils.abstract_model import ABSTitleModel, CreatedUpdatedModel
from .project import Project


class ProjectItem(ABSTitleModel, CreatedUpdatedModel):
    """
    Модель для пунктов проекта.
    """
    project = models.ForeignKey(Project, verbose_name='Проект', blank=True, null=True)
    file = FileBrowseField('Файл', max_length=255, directory="crm/", blank=True, null=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Автор', null=True, blank=True)

    class Meta:
        verbose_name = 'Пункт проекта'
        verbose_name_plural = 'Пункты проектов'
        ordering = ('-created',)
