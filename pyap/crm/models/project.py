from django.db import models
from django.conf import settings
from utils.abstract_model import ABSTitleModel, CreatedUpdatedModel
from ..models import ProjectStep


class Project(ABSTitleModel, CreatedUpdatedModel):
    """
    Модель для формирование проекта.
    """
    step = models.ForeignKey(ProjectStep, verbose_name='Этапы', blank=True, null=True)
    client = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name='Клиент', related_name='client', null=True, blank=True)
    author = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Замерщик', null=True, blank=True)
    start_at = models.DateField(verbose_name='Дата приемки', blank=True, null=True)
    end_at = models.DateField(verbose_name='Дата сдачи', blank=True, null=True)

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'
        ordering = ('-created', 'end_at')
