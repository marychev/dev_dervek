from django.contrib import admin
from daterange_filter.filter import DateRangeFilter
from utils.abstract_admin import DefaultSettings, AbstractBaseInline
from ..models import Sample, ProjectItem, Project


# INLINE ---

class SampleInline(AbstractBaseInline):
    raw_id_fields = ('author',)
    model = Sample


class ProjectItemInline(AbstractBaseInline):
    raw_id_fields = ('author',)
    model = ProjectItem

# --------------------------------------------


@admin.register(Project)
class ProjectAdmin(DefaultSettings):
    menu_group = "CRM"
    menu_title = "Проекты"
    inlines = (SampleInline, ProjectItemInline)
    readonly_fields = ('created', 'updated')
    raw_id_fields = ('client', 'author',)
    search_fields = ('title', 'client__email')
    actions = DefaultSettings.abs_actions
    list_filter = (
        'step',
        ('created', DateRangeFilter), ('updated', DateRangeFilter),
        ('start_at', DateRangeFilter), ('end_at', DateRangeFilter)
    )
    list_display = ('title', 'client', 'step', 'end_at')
    list_display_links = ('title', 'client')
