from django.contrib import admin
from daterange_filter.filter import DateRangeFilter
from utils.abstract_admin import DefaultSettings
from ..models import ProjectItem


@admin.register(ProjectItem)
class ProjectItemAdmin(DefaultSettings):
    menu_group = "CRM"
    menu_title = "Пункты проектов"
    readonly_fields = ('created', 'updated')
    raw_id_fields = ('project', 'author')
    search_fields = ('title', 'project__title',)
    actions = DefaultSettings.abs_actions
    list_filter = (
        ('created', DateRangeFilter), ('updated', DateRangeFilter)
    )
    list_display = ('project', 'title', 'file', 'author', 'created')
    list_display_links = ('project', 'title')
