from django.contrib import admin
from utils.abstract_admin import DefaultSettings
from ..models import ProjectStep


@admin.register(ProjectStep)
class ProjectStepAdmin(DefaultSettings):
    menu_group = "CRM"
    menu_title = "Этапы"
    search_fields = ('title',)
    list_display = ('title', 'description')
