from django.contrib import admin
from daterange_filter.filter import DateRangeFilter
from utils.abstract_admin import DefaultSettings
from ..models import Sample


@admin.register(Sample)
class SampleAdmin(DefaultSettings):
    menu_group = "CRM"
    menu_title = "Замеры"
    readonly_fields = ('created', 'updated')
    raw_id_fields = ('project', 'author',)
    search_fields = ('project__title',)
    actions = DefaultSettings.abs_actions
    list_filter = (
        ('created', DateRangeFilter), ('updated', DateRangeFilter)
    )
    list_display = ('project', 'file', 'get_html', 'author', 'created')
    list_display_links = ('project', 'file')
