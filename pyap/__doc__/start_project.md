#  Развертывание проекта 


`dervek.ru` - название для проета для примера


Скачать или клонировать себе проект
Подготовка папок для проекта
    
    cd ~/projects
    mkdir dervek.ru
    cd dervek.ru
    
проверяем `ls`. Должны быть следующие директории `pyap theme`


Ставим виртуальное окружение. Активируем.

	virtualenv --python=python3 venv
	source venv/bin/activate


Устанавливаем Джанго и все необходимые зависимости. Входим в папку где лежит файл `manage.py`.
	
	 cd ~/projects/dervek.ru/pyap/
	 pip install -r requirements.txt


По умолчанию установливается база `SQLite` `dervek` в проект 
	
	python manage.py collectstatic --noinput
	python manage.py makemigrations
	python manage.py migrate

Проект создан! Проверяем `python manage.py runserver`. 
Запускаем локальный сервер и переходим на страницу в `http://localhost:8000/`. 
	

[I]Заполним проект дефолтными данными

	python manage.py createsuperuser
 		name: `superuser`
 		emal: `superuser@mail.ru`
 		password: `pyap_81086`

 	# [!]Эти команды применить раза 2-3. Пока не прекратятся ошибки
  	python manage.py loaddata users/fixtures/init.json
  	python manage.py loaddata home/fixtures/init.json
    python manage.py loaddata site_info/fixtures/init.json
    python manage.py loaddata settings_template/fixtures/init.json
    python manage.py loaddata include_area/fixtures/init.json
    python manage.py loaddata advertising/fixtures/init.json
    python manage.py loaddata gallery/fixtures/init.json
    python manage.py loaddata catalog/fixtures/init.json
    python manage.py loaddata product/fixtures/init.json
    python manage.py loaddata page/fixtures/init.json
    python manage.py loaddata blog/fixtures/init.json
    
    python manage.py loaddata menu/fixtures/init.json
    python manage.py loaddata order/fixtures/init.json


Проверяем `python manage.py runserver`.


Создадим базу данных `dervek` и настроем `postgres` для проекта

	sudo apt-get update
	sudo apt-get install python3-pip python3-dev libpq-dev postgresql postgresql-contrib 
	
	# Если нужен `postgis`
	# sudo apt-get install postgis gdal-bin	

	sudo -u postgres psql
	postgres=# 
		CREATE DATABASE dervek;
		CREATE USER dervek WITH PASSWORD 'pyap_81086';
		ALTER ROLE dervek SET client_encoding TO 'utf8';
		ALTER ROLE dervek SET default_transaction_isolation TO 'read committed';
		ALTER ROLE dervek SET timezone TO 'UTC';
		GRANT ALL PRIVILEGES ON DATABASE dervek TO dervek;
		\q


Меняем настройку подключения к Базе в `~/projects/dervek.ru/pyap/pyap/settings.py`
```
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'dervek',
        'USER': 'dervek',
        'PASSWORD': 'pyap_81086',
        'HOST': 'localhost',
        'PORT': '',
    }
}
...
```
	
	python manage.py migrate


[I]Заполним проект дефолтными данными
	
	# ... создаем суперпользователя и приминяем фикстуры ...


Проверяем `python manage.py runserver`.

# Если сервер


## GIT / BITBACKET 
	
	cd ~/projects/dervek.ru

	git config --global user.name "Mihail"
	git config --global user.email my@mail.com
	git config --global core.pager 'less -r'

	git init
	git add *
	git commit -m 'Hello World!'
	
	# --settings
	git remote add origin git@bitbucket.org:marychev/dervek.git
	git push -u origin master


#### TRY/EXCEPT 

1.`Error: That port is already in use.`
	
	fuser -k 8000/tcp 


# ----      ---- #
  --- [pYAp] --- 
# ----      ---- #



