# -*- coding: utf-8 -*-
from transliterate import translit


def translaton_field(name):
    """Вернуть перевод передаваемого значения """
    return translit(''.join(name.replace(' ', '-')
                            .replace(u'/', '-').replace(u'№', '').replace(u'.', '')
                            .replace(u'(', '').replace(u')', '').replace(u'ь', '')
                            ), 'ru', reversed=True).lower()

