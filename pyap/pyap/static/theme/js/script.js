"use strict";

jQuery(window).scroll(function() {


	var wpadminbar =  jQuery('#wpadminbar').outerHeight();
	var topBarHeight = jQuery('#top-bar').outerHeight();
	var headerV2Height = jQuery('.header-v2').outerHeight();
	var headerContainer = (jQuery('.header-container'));
	var headerHeight = headerContainer.outerHeight();
	if (jQuery(this).scrollTop() > headerHeight){
		jQuery('.sticky-tr').removeClass("transparent-header").css('top', -topBarHeight - headerV2Height);
		jQuery('.sticky').css('top', -topBarHeight - headerV2Height + wpadminbar);

	}
	else{
		jQuery('.sticky-tr').addClass("transparent-header").css('top', 0);
		jQuery('.sticky').css('top', 0);
	}
});

jQuery(document).ready(function($) {
	$( ".icon-menu" ).click(function() {
		$('.primary-menu').toggleClass('active');
		$('body').toggleClass('hidden');
	});

	$('.primary-menu a').click(function() {
		$('.primary-menu').removeClass('active');
	});

	var dateElement = $('.datepicker');
	dateElement.datepicker({
		minDate: 0,
		dateFormat: dateElement.attr('format'),
	});
	$('.datepicker').datepicker( "setDate", "getDate" );
	$('.tabs').tabs({show: 'fade', hide: 'fade'});

	// RWD Navigation
	function initMainNavigation( container ) {
		// Add dropdown toggle that display child menu items.
		container.find( '.menu-item-has-children > a' ).after( '<button class="dropdown-toggle">'  + '</button>' );

		// Toggle buttons and submenu items with active children menu items.
		container.find( '.current-menu-ancestor > button' ).addClass( 'toggle-on' );
		container.find( '.current-menu-ancestor > .sub-menu' ).addClass( 'toggled-on' );

		container.find( '.dropdown-toggle' ).click( function( e ) {
			var _this = $( this );
			e.preventDefault();
			_this.toggleClass( 'toggle-on' );
			_this.next( '.children, .sub-menu' ).toggleClass( 'toggled-on' );
		} );
	}
	initMainNavigation( $( '#nav' ) );



	//Magnific Popup
	$('.dervek-product-gallery__wrapper').magnificPopup({
		delegate: 'a',
		type: 'image',
	});
	$('.gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery:{
			enabled:true
		}
	});

    //Swiper Product Categories
    var swiper = new Swiper('#categories-wrapper',{
	    cleanupStyles: true,
	    slidesPerView: 4,
	    spaceBetween: 30,
	    pagination: '.swiper-pagination',
        paginationClickable: true,
		nextButton: '.swiper-cat-next',
		prevButton: '.swiper-cat-prev',
  		slideClass: 'item',
		breakpoints: {
            1024: {
                slidesPerView: 4,
                spaceBetween: 30
            },
            768: {
                slidesPerView: 3,
                spaceBetween: 30
            },
            600: {
                slidesPerView: 2,
                spaceBetween: 15
            },
            270: {
                slidesPerView: 1,
                spaceBetween: 1
            }
        }
    });

    var swiper = new Swiper('#testimonials-slider .swiper-container',{
		nextButton: '.next',
		prevButton: '.prev',
        paginationClickable: true,
        cleanupStyles: true,
        loop: true,
    });

	//Latest News Slider
    var swiper = new Swiper('#latest-news .swiper-container',{
		pagination: '.swiper-pagination',
		nextButton: '#latest-news .slider-button-next',
		prevButton: '#latest-news .slider-button-prev',
        paginationClickable: true,
        cleanupStyles: true,
        autoHeight: true,
    });

	//Swiper Promo Slider
    var swiper = new Swiper('#promo-slider',{
		pagination: '.swiper-pagination',
		nextButton: '.slider-button-next',
		prevButton: '.slider-button-prev',
        paginationClickable: true,
        cleanupStyles: true,
        autoplay: 6000,
    });

    var swiper = new Swiper('#page-slider',{
        cleanupStyles: true,
        paginationClickable: true,
        autoplay: 6000,
        loop: true,
		pagination: '.swiper-pagination',
		nextButton: '.slider-button-next',
		prevButton: '.slider-button-prev',
    });

/* OPEN TABLE DATE PICKER
   ========================================================================== */
	var otDate = $('.otw-reservation-date');
	otDate.datepicker({
		minDate: 0,
		dateFormat: otDate.attr('format'),
		onSelect: function() {
			var secconds = Date.parse(otDate.val());
			var date = new Date(secconds);
			var yy = date.getFullYear();
			var mm = date.getMonth() + 1;
			var dd = date.getDate();
			$('.startDate').val( mm + '/' + dd + '/' + yy);
		}
	});
	otDate.datepicker( "setDate", "getDate" );


});
window.onload = function(){
	if(document.getElementById('slider')) {
		var slider = document.getElementById('slider').getElementsByClassName('item');
		if( slider.length != 0 ) {
			var sliderLength = slider.length;
			var count = 0;
			bg_switch();
			setInterval(bg_switch, 7000);
		}
	}
	function bg_switch(){
		slider[count].classList.add('active');
		slider[count].style.zIndex = 1;
		var scount = count;

		if(scount === 0) {
			slider[sliderLength - 1].style.removeProperty('z-index');
			setTimeout(function(){
				slider[sliderLength - 1].classList.remove('active');
			},3000);
		} else {
			slider[scount - 1].style.removeProperty('z-index');
			setTimeout(function(){
				slider[scount - 1].classList.remove('active');
			},3000);
		}
		(sliderLength === count + 1) ? count = 0 : count++;
	}
}