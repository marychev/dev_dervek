"use strict";

/**************************************************
 * Обработка нажатия кнопки "ДОБАВИТЬ В КОРЗИНУ" *
 * ************************************************/

// определить одиночный товар или с есть варианты товара
var is_product_item = false;
if ($('input[name="product_item"]').length > 1) {
    is_product_item = true;
}

// уставноить выбранный вариант товара
$('input[type=radio][name=product_item]').change(function() {
    $('#productId').val(this.value);
});


$('#addCartForm').on('submit', function(event){
    event.preventDefault();

    if ($('#productId').val() === ''){
        alert('Не выбран вариант товара');
        return false;}
    else {
        var data = {
            'product_id': $('#productId').val(),
            'quantity': $('#quantity').val(),
            'is_product_item': is_product_item}
        addCart(data);
    }
});

function addCart(data) {
    $.ajax({
        url : location.pathname,
        type : "POST",
        data: data,

        success : function(json) {
            // Оповестить о результате добавлении товара
            var div = createPopup(json);
            var link_del = document.createElement('a');
            link_del.innerText = 'x';
            link_del.className = 'del_popup';

            div.appendChild(link_del);
            $('#addCartForm').append(div);
            // преждевременное удаление
            $(link_del).click(function () {
                deleteElement($(div), 100);
            });
            deleteElement($(div));

            // Изменить общие данные корзины в шапке сайта
            var oldTotalCartQty = Number($('.jsTotalCartQty').text());
            var oldTotalCartPrice = parseFloat($('.jsTotalCartPrice').text());
            $('.jsTotalCartQty').text(oldTotalCartQty + json.quantity);
            $('.jsTotalCartPrice').text(oldTotalCartPrice + (json.price * json.quantity));

        },

        // handle a non-successful response
        error : function(xhr,errmsg,err) {
            $('#ajaxError').html("<div class='alert-danger text-center'>Oops! Произошла непредвиденная ошибка. <br>Просим сообщить нам о ней! <br>" + errmsg +
                " <a href='#'>&times;</a></div>");
            console.warn(xhr.status + ": " + xhr.responseText);
        }
    });
};


/**********************************************************************************
 * Создать элемент для оповещения
 *
 * @param data(json) - данные: ответа от сервера(!)
 * ********************************************************************************/
var createPopup = function (data) {
    var msg = '<b>ТОВАР ДОБАВЛЕН!</b><br>' +
        '<span>' + data.name + ' - </span><b>'+data.quantity+' шт.</b><br>' +
        '<small class="text-muted">' + data.articul + '</small>';
    var div = document.createElement('div');
    div.className = 'msg_add_cart';
    div.innerHTML = msg;
    return div;
};


/**********************************************************************************
 * Плавное удаление оповещения
 *
 * @param elem(object) - html элемент(!)
 * @param timeDelta(Number) - отрезок времени, через который элемент нужно удалить
 * ********************************************************************************/
var deleteElement = function (elem, timeDelta) {
    timeDelta = timeDelta || 5000;
    setTimeout(function () { elem.slideUp(); }, timeDelta);
    setTimeout(function () { elem.remove(); }, timeDelta + 1000);
};
