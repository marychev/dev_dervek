/***********************************************************
 *  Увеличение фотографии в отдельном окне, при наведении *
 * *********************************************************/
jQuery(function(){
    $(".jsZoomPhotoMain").imagezoomsl();

    $(".jsZoomPhotoThumb").click(function(){
        var that = this;
        //копируем атрибуты из превью-картинки в контейнер-картинку
        $(".jsZoomPhotoMain").fadeOut(300, function(){
            $(this).attr("src",     $(that).attr("data-large"))   // путь до small картинки
                .attr("data-large", $(that).attr("data-large"))  // путь до big картинки
                //дополнительные атрибуты, если есть
                //.attr("data-title",       $(that).attr("data-title"))       // заголовок подсказки
                //.attr("data-help",        $(that).attr("data-help"))        // текст подсказки
                //.attr("data-text-bottom", $(that).attr("data-text-bottom")) // текст снизу картинки
                .fadeIn(600);
        });
    });
});
