$(function () {

    var $menu = $("nav#my-menu");
    $menu.mmenu({
        // options
        position	: 'left',
        classes		: 'mm-black',
        counters	: false,
        searchfield	: true,
        header      : {
            add       : true,
            update    : true,
            title     :'Навигация'
        }
    }, {
        // configuration
    });
    var $icon = $(".hamburger");
    //var API = $menu.data("mmenu");

    $icon.on("click", function () {
        setTimeout(function () {
            $icon.addClass("is-active");
        }, 300);
    });

    $('#my-menu').on('closed.mm', function() {
        // do something
        $icon.removeClass("is-active");
    });



    // Home - Slider
    $('.slider').bxSlider({
        mode: 'horizontal',
        controls: true,
        auto: true,
        speed: 900,
        touchEnabled: true,
        oneToOneTouch: true
    });


    // // Shop - Menu - List
    // $('.catalog li.active').addClass('open').children('ul').show();
    // $('.catalog li.has-sub > span').on('click', function(){
    //     // $(this).removeAttr('href');
    //     var element = $(this).parent('li');
    //     if (element.hasClass('open')) {
    //         element.removeClass('open');
    //         element.find('li').removeClass('open');
    //         element.find('ul').slideUp(200);
    //     }
    //     else {
    //         element.addClass('open');
    //         element.children('ul').slideDown(200);
    //         element.siblings('li').children('ul').slideUp(200);
    //         element.siblings('li').removeClass('open');
    //         element.siblings('li').find('li').removeClass('open');
    //         element.siblings('li').find('ul').slideUp(200);
    //     }
    // });

    // Product - Slider
    var slider = $('.product-slider'); // селектор слайдера
    var pagerItem = $('.product-pager li'); // селектор пункта пагинатора
    var active = 'active'; // класс активного пункта пагинатора

    if ( slider.length ) {
        var prev = false;
        function pager() {
            pagerItem.filter('.' + active).each(function() {
                var el = $(this);
                if (prev) {
                    if ( el.is(':first-child') ) {
                        el.removeClass(active);
                        pagerItem.filter(':last').addClass(active);
                    } else el.removeClass(active).prev().addClass(active);
                } else {
                    if ( el.is(':last-child') ) {
                        el.removeClass(active);
                        pagerItem.filter(':first').addClass(active);
                    } else el.removeClass(active).next().addClass(active);
                }
            })
        }
        slider.bxSlider({
            // опции плагина
            controls: false,
            pager: false,
            slideMargin: 10,
            auto: true,
            autoHover: true,
            pause: 5000,
            touchEnabled: true,
            oneToOneTouch: true,
            onSlidePrev: function() { prev = true; pager(); },
            onSlideNext: function() { prev = false; pager(); }
            // конец опций
        });
        pagerItem.click(function() {
            slider.stopAuto();
            var index = pagerItem.index($(this));
            slider.finish().goToSlide(index);
            pagerItem.removeClass(active);
            $(this).addClass(active);
            console.log( slider );
        }).mouseleave(function() {
            slider.startAuto();
        });
        pagerItem.filter(':first').addClass(active);
    }

    // Zoom
    $('.slb').simplebox({
        fadeSpeed: 300, // default is 400ms, applied to all fade animations in the plugin
        darkMode: true // default is false
    });

    // Open - Modal
    $('#button-pass').click(function () {
        $('#myModal').modal('show');
    });


    // loader pages
    $('.loader').fadeOut();
});